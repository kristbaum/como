import 'dart:convert';

import 'package:como/config.dart';

abstract class ComoResponse {
  ComoResponse({
    required this.skipped,
  });

  final bool skipped;

  factory ComoResponse.toJson() => throw UnimplementedError();
}

sendResponse(
    String gameSlug, int promptId, Map<String, dynamic> responseJson) async {
  var response = await client.post(
    Uri.parse('$backendUrl/api/games/$gameSlug/prompts/$promptId/responses/'),
    body: jsonEncode(responseJson),
    headers: defaultRequestHeaders,
  );

  if (response.statusCode == 201) {
    log.info("Response sent successfully");
  } else {
    log.warning("Response failed");
    // TODO: Handle this error
    //throw Exception('Failed to send response');
  }
}
