import 'package:flutter/material.dart';
import 'dart:convert';

class Game {
  final int id;
  final String name;
  final String slug;
  final String shortDescription;
  final String longDescription;
  final String user;
  final Uri userpage;
  final String questionWithJokers;
  final String promptType;
  final Icon promptIcon;
  final String responseType;
  final Icon responseIcon;
  final String? actionType;
  final bool isActive;
  final String confidenceType;
  final DateTime createdAt;
  final String createdAtShort;
  final String suggestedCSVHeaders;
  final List<String> actionRequestsWithPlaceholders;
  final List<String> promptLabelsList;

  Game({
    required this.id,
    required this.name,
    required this.slug,
    required this.shortDescription,
    required this.longDescription,
    required this.user,
    required this.userpage,
    required this.questionWithJokers,
    required this.promptType,
    required this.promptIcon,
    required this.responseType,
    required this.responseIcon,
    this.actionType,
    required this.isActive,
    required this.confidenceType,
    required this.createdAt,
    required this.createdAtShort,
    required this.suggestedCSVHeaders,
    required this.actionRequestsWithPlaceholders,
    required this.promptLabelsList,
  });

  factory Game.fromJson(Map<String, dynamic> json) {
    Icon promptIcon = (json['prompt_type'] == 'TT' ||
            json['prompt_type'] == 'TTT' ||
            json['prompt_type'] == 'TTTT')
        ? const Icon(Icons.text_fields)
        : const Icon(Icons.image);

    Icon responseIcon = json['response_type'] == 'YN'
        ? const Icon(Icons.check_circle_outline)
        : const Icon(Icons.text_fields);

    List<String> actionRequestsWithPlaceholders =
        List<String>.from(json['action_requests_with_placeholders'] ?? []);

    List<String> promptLabelsList;
    var labelsData = json['prompt_labels_list'];
    if (labelsData is String) {
      promptLabelsList = List<String>.from(jsonDecode(labelsData));
    } else {
      promptLabelsList = [];
    }

    return Game(
      id: json['id'],
      name: json['name'],
      slug: json['slug'],
      shortDescription: json['short_description'],
      longDescription: json['long_description'],
      user: json['user'],
      userpage:
          Uri.parse("https://meta.wikimedia.org/wiki/User:${json['user']}"),
      questionWithJokers: json['question_with_jokers'],
      promptType: json['prompt_type'],
      promptIcon: promptIcon,
      responseType: json['response_type'],
      responseIcon: responseIcon,
      actionType: json['action_type'],
      isActive: json['is_active'],
      confidenceType: json['confidence_type'],
      createdAt: DateTime.parse(json['created_at']),
      createdAtShort:
          "${DateTime.parse(json['created_at']).day}.${DateTime.parse(json['created_at']).month}.${DateTime.parse(json['created_at']).year}",
      suggestedCSVHeaders: json['suggested_csv_headers'],
      actionRequestsWithPlaceholders: actionRequestsWithPlaceholders,
      promptLabelsList: promptLabelsList,
    );
  }
}
