package org.kristbaum.como.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import org.json.JSONException;
import org.json.JSONObject;
import org.kristbaum.como.R;
import org.kristbaum.como.client.Invoker;
import org.kristbaum.como.database.models.SenseCreation;
import org.kristbaum.como.database.models.User;
import org.kristbaum.como.viewModels.CreationViewModel;
import org.kristbaum.como.viewModels.MainViewModel;
import org.kristbaum.como.viewModels.SyncViewModel;

import java.util.List;
import java.util.Random;
import java.util.concurrent.Executor;

public class CreationActivity extends AppCompatActivity {
    private static final String TAG = "CreatorActivity";
    private SyncViewModel syncViewModel;
    private CreationViewModel creationViewModel;


    private List<SenseCreation> senseCreationsList;
    private EditText senseBox;
    private SenseCreation activeSenseCreation;
    private int lastRandom = 9999999;
    private boolean initial = true;
    private User user;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_creation);
        creationViewModel = new ViewModelProvider(this).get(CreationViewModel.class);
        syncViewModel = new ViewModelProvider(this).get(SyncViewModel.class);
        creationViewModel.getUserData().observe(this, new Observer<User>() {
            @Override
            public void onChanged(User userdata) {
                user = userdata;
            }
        });

        senseBox = findViewById(R.id.editTextSenseBox);
        final Button saveButton = findViewById(R.id.buttonSave);
        final Button skipButton = findViewById(R.id.buttonSkip);

        setListeners(senseBox, skipButton, saveButton);

        creationViewModel.getAllUneditedInLanguage().observe(this, new Observer<List<SenseCreation>>() {
            @Override
            public void onChanged(List<SenseCreation> senseCreations) {
                senseCreationsList = senseCreations;
                if (senseCreationsList.size() < 10) {
                    syncData();
                }
                setSenseCreation(true);
            }
        });

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    private void setListeners(final EditText senseBox, final Button skipButton, final Button saveButton) {
        senseBox.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().trim().length() == 0) {
                    saveButton.setEnabled(false);
                } else {
                    saveButton.setEnabled(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });

        skipButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                skipSenseCreation();
                setSenseCreation(false);
            }
        });

        saveButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                saveInput();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    private void saveInput() {
        int senseCredits = 0;
        try {
            JSONObject stats = new JSONObject(user.getStats());
            senseCredits = stats.getInt("times_guessed_correct_since_sense_creation")
                    - MainViewModel.getPointsToSenseCreation();
            stats.put("times_guessed_correct_since_sense_creation",
                    senseCredits);
            user.setStats(stats.toString());
            creationViewModel.updateUserData(user);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        String playerInput = senseBox.getText().toString().trim();
        Log.v(TAG,"Answer provided by player: " + playerInput);

        JSONObject obj = new JSONObject();
        try {
            obj.put("sense_input", playerInput);
            obj.put("skipped", false);
            obj.put("sense_lang", MainViewModel.getSense_lang());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        activeSenseCreation.setPlayer_input(obj.toString());
        creationViewModel.updateSenseCreation(activeSenseCreation);
        syncData();

        if (senseCredits >= MainViewModel.getPointsToSenseCreation()) {
            setSenseCreation(false);
        } else {
            Intent intent = new Intent(this, GuessingActivity.class);
            startActivity(intent);
        }
    }

    private void syncData() {
        Executor executor = new Invoker();
        executor.execute(new Runnable() {
            @Override
            public void run() {
                syncViewModel.syncData();
            }
        });
    }

    private void skipSenseCreation() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("skipped", true);
            obj.put("sense_input", "");
            obj.put("sense_lang", MainViewModel.getSense_lang());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        activeSenseCreation.setPlayer_input(obj.toString());
        creationViewModel.updateSenseCreation(activeSenseCreation);
    }

    private void setSenseCreation(boolean initial) {
        if (initial && this.initial) {
            this.initial = false;
        } else if (initial){
            return;
        }

        //TODO: check for enough items
        if (senseCreationsList.size() < 10) {
            syncData();
        }

        int random = new Random().nextInt(senseCreationsList.size());
        if (activeSenseCreation != null) {
            while (random == lastRandom) {
                random = new Random().nextInt(senseCreationsList.size());
            }
        }
        lastRandom = random;

        try {
            activeSenseCreation = senseCreationsList.get(random);
            JSONObject senseCreationObj = new JSONObject(activeSenseCreation.suggestion_content);

            Log.v(TAG, "SenseCreation selected" + senseCreationObj);
            String lemma = senseCreationObj.getJSONObject("lemma").getString("value");
            String cat = senseCreationObj.getJSONObject("cat").getString("value");
            String link = senseCreationObj.getJSONObject("l").getString("value");
            final TextView lemmaView = findViewById(R.id.textViewLemma);
            lemmaView.setText(lemma);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        senseBox.setText("");
    }
}