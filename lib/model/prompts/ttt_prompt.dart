import 'package:como/model/prompts/como_prompt.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:como/model/prompts/response_option.dart';

part 'ttt_prompt.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class TTTPrompt extends ComoPrompt {
  TTTPrompt({
    required super.id,
    required super.question,
    required super.responseOptions,
    required this.text1,
    this.link1,
    required this.text2,
    this.link2,
    required this.text3,
    this.link3,
  });

  final String text1;
  final Uri? link1;
  final String text2;
  final Uri? link2;
  final String text3;
  final Uri? link3;

  factory TTTPrompt.fromJson(Map<String, dynamic> json) =>
      _$TTTPromptFromJson(json);
}
