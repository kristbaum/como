import 'package:flutter/material.dart';

class AppUser {
  String username;
  String password;
  String? token;
  String language;
  String? wikimediaUsername;
  int missingForStreakToday;
  ThemeMode? theme;

  AppUser({
    this.username = "User",
    this.token,
    this.password = "",
    this.language = "en",
    this.wikimediaUsername,
    this.missingForStreakToday = 10,
  });

  bool isAnonymous() {
    return token == null;
  }

  bool isWikimediaUser() {
    if (wikimediaUsername == null) {
      return false;
    } else {
      return wikimediaUsername!.isNotEmpty;
    }
  }
}
