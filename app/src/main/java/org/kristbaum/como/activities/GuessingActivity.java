package org.kristbaum.como.activities;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.kristbaum.como.R;
import org.kristbaum.como.client.Invoker;
import org.kristbaum.como.database.models.Game;
import org.kristbaum.como.database.models.User;
import org.kristbaum.como.viewModels.GuessingViewModel;
import org.kristbaum.como.viewModels.MainViewModel;
import org.kristbaum.como.viewModels.SyncViewModel;

import java.util.List;
import java.util.Random;
import java.util.concurrent.Executor;

public class GuessingActivity extends AppCompatActivity {

    private static final String TAG = "GameActivity";
    private SyncViewModel syncViewModel;
    private GuessingViewModel guessingViewModel;

    private Button checkButton;
    private Button skipButton;

    private TextView textViewSense;
    private String lemma;
    private EditText answerBox;
    private List<Game> gamesList;
    private User user;
    private Game activeGame;
    private int lastRandom = 9999999;
    private boolean initial = true;
    private ProgressBar progressBar;

    private int tries = 0;
    private TextView textViewTips;
    private TextView textViewCat;
    private LinearLayout layoutTips;
    private JSONArray playerInputs = new JSONArray();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guessing);
        guessingViewModel = new ViewModelProvider(this).get(GuessingViewModel.class);
        syncViewModel = new ViewModelProvider(this).get(SyncViewModel.class);
        guessingViewModel.getUserData().observe(this, new Observer<User>() {
            @Override
            public void onChanged(User userdata) {
                if (user == null) {
                    setGameObserver();
                }
                user = userdata;
            }
        });
        textViewSense = findViewById(R.id.textViewSense);
        textViewCat = findViewById(R.id.textViewCat);
        textViewTips = findViewById(R.id.textViewTips);
        layoutTips = findViewById(R.id.tipLayout);
        progressBar = findViewById(R.id.progressBar);

        checkButton = findViewById(R.id.buttonCheck);
        answerBox = findViewById(R.id.editTextAnswer);
        skipButton = findViewById(R.id.buttonSkip);
        setListeners(checkButton, skipButton, answerBox);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    protected void onStop() {
        super.onStop();
        String stats = guessingViewModel.getStats(user).toString();
        Log.v(TAG, "Save user stats: " + stats);
        user.setStats(stats);
        guessingViewModel.updateUser(user);
    }

    private void setGameObserver() {
        guessingViewModel.getUnplayedGames().observe(this, new Observer<List<Game>>() {
            @Override
            public void onChanged(List<Game> games) {
                gamesList = games;
                if (initial) {
                    initial = false;
                    setProgressBar(user, false);
                    setGame();
                }
            }
        });
    }

    private void setListeners(final Button checkButton, final Button skipButton, final EditText answerBox) {
        checkButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String answer = answerBox.getText().toString();
                Log.v(TAG,"Answer provided by player: " + answer);
                checkAnswer(answer);
            }
        });

        skipButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast toast = Toast.makeText(GuessingActivity.this, getResources().getString(R.string.AnswerToast)
                        + lemma, Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP|Gravity.END, 0,0);
                toast.show();
                addToUserObj("times_skipped_games");
                guessingViewModel.finishGame(tries, playerInputs, activeGame, false, true);
                tries = 0;
                setGame();
            }
        });

        answerBox.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().trim().length() == 0) {
                    checkButton.setEnabled(false);
                } else {
                    checkButton.setEnabled(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });

        answerBox.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (checkButton.isEnabled() &&
                        (actionId == EditorInfo.IME_ACTION_DONE ||
                                (event != null && event.getKeyCode() == KeyEvent.KEYCODE_ENTER))) {
                    checkButton.performClick();
                }
                return true;
            }
        });
    }

    private void checkAnswer(String rawAnswer) {
        tries += 1;
        String answer = rawAnswer.trim();
        Log.v(TAG, user.getStats());
        if (lemma.equals(answer)) {
            addToUserObj("times_guessed_correct");
            addToUserObj("useful_interactions_since_last_sync");
            addToUserObj("times_guessed_correct_since_sense_creation");
            setProgressBar(user, true);

            guessingViewModel.finishGame(tries, playerInputs, activeGame, true, false);
            setGame();
        } else {
            Toast toast = Toast.makeText(GuessingActivity.this, getResources().getString(R.string.False), Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.TOP|Gravity.END, 0,0);
            toast.show();
            addToUserObj("times_guessed_wrong");
            if (playerInputs.toString().contains("\"" + answer + "\"")) {
                playerInputs.put(answer);
                Log.v(TAG, "Player Input: " + answer);
            }
            addTips(tries, activeGame);
        }
    }

    private void setProgressBar(User user, boolean checkAnswer) {
        try {
            JSONObject stats = new JSONObject(user.getStats());
            int correct = stats.getInt("times_guessed_correct_since_sense_creation");
            int modulo = correct % MainViewModel.getPointsToSenseCreation();
            int multiplier = 100 / MainViewModel.getPointsToSenseCreation();
            progressBar.setProgress(modulo * multiplier);

            if (checkAnswer && modulo == 0) {
                Toast toast = Toast.makeText(GuessingActivity.this, getResources().getString(R.string.SensePointNotification), Toast.LENGTH_LONG);
                toast.setGravity(Gravity.TOP|Gravity.END, 0,0);
                toast.show();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void addTips(int tries, Game activeGame) {
        layoutTips.setVisibility(View.VISIBLE);
        String cat = guessingViewModel.getCategoryString(activeGame);
        if (cat.contains("Q1084")) {
            textViewCat.setText(getResources().getString(R.string.CatNoun));
        } else if (cat.contains("Q24905")) {
            textViewCat.setText(getResources().getString(R.string.CatVerb));
        } else {
            textViewCat.setText(getResources().getString(R.string.CatUnknown));
        }
        textViewTips.setText(guessingViewModel.getTip(lemma, tries));
    }

    private void addToUserObj(String fieldName) {
        try {
            JSONObject stats =  guessingViewModel.getStats(user);
            stats.put(fieldName, stats.getInt(fieldName) + 1);
            user.setStats(stats.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setGame() {
        tries = 0;
        playerInputs = new JSONArray();

        if (gamesList.size() == 0) {
            skipButton.setVisibility(View.INVISIBLE);
            checkButton.setVisibility(View.INVISIBLE);
            answerBox.setVisibility(View.INVISIBLE);
            textViewSense.setText(getResources().getString(R.string.NoNewGames));
            layoutTips.setVisibility(View.INVISIBLE);
            Executor executor = new Invoker();
            executor.execute(new Runnable() {
                @Override
                public void run() {
                    syncViewModel.syncData();
                }
            });
            return;
        } else if (gamesList.size() < 10) {
            Executor executor = new Invoker();
            executor.execute(new Runnable() {
                @Override
                public void run() {
                    syncViewModel.syncData();
                }
            });
        }

        layoutTips.setVisibility(View.INVISIBLE);
        textViewTips.setText("");
        textViewCat.setText("");

        JSONObject stats = guessingViewModel.getStats(user);
        while (stats == null) {
            try {
                Thread.sleep(20);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            stats = guessingViewModel.getStats(user);
        }

        String sense = "";
        int random = new Random().nextInt(gamesList.size());
        if (activeGame != null) {
            while (random == lastRandom) {
                random = new Random().nextInt(gamesList.size());
            }
        }
        lastRandom = random;
        activeGame = gamesList.get(random);
        try {
            JSONObject gameObj = new JSONObject(activeGame.content);
            lemma = gameObj.getJSONObject("lemma").getString("value").trim();
            Log.v(TAG, "Solution: " + lemma);
            sense = gameObj.getJSONObject("sense").getString("value");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        answerBox.setText("");
        textViewSense.setText(sense);
    }
}