
abstract class WebConditionalImport {
  Future<void> pushState(Uri redirectUri);
}

// The factory constructor delegates to the platform-specific `getInstance`.
WebConditionalImport getInstance() =>
    throw UnsupportedError('Unsupported platform');
