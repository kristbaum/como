import 'package:como/config.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class ImageCardWidget extends StatelessWidget {
  final String title;
  final Uri imageUri;
  final String imageFilename;
  final Uri imageFilenameUrl;

  const ImageCardWidget({
    super.key,
    required this.title,
    required this.imageUri,
    required this.imageFilename,
    required this.imageFilenameUrl,
  });

  @override
  Widget build(BuildContext context) {
    return Card(
      child: ListTile(
        title: Text(
          title,
          style: const TextStyle(
            fontSize: 12.0,
            color: Colors.grey,
          ),
        ),
        subtitle: Column(
          children: [
            InkWell(
              onTap: () => launchUrl(imageFilenameUrl),
              child: Text(imageFilename,
                  style: const TextStyle(color: Colors.blue),
                  textAlign: TextAlign.center,
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2),
            ),
            InkWell(
              onTap: () => _showImageDialog(context, imageUri),
              child: ConstrainedBox(
                constraints: const BoxConstraints(
                  maxHeight: 500, // Set a maximum height
                  maxWidth: 500, // Set a maximum width
                ),
                child: Image.network(
                  imageUri.toString(),
                  fit: BoxFit.contain, // Maintains aspect ratio within the box
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _showImageDialog(BuildContext context, Uri imageUrl) {
    final bool isFullScreen = MediaQuery.of(context).size.width < appWidth;

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          insetPadding: EdgeInsets.all(
              isFullScreen ? 0 : 20), // Padding for smaller screens
          child: isFullScreen
              ? Scaffold(
                  appBar: AppBar(
                    leading: IconButton(
                      icon: const Icon(Icons.close),
                      onPressed: () => Navigator.of(context).pop(),
                    ),
                  ),
                  body: Center(
                    child: InteractiveViewer(
                      panEnabled: true,
                      minScale: 0.5,
                      maxScale: 4.0,
                      child: Image.network(
                        imageUrl.toString(),
                        fit: BoxFit.contain,
                      ),
                    ),
                  ),
                )
              : InteractiveViewer(
                  panEnabled: true,
                  minScale: 0.5,
                  maxScale: 4.0,
                  child: Image.network(
                    imageUrl.toString(),
                    fit: BoxFit.contain,
                  ),
                ),
        );
      },
    );
  }
}
