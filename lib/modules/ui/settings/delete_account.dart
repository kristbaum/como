import 'package:como/login.dart';
import 'package:flutter/material.dart';
import 'package:como/l10n/app_localizations.dart';

class DeleteAccountCard extends StatelessWidget {
  const DeleteAccountCard({super.key});

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          ListTile(
            leading: const Icon(Icons.delete_forever),
            title: Text(AppLocalizations.of(context)!.delete_account),
            subtitle:
                Text(AppLocalizations.of(context)!.delete_account_description),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              TextButton(
                child: Text(AppLocalizations.of(context)!.delete_account),
                onPressed: () {
                  logout();
                  ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(
                      content: Text(
                          AppLocalizations.of(context)!.account_deleted_reload),
                    ),
                  );
                },
              ),
            ],
          ),
        ],
      ),
    );
  }
}
