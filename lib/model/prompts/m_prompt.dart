import 'package:como/model/prompts/como_prompt.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:como/model/prompts/response_option.dart';
part 'm_prompt.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class MPrompt extends ComoPrompt {
  MPrompt({
    required super.id,
    required super.question,
    required super.responseOptions,
    required this.media1,
    required this.media1Filename,
    required this.media1FilenameUrl,
  });

  final Uri media1;
  final String media1Filename;
  final Uri media1FilenameUrl;

  factory MPrompt.fromJson(Map<String, dynamic> json) =>
      _$MPromptFromJson(json);
}
