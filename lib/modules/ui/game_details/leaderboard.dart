import 'package:como/config.dart';
import 'package:como/model/leaderboard_entry.dart';
import 'package:como/model/stat.dart';
import 'package:como/pages/game_details.dart';
import 'package:como/services/game_details.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class Leaderboard extends StatelessWidget {
  final GameDetailsPage widget;

  const Leaderboard({
    super.key,
    required this.widget,
  });

  @override
  Widget build(BuildContext context) {
    return Card(
      child: ListTile(
        title: const Text("Leaderboard"),
        subtitle: _buildLeaderboard(context),
      ),
    );
  }

  Widget _buildLeaderboard(BuildContext context) {
    return FutureBuilder<Stat>(
      future: fetchGameStats(widget.gameSlug),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return const Center(child: CircularProgressIndicator());
        } else if (snapshot.hasError) {
          return Text('Error: ${snapshot.error}');
        } else if (snapshot.data?.leaderboard.isEmpty ?? true) {
          return const Text("No leaderboard data available.");
        } else {
          return _buildDataTable(snapshot.data!.leaderboard);
        }
      },
    );
  }

  Widget _buildDataTable(List<LeaderboardEntry> leaderboard) {
    return Center(
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: DataTable(
          columns: const [
            DataColumn(label: Text('#')),
            DataColumn(label: Text('Username')),
            DataColumn(label: Text('# of Responses')),
          ],
          rows: List<DataRow>.generate(
            leaderboard.length,
            (index) => DataRow(
              selected: leaderboard[index].username == user.username,
              cells: [
                DataCell(
                    Text('${index + 1}')), // Display row number starting from 1
                DataCell(
                  _buildUsernameCell(leaderboard[index]),
                ),
                DataCell(Text('${leaderboard[index].responsesCount}')),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildUsernameCell(LeaderboardEntry entry) {
    if (entry.profileLink != null) {
      return InkWell(
        child: Text(
          entry.username,
          style: const TextStyle(color: Colors.blue),
        ),
        onTap: () => launchUrl(Uri.parse(entry.profileLink!)),
      );
    } else {
      return Text(entry.username);
    }
  }
}
