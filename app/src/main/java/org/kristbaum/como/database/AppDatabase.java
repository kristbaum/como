package org.kristbaum.como.database;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import org.kristbaum.como.database.Daos.GameDao;
import org.kristbaum.como.database.Daos.SenseCreationDao;
import org.kristbaum.como.database.Daos.UserDao;
import org.kristbaum.como.database.models.Game;
import org.kristbaum.como.database.models.SenseCreation;
import org.kristbaum.como.database.models.User;

@Database(entities = {User.class, Game.class, SenseCreation.class}, version = 9)
public abstract class AppDatabase extends RoomDatabase {
    private static final String TAG = "DB";
    private static AppDatabase instance;

    public abstract UserDao userDao();
    public abstract GameDao gameDao();
    public abstract SenseCreationDao senseCreationDao();

    public static synchronized AppDatabase getInstance(Context context) {
        if (instance == null) {
            instance = Room.databaseBuilder(context.getApplicationContext(),
                    AppDatabase.class, "como_database")
                    .fallbackToDestructiveMigration()
                    .addCallback(roomCallback)
                    .build();
        }
        return instance;
    }

    private static final RoomDatabase.Callback roomCallback = new RoomDatabase.Callback() {
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);
            new PopulateDbAsyncTask(instance).execute();
        }
    };


    private static class PopulateDbAsyncTask extends AsyncTask<Void, Void, Void> {
        private final UserDao userDao;
        private final GameDao gameDao;
        private final SenseCreationDao senseCreationDao;
        private PopulateDbAsyncTask(AppDatabase db) {
            userDao = db.userDao();
            gameDao = db.gameDao();
            senseCreationDao = db.senseCreationDao();
        }
        @Override
        protected Void doInBackground(Void... voids) {
            Log.v(TAG, "PopulateDB");
            return null;
        }
    }
}

