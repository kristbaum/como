import 'dart:convert';

import 'package:como/config.dart';
import 'package:como/l10n/app_localizations.dart';
import 'package:como/model/game.dart';
import 'package:como/model/stat.dart';
import 'package:file_saver/file_saver.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

Future<void> downloadZip(String slug) async {
  try {
    log.info("Downloading Zip file");
    final response = await client.get(
      Uri.parse('$backendUrl/api/games/$slug/download'),
      headers: defaultRequestHeaders,
    );
    if (response.statusCode == 200) {
      // Save the CSV file

      await FileSaver.instance.saveFile(
        name: '$slug.zip',
        bytes: response.bodyBytes,
        ext: 'zip',
      );
      log.info("Zip file downloaded successfully");
    } else {
      throw Exception('Failed to download Zip file');
    }
  } catch (e) {
    throw Exception('Network Error, please check your connection');
  }
}

Future<Game> fetchGameData(String slug) async {
  try {
    final response = await client.get(Uri.parse('$backendUrl/api/games/$slug'),
        headers: defaultRequestHeaders);
    if (response.statusCode == 200) {
      Game gameObj = Game.fromJson(jsonDecode(utf8.decode(response.bodyBytes)));
      return gameObj;
    } else {
      throw Exception('Failed to load game');
    }
  } catch (e) {
    throw Exception(
        'Network Error, please check your connection! Debug Details: $e');
  }
}

Future<Stat> fetchGameStats(String slug) async {
  try {
    final response = await client.get(
        Uri.parse('$backendUrl/api/stats/?game=$slug'),
        headers: defaultRequestHeaders);
    if (response.statusCode == 200) {
      Stat statObj = Stat.fromJson(jsonDecode(utf8.decode(response.bodyBytes)));
      return statObj;
    } else {
      throw Exception('Failed to load stat');
    }
  } catch (e) {
    throw Exception('Network Error, please check your connection');
  }
}

void toggleActiveState(
    String gameSlug, bool isActive, BuildContext context) async {
  try {
    final response = await client.patch(
      Uri.parse('$backendUrl/api/games/$gameSlug/'),
      headers: defaultRequestHeaders,
      body: jsonEncode({'is_active': !isActive}),
    );
    if (response.statusCode == 200) {
      log.info('Game status updated');
    } else {
      throw Exception('Failed to update game status');
    }
  } catch (e) {
    if (context.mounted) {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          content: Text('Error: Failed to update game status'),
        ),
      );
    }
  }
}

Future<String> fetchCsvSetup(BuildContext context) async {
  try {
    final response = await client.get(
      Uri.parse('$backendUrl/api/csv-suggestions/'),
      headers: defaultRequestHeaders,
    );
    if (response.statusCode == 200) {
      log.info('CSV setup fetched successfully');
      // Process the response if needed
      final data = response.body;
      log.info('Fetched CSV setup: $data');
      return data;
    } else {
      throw Exception('Failed to fetch CSV setup');
    }
  } catch (e) {
    if (context.mounted) {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          content: Text('Error: Failed to fetch CSV setup'),
        ),
      );
    }
  }
  log.info("Returning empty string");
  return '';
}

Future<void> updateGameField(BuildContext context, String gameSlug,
    Map<String, dynamic> updatedFields) async {
  log.info("Updating game fields: $updatedFields");
  try {
    final response = await client.patch(
        Uri.parse('$backendUrl/api/games/$gameSlug/'),
        headers: defaultRequestHeaders,
        body: jsonEncode(updatedFields));

    if (response.statusCode == 200) {
      log.info('Game fields updated successfully: $updatedFields');
      if (context.mounted) {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(AppLocalizations.of(context)!.fields_updated),
          ),
        );
      }
    } else {
      throw Exception('Failed to update game fields');
    }
  } catch (e) {
    log.severe('Error updating game fields: $e');
    if (context.mounted) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(content: Text(AppLocalizations.of(context)!.update_failed)),
      );
    }
  }
}

Future<void> deleteGame(BuildContext context, String gameSlug) async {
  log.info("Delete game: $gameSlug");
  try {
    final response = await client.delete(
      Uri.parse('$backendUrl/api/games/$gameSlug/'),
      headers: defaultRequestHeaders,
    );

    if (response.statusCode == 204) {
      log.info('Game deleted successfully: $gameSlug');
      if (context.mounted) {
        context.pop();
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text("Game deleted successfully"),
          ),
        );
      }
    } else {
      throw Exception('Failed to delete game');
    }
  } catch (e) {
    log.severe('Error deleting game: $e');
    if (context.mounted) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(content: Text("Error: Failed to delete game")),
      );
    }
  }
}
