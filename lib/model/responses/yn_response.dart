import 'dart:convert';

import 'package:como/config.dart';
import 'package:como/model/game.dart';
import 'package:como/model/responses/como_response.dart';

class YNResponse extends ComoResponse {
  YNResponse({
    required super.skipped,
    required this.answerIsYes,
  });
  final bool answerIsYes;

  Map<String, dynamic> toJson() {
    return {
      'skipped': skipped,
      'answer_is_yes': answerIsYes,
    };
  }
}

Future<void> sendYNResponse(
  bool skipped,
  bool answerIsYes,
  Game game,
  int promptId,
) async {
  log.info("Sending YN response");
  var ynResponse = YNResponse(
    skipped: skipped,
    answerIsYes: answerIsYes,
  );

  log.info(jsonEncode(ynResponse.toJson()));
  sendResponse(game.slug, promptId, ynResponse.toJson());
}
