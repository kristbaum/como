package org.kristbaum.como.client;

import android.util.Log;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.UnknownHostException;

import javax.net.ssl.HttpsURLConnection;

public class HttpConn {
    private static final String TAG = "HTTP";

    public static String sendPost(String r_url , JSONObject postDataParams) throws Exception {
        URL url = new URL(r_url);

        try {
            HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
            conn.setReadTimeout(20000);
            conn.setConnectTimeout(20000);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json; charset=utf-8");
            //conn.setDoInput(true);
            conn.setDoOutput(true);

            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter( new OutputStreamWriter(os, "UTF-8"));
            writer.write(postDataParams.toString());
            writer.flush();
            writer.close();
            os.close();

            int responseCode=conn.getResponseCode(); // To Check for 200
            Log.v(TAG, "POST Response Code: " + responseCode);
            if (responseCode == HttpsURLConnection.HTTP_OK) {

                BufferedReader in = new BufferedReader( new InputStreamReader(conn.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line;
                while((line = in.readLine()) != null) {
                    sb.append(line);
                }
                in.close();
                conn.disconnect();
                return sb.toString();
            } else if (responseCode == HttpsURLConnection.HTTP_UNAVAILABLE) {
                Log.v(TAG, "POST: Service unavailable, please retry");
                conn.disconnect();
                return "retry";
            }
            conn.disconnect();
            return null;
        } catch (UnknownHostException e) {
            Log.e(TAG, "Post request failed, no connection");
        }
        return null;
    }


    public static String sendGet(String url) throws IOException {
        URL obj = new URL(url);
        try {
            HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
            con.setRequestMethod("GET");
            int responseCode = con.getResponseCode();
            Log.v(TAG,"GET Response Code :: " + responseCode);

            if (responseCode == HttpsURLConnection.HTTP_OK) {
                BufferedReader in = new BufferedReader(new InputStreamReader( con.getInputStream()));
                String inputLine;
                StringBuilder response = new StringBuilder();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();
                con.disconnect();
                return response.toString();
            } else if (responseCode == HttpsURLConnection.HTTP_UNAVAILABLE) {
                con.disconnect();
                return "retry";
            } else {
                // TODO: handle error codes
                con.disconnect();
                return "";
            }
        } catch (UnknownHostException e) {
            Log.e(TAG, "No connection to host");
            return "";
        }
    }
}