package org.kristbaum.como.database.Daos;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import org.kristbaum.como.database.models.Game;

import java.util.List;

@Dao
public interface GameDao {
    @Query("SELECT * FROM game_table")
    LiveData<List<Game>> getAll();

    @Query("SELECT * FROM game_table " +
            "WHERE performance ISNULL AND lemma_lang = :lemma_lang AND sense_lang = :sense_lang")
    LiveData<List<Game>> getAllUnplayedGames(String lemma_lang, String sense_lang);

    @Query("SELECT * FROM game_table WHERE (NOT performance ISNULL) AND (NOT transferred)")
    List<Game> getAllPlayedNoTransferred();

    @Delete
    void delete(Game game);

    @Insert
    void insertGame(Game game);

    @Update
    void updateGame(Game game);
}