import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:intl/intl.dart' as intl;

import 'app_localizations_de.dart';
import 'app_localizations_en.dart';

// ignore_for_file: type=lint

/// Callers can lookup localized strings with an instance of AppLocalizations
/// returned by `AppLocalizations.of(context)`.
///
/// Applications need to include `AppLocalizations.delegate()` in their app's
/// `localizationDelegates` list, and the locales they support in the app's
/// `supportedLocales` list. For example:
///
/// ```dart
/// import 'l10n/app_localizations.dart';
///
/// return MaterialApp(
///   localizationsDelegates: AppLocalizations.localizationsDelegates,
///   supportedLocales: AppLocalizations.supportedLocales,
///   home: MyApplicationHome(),
/// );
/// ```
///
/// ## Update pubspec.yaml
///
/// Please make sure to update your pubspec.yaml to include the following
/// packages:
///
/// ```yaml
/// dependencies:
///   # Internationalization support.
///   flutter_localizations:
///     sdk: flutter
///   intl: any # Use the pinned version from flutter_localizations
///
///   # Rest of dependencies
/// ```
///
/// ## iOS Applications
///
/// iOS applications define key application metadata, including supported
/// locales, in an Info.plist file that is built into the application bundle.
/// To configure the locales supported by your app, you’ll need to edit this
/// file.
///
/// First, open your project’s ios/Runner.xcworkspace Xcode workspace file.
/// Then, in the Project Navigator, open the Info.plist file under the Runner
/// project’s Runner folder.
///
/// Next, select the Information Property List item, select Add Item from the
/// Editor menu, then select Localizations from the pop-up menu.
///
/// Select and expand the newly-created Localizations item then, for each
/// locale your application supports, add a new item and select the locale
/// you wish to add from the pop-up menu in the Value field. This list should
/// be consistent with the languages listed in the AppLocalizations.supportedLocales
/// property.
abstract class AppLocalizations {
  AppLocalizations(String locale) : localeName = intl.Intl.canonicalizedLocale(locale.toString());

  final String localeName;

  static AppLocalizations? of(BuildContext context) {
    return Localizations.of<AppLocalizations>(context, AppLocalizations);
  }

  static const LocalizationsDelegate<AppLocalizations> delegate = _AppLocalizationsDelegate();

  /// A list of this localizations delegate along with the default localizations
  /// delegates.
  ///
  /// Returns a list of localizations delegates containing this delegate along with
  /// GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate,
  /// and GlobalWidgetsLocalizations.delegate.
  ///
  /// Additional delegates can be added by appending to this list in
  /// MaterialApp. This list does not have to be used at all if a custom list
  /// of delegates is preferred or required.
  static const List<LocalizationsDelegate<dynamic>> localizationsDelegates = <LocalizationsDelegate<dynamic>>[
    delegate,
    GlobalMaterialLocalizations.delegate,
    GlobalCupertinoLocalizations.delegate,
    GlobalWidgetsLocalizations.delegate,
  ];

  /// A list of this localizations delegate's supported locales.
  static const List<Locale> supportedLocales = <Locale>[
    Locale('de'),
    Locale('en')
  ];

  /// No description provided for @pageHomeWikimediaLogin.
  ///
  /// In en, this message translates to:
  /// **'Login with Wikimedia'**
  String get pageHomeWikimediaLogin;

  /// No description provided for @drawer_continue.
  ///
  /// In en, this message translates to:
  /// **'Continue'**
  String get drawer_continue;

  /// No description provided for @drawer_continue_desc.
  ///
  /// In en, this message translates to:
  /// **'Continue the game you were playing last'**
  String get drawer_continue_desc;

  /// No description provided for @drawer_random_game.
  ///
  /// In en, this message translates to:
  /// **'Random'**
  String get drawer_random_game;

  /// No description provided for @drawer_random_game_desc.
  ///
  /// In en, this message translates to:
  /// **'Start a random game'**
  String get drawer_random_game_desc;

  /// No description provided for @drawer_create.
  ///
  /// In en, this message translates to:
  /// **'Create'**
  String get drawer_create;

  /// No description provided for @drawer_create_desc.
  ///
  /// In en, this message translates to:
  /// **'Create a new game'**
  String get drawer_create_desc;

  /// No description provided for @drawer_gameslist.
  ///
  /// In en, this message translates to:
  /// **'List'**
  String get drawer_gameslist;

  /// No description provided for @drawer_gameslist_desc.
  ///
  /// In en, this message translates to:
  /// **'List all public games'**
  String get drawer_gameslist_desc;

  /// No description provided for @drawer_mygames.
  ///
  /// In en, this message translates to:
  /// **'My games'**
  String get drawer_mygames;

  /// No description provided for @drawer_stats.
  ///
  /// In en, this message translates to:
  /// **'Stats'**
  String get drawer_stats;

  /// No description provided for @drawer_privacy_policy.
  ///
  /// In en, this message translates to:
  /// **'Privacy policy'**
  String get drawer_privacy_policy;

  /// No description provided for @drawer_settings.
  ///
  /// In en, this message translates to:
  /// **'Settings'**
  String get drawer_settings;

  /// No description provided for @drawer_about.
  ///
  /// In en, this message translates to:
  /// **'Help/About'**
  String get drawer_about;

  /// No description provided for @error.
  ///
  /// In en, this message translates to:
  /// **'Error'**
  String get error;

  /// No description provided for @set_inactive_as_admin.
  ///
  /// In en, this message translates to:
  /// **'Set Inactive (as Admin)'**
  String get set_inactive_as_admin;

  /// No description provided for @set_active_as_admin.
  ///
  /// In en, this message translates to:
  /// **'Set Active (as Admin)'**
  String get set_active_as_admin;

  /// No description provided for @share_this_game.
  ///
  /// In en, this message translates to:
  /// **'Share this game'**
  String get share_this_game;

  /// No description provided for @start.
  ///
  /// In en, this message translates to:
  /// **'Start'**
  String get start;

  /// No description provided for @this_game_is_not_yet_active_or_finished.
  ///
  /// In en, this message translates to:
  /// **'This game is not yet active or finished'**
  String get this_game_is_not_yet_active_or_finished;

  /// No description provided for @you_need_to_be_logged_in_to_play_this_game.
  ///
  /// In en, this message translates to:
  /// **'You need to be logged in to play this game'**
  String get you_need_to_be_logged_in_to_play_this_game;

  /// No description provided for @description.
  ///
  /// In en, this message translates to:
  /// **'Description'**
  String get description;

  /// No description provided for @statistics.
  ///
  /// In en, this message translates to:
  /// **'Statistics'**
  String get statistics;

  /// No description provided for @done_prompts.
  ///
  /// In en, this message translates to:
  /// **'Done Prompts'**
  String get done_prompts;

  /// No description provided for @total_prompts.
  ///
  /// In en, this message translates to:
  /// **'Total Prompts'**
  String get total_prompts;

  /// No description provided for @data_from_this_game_as_a_csv_table.
  ///
  /// In en, this message translates to:
  /// **'Data from this game as a CSV table'**
  String get data_from_this_game_as_a_csv_table;

  /// No description provided for @download_csv.
  ///
  /// In en, this message translates to:
  /// **'Download CSV'**
  String get download_csv;

  /// No description provided for @user.
  ///
  /// In en, this message translates to:
  /// **'User'**
  String get user;

  /// No description provided for @question_with_placeholders.
  ///
  /// In en, this message translates to:
  /// **'Question (with placeholders)'**
  String get question_with_placeholders;

  /// No description provided for @attached_requests.
  ///
  /// In en, this message translates to:
  /// **'Attached Requests'**
  String get attached_requests;

  /// No description provided for @prompt_labels.
  ///
  /// In en, this message translates to:
  /// **'Prompt Labels'**
  String get prompt_labels;

  /// No description provided for @prompt_type.
  ///
  /// In en, this message translates to:
  /// **'Prompt Type'**
  String get prompt_type;

  /// No description provided for @response_type.
  ///
  /// In en, this message translates to:
  /// **'Response Type'**
  String get response_type;

  /// No description provided for @action_type.
  ///
  /// In en, this message translates to:
  /// **'Action Type'**
  String get action_type;

  /// No description provided for @none.
  ///
  /// In en, this message translates to:
  /// **'None'**
  String get none;

  /// No description provided for @confidence_type.
  ///
  /// In en, this message translates to:
  /// **'Confidence Type'**
  String get confidence_type;

  /// No description provided for @show_advanced.
  ///
  /// In en, this message translates to:
  /// **'Show Advanced'**
  String get show_advanced;

  /// No description provided for @welcome_to_como.
  ///
  /// In en, this message translates to:
  /// **'Welcome to Como!'**
  String get welcome_to_como;

  /// No description provided for @play_to_increase_knowledge.
  ///
  /// In en, this message translates to:
  /// **'Contribute Easily, Crowdsource Quickly.'**
  String get play_to_increase_knowledge;

  /// No description provided for @continue_recent_games.
  ///
  /// In en, this message translates to:
  /// **'Recently Played'**
  String get continue_recent_games;

  /// No description provided for @popular_games.
  ///
  /// In en, this message translates to:
  /// **'Popular'**
  String get popular_games;

  /// No description provided for @newest_games.
  ///
  /// In en, this message translates to:
  /// **'Newest'**
  String get newest_games;

  /// No description provided for @continue_button.
  ///
  /// In en, this message translates to:
  /// **'Continue Last'**
  String get continue_button;

  /// No description provided for @delete_account.
  ///
  /// In en, this message translates to:
  /// **'Delete Account'**
  String get delete_account;

  /// No description provided for @delete_account_description.
  ///
  /// In en, this message translates to:
  /// **'Delete your account and all your data. A new account will be generated without any usage data on reentry. This action cannot be undone.'**
  String get delete_account_description;

  /// No description provided for @account_deleted_reload.
  ///
  /// In en, this message translates to:
  /// **'Account deleted, reload the app now!'**
  String get account_deleted_reload;

  /// No description provided for @add_csv_file.
  ///
  /// In en, this message translates to:
  /// **'Add a csv file'**
  String get add_csv_file;

  /// No description provided for @create_game.
  ///
  /// In en, this message translates to:
  /// **'Create Game'**
  String get create_game;

  /// No description provided for @submit.
  ///
  /// In en, this message translates to:
  /// **'Submit'**
  String get submit;

  /// No description provided for @login_to_create_game.
  ///
  /// In en, this message translates to:
  /// **'Please log in first to create a game.'**
  String get login_to_create_game;

  /// No description provided for @game_created_successfully.
  ///
  /// In en, this message translates to:
  /// **'Game created successfully'**
  String get game_created_successfully;

  /// No description provided for @failed_to_create_game.
  ///
  /// In en, this message translates to:
  /// **'Failed to create game'**
  String get failed_to_create_game;

  /// No description provided for @name.
  ///
  /// In en, this message translates to:
  /// **'Name'**
  String get name;

  /// No description provided for @name_hint.
  ///
  /// In en, this message translates to:
  /// **'e.g \"Picture Quiz\"'**
  String get name_hint;

  /// No description provided for @enter_name.
  ///
  /// In en, this message translates to:
  /// **'Enter a name'**
  String get enter_name;

  /// No description provided for @short_description.
  ///
  /// In en, this message translates to:
  /// **'Short Description'**
  String get short_description;

  /// No description provided for @short_description_hint.
  ///
  /// In en, this message translates to:
  /// **'e.g \"Match the image with the text.\"'**
  String get short_description_hint;

  /// No description provided for @enter_short_description.
  ///
  /// In en, this message translates to:
  /// **'Enter a short description'**
  String get enter_short_description;

  /// No description provided for @long_description.
  ///
  /// In en, this message translates to:
  /// **'Long Description'**
  String get long_description;

  /// No description provided for @long_description_hint.
  ///
  /// In en, this message translates to:
  /// **'e.g Where the data came from, what edits will be performed. Included links will be clickable!'**
  String get long_description_hint;

  /// No description provided for @enter_long_description.
  ///
  /// In en, this message translates to:
  /// **'Enter a long description'**
  String get enter_long_description;

  /// No description provided for @question.
  ///
  /// In en, this message translates to:
  /// **'Question'**
  String get question;

  /// No description provided for @question_hint.
  ///
  /// In en, this message translates to:
  /// **'e.g. \"Does a) depict b)?\"'**
  String get question_hint;

  /// No description provided for @enter_question.
  ///
  /// In en, this message translates to:
  /// **'Enter a question'**
  String get enter_question;

  /// No description provided for @list_publicly.
  ///
  /// In en, this message translates to:
  /// **'Public'**
  String get list_publicly;

  /// No description provided for @yes.
  ///
  /// In en, this message translates to:
  /// **'yes'**
  String get yes;

  /// No description provided for @no.
  ///
  /// In en, this message translates to:
  /// **'no'**
  String get no;

  /// No description provided for @prompt_labels_hint.
  ///
  /// In en, this message translates to:
  /// **'Enter your prompt labels as a JSON Array here, maximum 20 characters each. Separate with commas.'**
  String get prompt_labels_hint;

  /// No description provided for @enter_prompt_labels.
  ///
  /// In en, this message translates to:
  /// **'Enter prompt labels'**
  String get enter_prompt_labels;

  /// No description provided for @wikibase_api.
  ///
  /// In en, this message translates to:
  /// **'Wikibase REST API'**
  String get wikibase_api;

  /// No description provided for @action_api.
  ///
  /// In en, this message translates to:
  /// **'Action API'**
  String get action_api;

  /// No description provided for @add_another_request.
  ///
  /// In en, this message translates to:
  /// **'Add another request'**
  String get add_another_request;

  /// No description provided for @request_url.
  ///
  /// In en, this message translates to:
  /// **'Request URL'**
  String get request_url;

  /// No description provided for @request_url_hint.
  ///
  /// In en, this message translates to:
  /// **'https://www.wikidata.org/w/rest.php/wikibase/v0/entities/items/%id1%/descriptions/en'**
  String get request_url_hint;

  /// No description provided for @request_body.
  ///
  /// In en, this message translates to:
  /// **'Request Body:'**
  String get request_body;

  /// No description provided for @request_body_hint.
  ///
  /// In en, this message translates to:
  /// **'Enter your data as JSON formatted text here'**
  String get request_body_hint;

  /// No description provided for @http_method.
  ///
  /// In en, this message translates to:
  /// **'HTTP Method'**
  String get http_method;

  /// No description provided for @select_http_method.
  ///
  /// In en, this message translates to:
  /// **'Please select an HTTP method'**
  String get select_http_method;

  /// No description provided for @enter_value.
  ///
  /// In en, this message translates to:
  /// **'Enter a value'**
  String get enter_value;

  /// No description provided for @paste_data_or_use_file.
  ///
  /// In en, this message translates to:
  /// **'Paste your data here or use a file: '**
  String get paste_data_or_use_file;

  /// No description provided for @csv_data_hint.
  ///
  /// In en, this message translates to:
  /// **'Enter your data as CSV formatted text here'**
  String get csv_data_hint;

  /// No description provided for @use_file.
  ///
  /// In en, this message translates to:
  /// **'Use file: '**
  String get use_file;

  /// No description provided for @json_data_hint.
  ///
  /// In en, this message translates to:
  /// **'Enter your data as JSON formatted text here'**
  String get json_data_hint;

  /// No description provided for @change_language.
  ///
  /// In en, this message translates to:
  /// **'Change Language'**
  String get change_language;

  /// No description provided for @upload_csv.
  ///
  /// In en, this message translates to:
  /// **'Upload CSV'**
  String get upload_csv;

  /// No description provided for @required_columns.
  ///
  /// In en, this message translates to:
  /// **'Required columns'**
  String get required_columns;

  /// No description provided for @start_upload.
  ///
  /// In en, this message translates to:
  /// **'Start Upload'**
  String get start_upload;

  /// No description provided for @select_file.
  ///
  /// In en, this message translates to:
  /// **'Select File'**
  String get select_file;

  /// No description provided for @edit.
  ///
  /// In en, this message translates to:
  /// **'Edit'**
  String get edit;

  /// No description provided for @editDescription.
  ///
  /// In en, this message translates to:
  /// **'Edit Description'**
  String get editDescription;

  /// No description provided for @enterDescription.
  ///
  /// In en, this message translates to:
  /// **'Enter a description'**
  String get enterDescription;

  /// No description provided for @description_updated.
  ///
  /// In en, this message translates to:
  /// **'Description updated'**
  String get description_updated;

  /// No description provided for @update_failed.
  ///
  /// In en, this message translates to:
  /// **'Update failed'**
  String get update_failed;

  /// No description provided for @fields_updated.
  ///
  /// In en, this message translates to:
  /// **'Fields updated'**
  String get fields_updated;
}

class _AppLocalizationsDelegate extends LocalizationsDelegate<AppLocalizations> {
  const _AppLocalizationsDelegate();

  @override
  Future<AppLocalizations> load(Locale locale) {
    return SynchronousFuture<AppLocalizations>(lookupAppLocalizations(locale));
  }

  @override
  bool isSupported(Locale locale) => <String>['de', 'en'].contains(locale.languageCode);

  @override
  bool shouldReload(_AppLocalizationsDelegate old) => false;
}

AppLocalizations lookupAppLocalizations(Locale locale) {


  // Lookup logic when only language code is specified.
  switch (locale.languageCode) {
    case 'de': return AppLocalizationsDe();
    case 'en': return AppLocalizationsEn();
  }

  throw FlutterError(
    'AppLocalizations.delegate failed to load unsupported locale "$locale". This is likely '
    'an issue with the localizations generation tool. Please file an issue '
    'on GitHub with a reproducible sample app and the gen-l10n configuration '
    'that was used.'
  );
}
