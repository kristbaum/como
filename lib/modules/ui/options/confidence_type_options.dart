import 'package:flutter/material.dart';

class ConfidenceOptions extends StatefulWidget {
  final String confidence;
  final ValueChanged<String> onChanged;

  const ConfidenceOptions(
      {super.key, required this.confidence, required this.onChanged});

  @override
  ConfidenceOptionsState createState() => ConfidenceOptionsState();
}

class ConfidenceOptionsState extends State<ConfidenceOptions> {
  late String _confidence;

  @override
  void initState() {
    super.initState();
    _confidence =
        widget.confidence; // Initialize with the value passed from the parent
  }

  Widget _buildOption(String value, String title) {
    return ListTile(
      title: Text(title),
      leading: Radio<String>(
        value: value,
        groupValue: _confidence,
        onChanged: (String? newValue) {
          setState(() {
            _confidence = newValue!;
          });
          widget.onChanged(_confidence);
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    const Map<String, String> options = {
      'S': 'Single Confirmation',
      'L': 'Single Confirmation (require account)',
      'D': 'Double Confirmation',
      'E': 'Double Confirmation (require acccount)',
    };

    return Wrap(
      alignment: WrapAlignment.center,
      children: options.entries
          .map((entry) => _buildOption(entry.key, entry.value))
          .toList(),
    );
  }
}
