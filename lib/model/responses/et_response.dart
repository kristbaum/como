import 'dart:convert';

import 'package:como/config.dart';
import 'package:como/model/game.dart';
import 'package:como/model/responses/como_response.dart';

class ETResponse extends ComoResponse {
  ETResponse({
    required super.skipped,
    required this.text,
  });
  final String text;

  Map<String, dynamic> toJson() {
    return {
      'skipped': skipped,
      'text': text,
    };
  }
}

Future<void> sendETResponse(
  bool skipped,
  String text,
  Game game,
  int promptId,
) async {
  log.info("Sending ET response");
  var etResponse = ETResponse(
    text: text,
    skipped: skipped,
  );

  log.info(jsonEncode(etResponse.toJson()));
  sendResponse(game.slug, promptId, etResponse.toJson());
}
