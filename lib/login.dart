import 'dart:convert';
import 'dart:math';
import 'package:como/config.dart';

Future<bool> isTokenActive(String token) async {
  try {
    final response = await client.get(
      Uri.parse('$backendUrl/auth/user/'),
      headers: {'Authorization': 'Token $token'},
    );
    return response.statusCode == 200;
  } catch (e) {
    log.warning("Failed to verify token: $e");
    return false;
  }
}

Future<bool> logout() async {
  await storage.deleteAll();
  return true;
}

Future<void> newWMLogin(String wmUsername, String djToken) async {
  if (user.isWikimediaUser() &&
      user.token == djToken &&
      user.wikimediaUsername == wmUsername) {
    log.info("Already logged in.");
    return;
  }
  user.token = djToken;
  user.wikimediaUsername = wmUsername;
  await storage.write(key: 'wikimediaUsername', value: wmUsername);
  await storage.write(key: 'token', value: djToken);
  return;
}

Future<void> readCredentials() async {
  final username = await storage.read(key: "username");
  final token = await storage.read(key: "token");
  final password = await storage.read(key: "password");
  final language = await storage.read(key: "language");
  final wikimediaUsername = await storage.read(key: "wikimediaUsername");
  final themeSelection = await storage.read(key: "themeSelection");

  if (token == null || username == null || password == null) {
    log.info("No credentials found, stay anonymous");
    //await signUp();
  } else {
    log.info("Token found, testing if token is still active");
    if (await isTokenActive(token)) {
      if (wikimediaUsername != null) {
        await testWMCredentials(token);
      }
      user.username = username;
      user.password = password;
      user.token = token;
      user.language = language ?? "en";
      user.wikimediaUsername = wikimediaUsername;
    } else {
      await tokenRefresh(username, password);
    }
  }
}

Future<void> tokenRefresh(String username, String password) async {
  log.warning("Token is not active anymore, trying to login again");
  final reloginResponse = await client.post(
    Uri.parse('$backendUrl/auth/login/'),
    headers: {'Content-Type': 'application/json'},
    body: json.encode({'username': username, 'password': password}),
  );

  if (reloginResponse.statusCode == 200) {
    final newToken = json.decode(reloginResponse.body)['key'];
    await storage.write(key: 'token', value: newToken);
    user.token = newToken;
  } else {
    log.warning("Login after finding inactive token failed");
    await storage.deleteAll();
    await readCredentials();
  }
}

Future<void> testWMCredentials(String token) async {
  final wmLoginResponse = await client.get(
    Uri.parse('$backendUrl/auth/test_oauth/'),
    headers: {
      'Authorization': "Token $token",
      'Content-type': 'application/json'
    },
  );

  if (wmLoginResponse.statusCode == 200) {
    log.info("Wikimedia login successful");
  } else {
    log.warning("Wikimedia login failed");
    user.wikimediaUsername = null;
    await storage.delete(key: 'wikimediaUsername');
    await readCredentials();
  }
}

Future<void> signUp() async {
  log.info("No credentials found, trying to signup");
  String newUsername = (100000000 + Random().nextInt(900000000)).toString();
  final randomSecure = Random.secure();
  var values = List<int>.generate(64, (i) => randomSecure.nextInt(255));
  String newPassword = base64UrlEncode(values);

  final signupResponse = await client
      .post(
        Uri.parse('$backendUrl/auth/registration/'),
        headers: {'Content-Type': 'application/json'},
        body: json.encode({
          'username': newUsername,
          'password1': newPassword,
          'password2': newPassword
        }),
      )
      .timeout(const Duration(seconds: 5));

  if (signupResponse.statusCode == 204) {
    await loginAfterSignup(newUsername, newPassword);
  } else {
    log.warning("Signup failed, deleting all storage");
    logout();
    throw "No token in storage";
  }
}

Future<void> loginAfterSignup(String newUsername, String newPassword) async {
  final loginResponse = await client.post(
    Uri.parse('$backendUrl/auth/login/'),
    headers: {'Content-Type': 'application/json'},
    body: json.encode({'username': newUsername, 'password': newPassword}),
  );

  if (loginResponse.statusCode == 200) {
    final newToken = json.decode(loginResponse.body)['key'];
    await storage.write(key: 'username', value: newUsername);
    await storage.write(key: 'token', value: newToken);
    await storage.write(key: 'password', value: newPassword);
    await storage.write(key: 'language', value: "en");

    user.username = newUsername;
    user.password = newPassword;
    user.token = newToken;
    user.language = "en";
    log.info("Signup and login successful");
  } else {
    log.warning("Login after signup failed");
  }
}
