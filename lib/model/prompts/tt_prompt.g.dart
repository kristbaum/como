// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'tt_prompt.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TTPrompt _$TTPromptFromJson(Map<String, dynamic> json) => TTPrompt(
      id: (json['id'] as num).toInt(),
      question: json['question'] as String,
      responseOptions: (json['response_options'] as List<dynamic>)
          .map((e) => ResponseOption.fromJson(e as Map<String, dynamic>))
          .toList(),
      text1: json['text1'] as String,
      link1: json['link1'] == null ? null : Uri.parse(json['link1'] as String),
      text2: json['text2'] as String,
      link2: json['link2'] == null ? null : Uri.parse(json['link2'] as String),
    );

Map<String, dynamic> _$TTPromptToJson(TTPrompt instance) => <String, dynamic>{
      'id': instance.id,
      'question': instance.question,
      'response_options': instance.responseOptions,
      'text1': instance.text1,
      'link1': instance.link1?.toString(),
      'text2': instance.text2,
      'link2': instance.link2?.toString(),
    };
