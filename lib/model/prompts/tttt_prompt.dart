import 'package:como/model/prompts/como_prompt.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:como/model/prompts/response_option.dart';
part 'tttt_prompt.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class TTTTPrompt extends ComoPrompt {
  TTTTPrompt({
    required super.id,
    required super.question,
    required super.responseOptions,
    required this.text1,
    this.link1,
    required this.text2,
    this.link2,
    required this.text3,
    this.link3,
    required this.text4,
    this.link4,
  });

  final String text1;
  final Uri? link1;
  final String text2;
  final Uri? link2;
  final String text3;
  final Uri? link3;
  final String text4;
  final Uri? link4;

  factory TTTTPrompt.fromJson(Map<String, dynamic> json) =>
      _$TTTTPromptFromJson(json);
}
