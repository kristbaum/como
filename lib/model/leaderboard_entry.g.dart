// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'leaderboard_entry.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LeaderboardEntry _$LeaderboardEntryFromJson(Map<String, dynamic> json) =>
    LeaderboardEntry(
      json['username'] as String,
      json['profile_link'] as String?,
      (json['responses_count'] as num).toInt(),
    );

Map<String, dynamic> _$LeaderboardEntryToJson(LeaderboardEntry instance) =>
    <String, dynamic>{
      'username': instance.username,
      'profile_link': instance.profileLink,
      'responses_count': instance.responsesCount,
    };
