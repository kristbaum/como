// ignore: unused_import
import 'package:intl/intl.dart' as intl;
import 'app_localizations.dart';

// ignore_for_file: type=lint

/// The translations for German (`de`).
class AppLocalizationsDe extends AppLocalizations {
  AppLocalizationsDe([String locale = 'de']) : super(locale);

  @override
  String get pageHomeWikimediaLogin => 'Login mit Wikimedia';

  @override
  String get drawer_continue => 'Fortsetzen';

  @override
  String get drawer_continue_desc => 'Setze das zuletzt gespielte Spiel fort';

  @override
  String get drawer_random_game => 'Zufällig';

  @override
  String get drawer_random_game_desc => 'Starte ein zufälliges Spiel';

  @override
  String get drawer_create => 'Erstellen';

  @override
  String get drawer_create_desc => 'Erstelle ein neues Spiel';

  @override
  String get drawer_gameslist => 'Liste';

  @override
  String get drawer_gameslist_desc => 'Liste aller öffentlichen Spiele';

  @override
  String get drawer_mygames => 'Meine Spiele';

  @override
  String get drawer_stats => 'Statistik';

  @override
  String get drawer_privacy_policy => 'Datenschutzerklärung';

  @override
  String get drawer_settings => 'Einstellungen';

  @override
  String get drawer_about => 'Hilfe/Über';

  @override
  String get error => 'Fehler';

  @override
  String get set_inactive_as_admin => 'Als Admin deaktivieren';

  @override
  String get set_active_as_admin => 'Als Admin aktivieren';

  @override
  String get share_this_game => 'Dieses Spiel teilen';

  @override
  String get start => 'Start';

  @override
  String get this_game_is_not_yet_active_or_finished => 'Dieses Spiel ist noch nicht aktiv oder beendet';

  @override
  String get you_need_to_be_logged_in_to_play_this_game => 'Sie müssen eingeloggt sein, um dieses Spiel zu spielen';

  @override
  String get description => 'Beschreibung';

  @override
  String get statistics => 'Statistiken';

  @override
  String get done_prompts => 'Abgeschlossene Prompts';

  @override
  String get total_prompts => 'Gesamtanzahl der Prompts';

  @override
  String get data_from_this_game_as_a_csv_table => 'Daten aus diesem Spiel als CSV-Tabelle';

  @override
  String get download_csv => 'CSV herunterladen';

  @override
  String get user => 'Benutzer';

  @override
  String get question_with_placeholders => 'Frage (mit Platzhaltern)';

  @override
  String get attached_requests => 'Angehängte Anfragen';

  @override
  String get prompt_labels => 'Prmoptlabels';

  @override
  String get prompt_type => 'Prompttyp';

  @override
  String get response_type => 'Antworttyp';

  @override
  String get action_type => 'Aktionstyp';

  @override
  String get none => 'Keine';

  @override
  String get confidence_type => 'Vertrauenstyp';

  @override
  String get show_advanced => 'Erweiterte Optionen anzeigen';

  @override
  String get welcome_to_como => 'Willkommen bei Como!';

  @override
  String get play_to_increase_knowledge => 'Einfach beitragen, schnell crowdsourcen.';

  @override
  String get continue_recent_games => 'Kürzlich gespielte';

  @override
  String get popular_games => 'Beliebte';

  @override
  String get newest_games => 'Neueste';

  @override
  String get continue_button => 'Fortsetzen';

  @override
  String get delete_account => 'Konto löschen';

  @override
  String get delete_account_description => 'Löschen Sie Ihr Konto und alle Ihre Daten. Ein neues Konto wird ohne Nutzungsdaten bei erneutem Login generiert. Diese Aktion kann nicht rückgängig gemacht werden.';

  @override
  String get account_deleted_reload => 'Konto gelöscht, bitte die App neu laden!';

  @override
  String get add_csv_file => 'Fügen Sie eine CSV-Datei hinzu';

  @override
  String get create_game => 'Spiel erstellen';

  @override
  String get submit => 'Einreichen';

  @override
  String get login_to_create_game => 'Bitte melden Sie sich zuerst an, um ein Spiel zu erstellen.';

  @override
  String get game_created_successfully => 'Spiel erfolgreich erstellt';

  @override
  String get failed_to_create_game => 'Spiel konnte nicht erstellt werden';

  @override
  String get name => 'Name';

  @override
  String get name_hint => 'z.B. \"Bilder-Quiz\"';

  @override
  String get enter_name => 'Geben Sie einen Namen ein';

  @override
  String get short_description => 'Kurze Beschreibung';

  @override
  String get short_description_hint => 'z.B. \"Ordne das Bild dem Text zu.\"';

  @override
  String get enter_short_description => 'Geben Sie eine kurze Beschreibung ein';

  @override
  String get long_description => 'Lange Beschreibung';

  @override
  String get long_description_hint => 'z.B. Woher die Daten stammen, welche Änderungen vorgenommen werden. Eingefügte Links sind anklickbar!';

  @override
  String get enter_long_description => 'Geben Sie eine lange Beschreibung ein';

  @override
  String get question => 'Frage';

  @override
  String get question_hint => 'z.B. \"Zeigt a) b)?\"';

  @override
  String get enter_question => 'Geben Sie eine Frage ein';

  @override
  String get list_publicly => 'Öffentlich';

  @override
  String get yes => 'ja';

  @override
  String get no => 'nein';

  @override
  String get prompt_labels_hint => 'Geben Sie Ihre Aufforderungsbeschriftungen hier als JSON-Array ein, maximal 20 Zeichen pro Beschriftung. Trennen Sie die Beschriftungen mit Kommas.';

  @override
  String get enter_prompt_labels => 'Geben Sie Aufforderungsbeschriftungen ein';

  @override
  String get wikibase_api => 'Wikibase API';

  @override
  String get action_api => 'Action API';

  @override
  String get add_another_request => 'Eine weitere Anfrage hinzufügen';

  @override
  String get request_url => 'Anfrage-URL';

  @override
  String get request_url_hint => 'https://www.wikidata.org/w/rest.php/wikibase/v0/entities/items/%id1%/descriptions/en';

  @override
  String get request_body => 'Anfrageinhalt:';

  @override
  String get request_body_hint => 'Geben Sie Ihre Daten hier als JSON-formatierten Text ein';

  @override
  String get http_method => 'HTTP-Methode';

  @override
  String get select_http_method => 'Bitte wählen Sie eine HTTP-Methode aus';

  @override
  String get enter_value => 'Geben Sie einen Wert ein';

  @override
  String get paste_data_or_use_file => 'Fügen Sie Ihre Daten hier ein oder verwenden Sie eine Datei: ';

  @override
  String get csv_data_hint => 'Geben Sie Ihre Daten hier als CSV-formatierten Text ein';

  @override
  String get use_file => 'Datei verwenden: ';

  @override
  String get json_data_hint => 'Geben Sie Ihre Daten hier als JSON-formatierten Text ein';

  @override
  String get change_language => 'Sprache wechseln';

  @override
  String get upload_csv => 'CSV hochladen';

  @override
  String get required_columns => 'Notwendige Spalten';

  @override
  String get start_upload => 'Start Upload';

  @override
  String get select_file => 'Datei auswählen';

  @override
  String get edit => 'Bearbeiten';

  @override
  String get editDescription => 'Edit Description';

  @override
  String get enterDescription => 'Enter a description';

  @override
  String get description_updated => 'Description updated';

  @override
  String get update_failed => 'Update failed';

  @override
  String get fields_updated => 'Fields updated';
}
