package org.kristbaum.como.client;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.kristbaum.como.database.models.Game;
import org.kristbaum.como.database.models.SenseCreation;
import org.kristbaum.como.database.models.User;
import org.kristbaum.como.viewModels.MainViewModel;
import org.kristbaum.como.viewModels.SyncViewModel;

import java.io.IOException;

public class Get {
    private static final String TAG = "Get";
    private final SyncViewModel syncViewModel;
    private boolean retry = true;

    public Get(SyncViewModel syncViewModel) {
        this.syncViewModel = syncViewModel;
        runGet();
    }

    private void runGet() {
        String dataString = getData();
        if (dataString == null) {
            Log.e(TAG, "Null data");
        } else if (dataString.equals("retry") && retry) {
            retry = false;
            runGet();
        } else {
            saveData(dataString);
        }
    }

    public String getData() {
        Log.i(TAG, "Send GET request");
        User user = syncViewModel.loadUserDataNotLive();
        // to prevent race condition
        while (user == null) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            user = syncViewModel.loadUserDataNotLive();
        }
        try {
            JSONObject userPrefs = new JSONObject(user.getPrefs());
            String lemmaLang = userPrefs.getString("lemma_lang");
            String senseLang = userPrefs.getString("sense_lang");
            try {
                return HttpConn.sendGet("https://como.kristbaum.org/como-app/v2/data/?" +
                        "uuid=" + MainViewModel.getUniqueID() +
                        "&lemma=" + lemmaLang +
                        "&sense=" + senseLang);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return "";
    }

    private void saveData(String dataString) {
        try {
            JSONObject obj = new JSONObject(dataString);

            JSONArray gameArray = obj.getJSONArray("games");
            for (int i = 0; i < gameArray.length(); i++) {
                JSONObject gameObj = gameArray.getJSONObject(i);
                JSONObject dataObj = gameObj.getJSONObject("data");
                String lemma_lang = dataObj.getJSONObject("lemma").getString("xml:lang");
                String sense_lang = dataObj.getJSONObject("sense").getString("xml:lang");
                Game game = new Game(
                        gameObj.getInt("id"),
                        dataObj.toString(),
                        lemma_lang,
                        sense_lang);
                syncViewModel.insertGame(game);
            }

            JSONArray senseCreationsArray = obj.getJSONArray("sense_creations");
            for (int i = 0; i < senseCreationsArray.length(); i++) {
                JSONObject senseCreationObj = senseCreationsArray.getJSONObject(i);
                JSONObject dataObj = senseCreationObj.getJSONObject("data");
                String lemma_lang = dataObj.getJSONObject("lemma").getString("xml:lang");
                SenseCreation senseCreation = new SenseCreation(
                        senseCreationObj.getInt("id"),
                        dataObj.toString(),
                        lemma_lang);
                syncViewModel.insertSenseCreation(senseCreation);
            }

            JSONObject userObjServer = obj.getJSONObject("user");
            User user = syncViewModel.loadUserDataNotLive();
            JSONObject userObj = new JSONObject(user.getStats());
            userObj.put("points", userObjServer.getInt("points"));
            user.setStats(userObj.toString());
            syncViewModel.updateUser(user);

        } catch (JSONException e) {
            Log.e(TAG, "Saving data from GET failed");
        }
    }
}