import 'package:flutter/material.dart';
import 'package:just_audio/just_audio.dart';
import 'package:url_launcher/url_launcher.dart';

class AudioCardWidget extends StatefulWidget {
  final String title;
  final Uri audioUri;
  final String audioFilename;
  final Uri audioFilenameUrl;

  const AudioCardWidget({
    super.key,
    required this.title,
    required this.audioUri,
    required this.audioFilename,
    required this.audioFilenameUrl,
  });

  @override
  AudioCardWidgetState createState() => AudioCardWidgetState();
}

class AudioCardWidgetState extends State<AudioCardWidget> {
  late AudioPlayer _player;
  bool isPlaying = false;

  @override
  void initState() {
    super.initState();
    _player = AudioPlayer();
    _player.setUrl(widget.audioUri.toString());
  }

  @override
  void dispose() {
    _player.dispose();
    super.dispose();
  }

  void _togglePlayback() async {
    if (isPlaying) {
      await _player.pause();
    } else {
      await _player.play();
    }
    setState(() {
      isPlaying = !isPlaying;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      child: ListTile(
        title: Text(
          widget.title,
          style: const TextStyle(
            fontSize: 12.0,
            color: Colors.grey,
          ),
        ),
        subtitle: Column(
          children: [
            InkWell(
              onTap: () => launchUrl(widget.audioFilenameUrl),
              child: Text(
                widget.audioFilename,
                style: const TextStyle(color: Colors.blue),
                textAlign: TextAlign.center,
                overflow: TextOverflow.ellipsis,
                maxLines: 2,
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                IconButton(
                  icon: Icon(isPlaying ? Icons.pause : Icons.play_arrow),
                  onPressed: _togglePlayback,
                ),
                IconButton(
                  icon: const Icon(Icons.stop),
                  onPressed: () async {
                    await _player.stop();
                    setState(() => isPlaying = false);
                  },
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
