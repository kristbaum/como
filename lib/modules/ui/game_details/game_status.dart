import 'package:como/login.dart';
import 'package:como/model/app_user.dart';
import 'package:como/model/game.dart';
import 'package:como/modules/utils.dart';
import 'package:como/pages/game_play.dart';
import 'package:flutter/material.dart';
import 'package:como/l10n/app_localizations.dart';

class GameStatus extends StatelessWidget {
  final Game game;
  final AppUser user;

  const GameStatus({
    super.key,
    required this.game,
    required this.user,
  });

  @override
  Widget build(BuildContext context) {
    if (!game.isActive) {
      return Center(
        child: Text(AppLocalizations.of(context)!
            .this_game_is_not_yet_active_or_finished),
      );
    }

    return Center(
      child: Column(
        spacing: 16,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          getGameStatusText(context, game, user),
          ElevatedButton(
            onPressed: checkStartButtonActive(game, user)
                ? () => _navigateToGamePlay(context)
                : null,
            style: ElevatedButton.styleFrom(
              textStyle: const TextStyle(fontSize: 30),
              padding: const EdgeInsets.all(16),
            ),
            child: Text(AppLocalizations.of(context)!.start),
          ),
        ],
      ),
    );
  }

  Widget getGameStatusText(BuildContext context, Game game, AppUser user) {
    if (!game.isActive) {
      return Text(AppLocalizations.of(context)!
          .this_game_is_not_yet_active_or_finished);
    }

    if (user.isWikimediaUser()) {
      return const SizedBox.shrink();
    }

    if (game.confidenceType == "S" || game.confidenceType == "D") {
      // For games that allow anonymous users
      if (user.isAnonymous()) {
        return Column(
          spacing: 16,
          children: [
            Text(AppLocalizations.of(context)!
                .you_need_to_be_logged_in_to_play_this_game),
            ElevatedButton(
              onPressed: () => {signUp(), _navigateToGamePlay(context)},
              child: Text("Play anonymously"),
            ),
            LoginButton()
          ],
        );
      } else {
        return LoginButton();
      }
    } else {
      if (user.isWikimediaUser()) {
        return const SizedBox.shrink();
      } else {
        return Column(
          spacing: 16,
          children: [
            Text(AppLocalizations.of(context)!
                .you_need_to_be_logged_in_to_play_this_game),
            LoginButton()
          ],
        );
      }
    }
  }

  void _navigateToGamePlay(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => GamePlayPage(game: game),
      ),
    );
  }
}

bool checkStartButtonActive(Game game, AppUser user) {
  return game.isActive &&
      !user.isAnonymous() &&
      (user.isWikimediaUser() ||
          game.confidenceType == "S" ||
          game.confidenceType == "D");
}
