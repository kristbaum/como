import 'package:json_annotation/json_annotation.dart';
part 'leaderboard_entry.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class LeaderboardEntry {
  LeaderboardEntry(this.username, this.profileLink, this.responsesCount);
  String username;
  String? profileLink;
  int responsesCount;

  factory LeaderboardEntry.fromJson(Map<String, dynamic> json) =>
      _$LeaderboardEntryFromJson(json);
}
