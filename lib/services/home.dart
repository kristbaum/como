import 'dart:convert';
import 'package:como/config.dart';
import 'package:como/model/game.dart';
import 'package:http/http.dart';

Future<List<Game>> fetchGamesList(String mode, bool showInactive) async {
  try {
    Response response = await client.get(
        Uri.parse(
            '$backendUrl/api/games/list/?mode=$mode&include_inactive=$showInactive'),
        headers: defaultRequestHeaders);

    if (response.statusCode == 200) {
      var decodedJSON = jsonDecode(response.body) as List<dynamic>;
      return decodedJSON
          .map<Game>((game) => Game.fromJson(game as Map<String, dynamic>))
          .toList();
    } else {
      log.warning('Failed to fetch games');
      return [];
    }
  } catch (e) {
    log.severe('Error fetching games list: $e');
    throw Exception(e);
  }
}

Future<Game?> fetchLastPlayedGame() async {
  try {
    List<Game> games = await fetchGamesList("recent", false);
    return games.isNotEmpty ? games.first : null;
  } catch (e) {
    log.severe('Error fetching the last played game: $e');
    throw Exception('Failed to fetch the last played game');
  }
}

Future<Map<String, int>> fetchGlobalStats() async {
  try {
    Response response = await client.get(
        Uri.parse('$backendUrl/api/stats/'),
        headers: defaultRequestHeaders);

    if (response.statusCode == 200) {
      var decodedJSON = jsonDecode(response.body) as Map<String, dynamic>;
      user.missingForStreakToday = decodedJSON['missing_rounds_today'] ?? 10;
      return {
        'done_prompts': decodedJSON['done_prompts'] ?? 0,
        'your_prompts': decodedJSON['your_prompts'] ?? 0,
        'streak': decodedJSON['streak'] ?? 0,
        'missing_rounds_today': user.missingForStreakToday,
      };
    } else {
      log.warning('Failed to fetch global statistics');
      return {};
    }
  } catch (e) {
    log.severe('Error fetching global statistics: $e');
    throw Exception('Failed to fetch global statistics');
  }
}
