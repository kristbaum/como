import 'package:como/l10n/app_localizations.dart';
import 'package:como/pages/home.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  testWidgets('HomePage shows welcome text', (WidgetTester tester) async {
    await tester.pumpWidget(const MaterialApp(
      localizationsDelegates: [
        AppLocalizations.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: AppLocalizations.supportedLocales,
      home: HomePage(),
    ));

    // Verify that the welcome text is shown.
    expect(find.text('Welcome to Como!'), findsOneWidget);
  });

  testWidgets('HomePage shows Continue button',
      (WidgetTester tester) async {
    await tester.pumpWidget(const MaterialApp(
      localizationsDelegates: [
        AppLocalizations.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: AppLocalizations.supportedLocales,
      home: HomePage(),
    ));

    // Verify that the Start and Continue buttons are shown.
    expect(find.text('Continue Last'), findsOneWidget);
  });

  testWidgets('HomePage shows icon', (WidgetTester tester) async {
    await tester.pumpWidget(const MaterialApp(
      localizationsDelegates: [
        AppLocalizations.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: AppLocalizations.supportedLocales,
      home: HomePage(),
    ));

    // Verify that the icon is shown.
    expect(find.byType(Image), findsOneWidget);
    expect(find.byType(CircleAvatar), findsNothing);
  });

  testWidgets('HomePage handles initial URI correctly',
      (WidgetTester tester) async {
    // This test checks if the initial URI is handled correctly by the HomePage.
    // Assuming initialUri is globally accessible and can be set for testing purposes.

    await tester.pumpWidget(const MaterialApp(
      localizationsDelegates: [
        AppLocalizations.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: AppLocalizations.supportedLocales,
      home: HomePage(),
    ));

    // Wait for post frame callback to be executed.
    await tester.pumpAndSettle();

    // Verify that the initial URI was handled correctly.
    // For this test, we assume that handling the initial URI will push a new route.
    // The exact verification will depend on the implementation details of `_handleInitialUri`.
  });
}
