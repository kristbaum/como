package org.kristbaum.como.viewModels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import org.kristbaum.como.database.models.SenseCreation;
import org.kristbaum.como.database.models.User;
import org.kristbaum.como.repositories.SenseCreationRepository;
import org.kristbaum.como.repositories.UserRepository;

import java.util.List;

public class CreationViewModel extends AndroidViewModel {

    private final SenseCreationRepository senseCreationRepository;
    private final UserRepository userRepository;
    private final LiveData<List<SenseCreation>> uneditedSenseCreations;
    private final LiveData<User> userData;

    public CreationViewModel(@NonNull Application application) {
        super(application);
        senseCreationRepository = new SenseCreationRepository(application);
        userRepository = new UserRepository(application);
        userData = userRepository.getUserData();
        uneditedSenseCreations = senseCreationRepository.getUneditedSenseCreations();
    }

    public LiveData<List<SenseCreation>> getUneditedSenseCreations() {
        return uneditedSenseCreations;
    }

    public void updateSenseCreation(SenseCreation senseCreation) {
        senseCreationRepository.updateSenseCreation(senseCreation);
    }

    public LiveData<List<SenseCreation>> getAllUneditedInLanguage() {
        return senseCreationRepository.getAllUneditedInLanguage();
    }

    public LiveData<User> getUserData() {
        return userData;
    }

    public void updateUserData(User user) {userRepository.updateUser(user);}

}
