package org.kristbaum.como.viewModels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import org.kristbaum.como.client.Invoker;
import org.kristbaum.como.database.models.User;
import org.kristbaum.como.repositories.UserRepository;

import java.util.concurrent.Executor;

public class SettingsViewModel extends AndroidViewModel {
    private final SyncViewModel syncViewModel;
    private final LiveData<User> userData;
    private final UserRepository userRepository;
    final private String defaultLang = "de";

    public SettingsViewModel(@NonNull Application application) {
        super(application);
        userRepository = new UserRepository(application);
        userData = userRepository.getUserData();
        syncViewModel = new SyncViewModel(application);
    }

    public LiveData<User> getUserData() {
        return userData;
    }

    public void updateUser(User user) {
        userRepository.updateUser(user);
        sync();
    }
    public void sync() {
        Executor executor = new Invoker();
        executor.execute(new Runnable() {
            @Override
            public void run() {
                syncViewModel.syncData();
            }
        });
    }
}
