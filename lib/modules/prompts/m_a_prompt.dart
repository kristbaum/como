import 'package:como/config.dart';
import 'package:como/model/prompts/m_prompt.dart';
import 'package:como/modules/prompts/base_prompt.dart';
import 'package:como/modules/responses/build_response.dart';
import 'package:como/modules/ui/audio_card_widget.dart';
import 'package:como/modules/utils.dart';
import 'package:como/services/game_play.dart';
import 'package:flutter/material.dart';

class APromptModule extends BasePromptModule<MPrompt> {
  const APromptModule({super.key, required super.game});

  @override
  APromptModuleState createState() => APromptModuleState();
}

class APromptModuleState extends BasePromptModuleState<MPrompt> {
  @override
  Future<List<MPrompt>> addNewPrompts() async {
    return await getIPrompts(widget.game);
  }

  @override
  Widget buildPromptContent(MPrompt prompt) {
    if (prompts.length > 1) {
      log.info("Precaching audio somehow?: ${prompts[1].media1}");
    }
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Flexible(
          child: SingleChildScrollView(
            child: Center(
              child: ConstrainedBox(
                constraints: const BoxConstraints(maxWidth: appWidth),
                child: ListView(
                  physics: const NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  children: [
                    questionWidget(prompt.question),
                    const SizedBox(height: 10),
                    AudioCardWidget(
                      title: addPromptLabel(0, widget.game.promptLabelsList),
                      audioUri: prompt.media1,
                      audioFilename: prompt.media1Filename,
                      audioFilenameUrl: prompt.media1FilenameUrl,
                    ),
                    const SizedBox(height: 10),
                  ],
                ),
              ),
            ),
          ),
        ),
        buildResponse(nextPrompt, widget.game.responseType, widget.game, prompt,
            addPromptLabel(1, widget.game.promptLabelsList)),
      ],
    );
  }
}
