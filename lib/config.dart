import 'package:como/model/app_user.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
import 'package:logging/logging.dart';

String backendUrl = 'http://localhost:8000';
var client = http.Client();

Map<String, String> defaultRequestHeaders = {
  'Content-type': 'application/json',
  'Authorization': user.token == null ? '' : "Token ${user.token}",
};

final log = Logger('DacitLogger');

FlutterSecureStorage storage = const FlutterSecureStorage();

AppUser user = AppUser();

const double appWidth = 840;
const double streakTarget = 10;
