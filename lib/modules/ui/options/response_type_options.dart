import 'package:flutter/material.dart';

class ResponseTypeOptions extends StatefulWidget {
  final String responseType;
  final ValueChanged<String> onChanged;

  const ResponseTypeOptions({
    super.key,
    required this.responseType,
    required this.onChanged,
  });

  @override
  ResponseTypeOptionsState createState() => ResponseTypeOptionsState();
}

class ResponseTypeOptionsState extends State<ResponseTypeOptions> {
  late String _responseType;

  @override
  void initState() {
    super.initState();
    _responseType = widget.responseType;
  }

  Widget _buildOption(String title, String value) {
    return ListTile(
      title: Text(title),
      leading: Radio<String>(
        value: value,
        groupValue: _responseType,
        onChanged: (String? newValue) {
          setState(() {
            _responseType = newValue!;
          });
          widget.onChanged(_responseType);
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    const options = {
      'YN': 'Yes/No',
      'SC': 'Single choice',
      'ET': 'Editable text',
    };

    return Wrap(
      alignment: WrapAlignment.center,
      children: options.entries
          .map((entry) => _buildOption(entry.value, entry.key))
          .toList(),
    );
  }
}
