import 'package:como/config.dart';
import 'package:como/services/game_details.dart';
import 'package:flutter/material.dart';
import 'package:como/l10n/app_localizations.dart';
import 'package:flutter_linkify/flutter_linkify.dart';
import 'package:url_launcher/url_launcher.dart';

class LongDescriptionCard extends StatelessWidget {
  final String text;
  final bool isEditable;
  final String gameSlug;

  const LongDescriptionCard({
    super.key,
    required this.text,
    required this.isEditable,
    required this.gameSlug,
  });

  @override
  Widget build(BuildContext context) {
    return Card(
      child: ListTile(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(AppLocalizations.of(context)!.description),
            if (isEditable)
              IconButton(
                icon: Icon(Icons.edit),
                onPressed: () {
                  _showEditDialog(context, gameSlug);
                },
              ),
          ],
        ),
        subtitle: Linkify(
          onOpen: (link) async {
            if (!await launchUrl(Uri.parse(link.url))) {
              log.info('Could not launch ${link.url}');
            }
          },
          text: text,
        ),
      ),
    );
  }

  void _showEditDialog(BuildContext context, String gameSlug) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        TextEditingController controller = TextEditingController(text: text);
        return AlertDialog(
          title: Text(AppLocalizations.of(context)!.editDescription),
          content: TextField(
            controller: controller,
            maxLines: null,
            decoration: InputDecoration(
              hintText: AppLocalizations.of(context)!.enterDescription,
            ),
          ),
          actions: [
            TextButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Text(MaterialLocalizations.of(context).cancelButtonLabel),
            ),
            ElevatedButton(
              onPressed: () {
                updateGameField(
                    context, gameSlug, {"long_description": controller.text});
                Navigator.of(context).pop();
              },
              child: Text(MaterialLocalizations.of(context).saveButtonLabel),
            ),
          ],
        );
      },
    );
  }
}
