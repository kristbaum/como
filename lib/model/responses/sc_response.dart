import 'dart:convert';

import 'package:como/config.dart';
import 'package:como/model/game.dart';
import 'package:como/model/responses/como_response.dart';

class SCResponse extends ComoResponse {
  SCResponse({
    required super.skipped,
    required this.selected,
  });
  final int selected;

  Map<String, dynamic> toJson() {
    return {
      'skipped': skipped,
      'selected': selected,
    };
  }
}

Future<void> sendSCResponse(
  Game game,
  int promptId,
  bool skipped,
  int selected,
) async {
  log.info("Sending SC response");
  var response = SCResponse(
    skipped: skipped,
    selected: selected,
  );

  log.info(jsonEncode(response.toJson()));
  sendResponse(game.slug, promptId, response.toJson());
}
