import 'package:como/config.dart';
import 'package:como/model/game.dart';
import 'package:como/modules/ui/game_details/advanced_details.dart';
import 'package:como/modules/ui/game_details/data_download_card.dart';
import 'package:como/modules/ui/game_details/leaderboard.dart';
import 'package:como/modules/ui/game_details/game_status.dart';
import 'package:como/modules/ui/game_details/long_description_card.dart';
import 'package:como/modules/ui/game_details/stat_widget.dart';
import 'package:como/services/game_details.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:share_plus/share_plus.dart';
import 'package:como/l10n/app_localizations.dart';

class GameDetailsPage extends StatefulWidget {
  final String gameSlug;

  const GameDetailsPage({super.key, required this.gameSlug});

  @override
  GameDetailsPageState createState() => GameDetailsPageState();
}

class GameDetailsPageState extends State<GameDetailsPage> {
  late Game game;
  bool _showAdvanced = false;
  bool _isAllowedToEdit = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: null,
        actions: [
          _buildPopupMenu(),
        ],
      ),
      body: RefreshIndicator(
        onRefresh: _refreshGameDetails,
        child: FutureBuilder<Game>(
          future: fetchGameData(widget.gameSlug),
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return const Center(child: CircularProgressIndicator());
            } else if (snapshot.hasError) {
              return Center(
                  child: Text(
                      '${AppLocalizations.of(context)!.error}: ${snapshot.error}'));
            }
            game = snapshot.data!;
            if (game.user == user.wikimediaUsername) {
              _isAllowedToEdit = true;
              log.info("User is allowed to edit game details");
            }
            return _buildGameDetails();
          },
        ),
      ),
    );
  }

  PopupMenuButton<String> _buildPopupMenu() {
    return PopupMenuButton<String>(
      onSelected: _handleMenuSelection,
      itemBuilder: (BuildContext context) => <PopupMenuEntry<String>>[
        _buildPopupMenuItem('show_advanced', Icons.settings,
            AppLocalizations.of(context)!.show_advanced),
        _buildPopupMenuItem(
            game.isActive ? 'set_inactive' : 'set_active',
            game.isActive ? Icons.lock : Icons.lock_open,
            game.isActive
                ? AppLocalizations.of(context)!.set_inactive_as_admin
                : AppLocalizations.of(context)!.set_active_as_admin),
        _buildPopupMenuItem('share_game', Icons.share,
            AppLocalizations.of(context)!.share_this_game, onTap: () {
          context.pop();
          Share.share('https://como.toolforge.org/#/game/${game.slug}');
        }),
        _buildPopupMenuItem(
            'delete_game', Icons.delete, "Delete Game (as Admin)", onTap: () {
          context.pop();
          deleteGame(context, game.slug);
        }),
      ],
    );
  }

  PopupMenuItem<String> _buildPopupMenuItem(
      String value, IconData icon, String text,
      {VoidCallback? onTap}) {
    return PopupMenuItem<String>(
      value: value,
      child: ListTile(
        leading: Icon(icon),
        title: Text(text),
        onTap: onTap,
      ),
    );
  }

  Future<void> _refreshGameDetails() async {
    Game newGame = await fetchGameData(widget.gameSlug);
    setState(() => game = newGame);
  }

  SingleChildScrollView _buildGameDetails() {
    return SingleChildScrollView(
      child: Center(
        child: ConstrainedBox(
          constraints: const BoxConstraints(maxWidth: appWidth),
          child: SelectionArea(
            child: ListView(
              padding: const EdgeInsets.all(8),
              physics: const NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              children: _buildGameDetailWidgets(),
            ),
          ),
        ),
      ),
    );
  }

  List<Widget> _buildGameDetailWidgets() {
    return [
      Center(
          child: Text(game.name,
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.displaySmall)),
      const SizedBox(height: 20),
      Text(game.shortDescription, textAlign: TextAlign.center),
      const SizedBox(height: 20),
      GameStatus(game: game, user: user),
      const SizedBox(height: 20),
      LongDescriptionCard(
          text: game.longDescription,
          isEditable: _isAllowedToEdit,
          gameSlug: game.slug),
      GameDetailStatWidget(widget: widget),
      Leaderboard(widget: widget),
      DataManagementCard(
          gameSlug: game.slug, suggestedCSVHeaders: game.suggestedCSVHeaders),
      if (_showAdvanced)
        AdvancedDetails(game: game, isEditable: _isAllowedToEdit),
    ];
  }

  Future<void> _handleMenuSelection(String value) async {
    switch (value) {
      case 'show_advanced':
        setState(() => _showAdvanced = !_showAdvanced);
        break;
      case 'set_active':
      case 'set_inactive':
        toggleActiveState(game.slug, game.isActive, context);
        Game newGame = await fetchGameData(widget.gameSlug);
        setState(() => game = newGame);
        break;
    }
  }
}
