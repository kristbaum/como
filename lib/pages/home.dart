import 'package:como/config.dart';
import 'package:como/model/game.dart';
import 'package:como/modules/ui/app_bar.dart';
import 'package:como/modules/ui/drawer.dart';
import 'package:como/modules/ui/game_row.dart';
import 'package:como/modules/ui/home/continue_button.dart';
import 'package:como/modules/ui/home/counter.dart';
import 'package:como/modules/ui/home/home_icon.dart';
import 'package:como/services/home.dart';
import 'package:flutter/material.dart';
import 'package:como/l10n/app_localizations.dart';
import 'package:url_launcher/url_launcher.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  HomePageState createState() => HomePageState();
}

class HomePageState extends State<HomePage> {
  final GlobalKey<ScaffoldState> _homeKey = GlobalKey();
  Future<Game?>? _lastPlayedGameFuture;
  final Map<String, Future<List<Game>>?> _gameListCache = {};
  Future<Map<String, int>>? _globalStatsFuture;

  @override
  void initState() {
    super.initState();
    _lastPlayedGameFuture = fetchLastPlayedGame();
    _globalStatsFuture = fetchGlobalStats();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: getComoAppBar(context, "Como"),
      key: _homeKey,
      drawer: drawer(context),
      body: SingleChildScrollView(
        child: Center(
          child: ConstrainedBox(
            constraints: const BoxConstraints(maxWidth: appWidth),
            child: ListView(
              physics: const NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              children: [
                ComoIcon(),
                const SizedBox(height: 20),
                Center(
                    child: Text(AppLocalizations.of(context)!.welcome_to_como)),
                Center(
                    child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(AppLocalizations.of(context)!
                            .play_to_increase_knowledge))),
                const SizedBox(height: 20),
                Center(
                  child: GestureDetector(
                    onTap: () {
                      launchUrl(
                          Uri.parse("https://meta.wikimedia.org/wiki/Como"));
                    },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text("What is this?",
                            style: const TextStyle(
                              fontWeight: FontWeight.w500,
                              fontSize: 22,
                              color: Colors.blue,
                              decoration: TextDecoration.underline,
                            )),
                        const SizedBox(width: 5),
                        const Icon(Icons.arrow_outward, color: Colors.blue),
                      ],
                    ),
                  ),
                ),
                const SizedBox(height: 10),
                ContinueButton(lastPlayedGameFuture: _lastPlayedGameFuture),
                const SizedBox(height: 20),
                const Divider(),
                _buildStatsSection(),
                const SizedBox(height: 20),
                _buildGameRow(
                    AppLocalizations.of(context)!.continue_recent_games,
                    'recent'),
                _buildGameRow(
                    AppLocalizations.of(context)!.popular_games, 'popular'),
                _buildGameRow(
                    AppLocalizations.of(context)!.newest_games, 'new'),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildGameRow(String title, String mode) {
    return GameRow(
      rowTitle: title,
      rowMode: mode,
      gameListFuture: _fetchGames(mode),
    );
  }

  Future<List<Game>> _fetchGames(String mode) {
    if (_gameListCache.containsKey(mode)) {
      return _gameListCache[mode]!;
    } else {
      final futureGames = fetchGamesList(mode, false);
      _gameListCache[mode] = futureGames;
      return futureGames;
    }
  }

  Widget _buildStatsSection() {
    return FutureBuilder<Map<String, int>>(
      future: _globalStatsFuture,
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return const Center(child: CircularProgressIndicator());
        } else if (snapshot.hasError) {
          return const Center(child: Text('Failed to load stats'));
        } else if (snapshot.hasData) {
          final stats = snapshot.data!;
          return Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Counter(title: 'Streak', count: stats['streak'] ?? 0),
              Counter(
                  title: 'Missing rounds',
                  count: stats['missing_rounds_today'] ?? 0),
              Counter(title: 'You', count: stats['your_prompts'] ?? 0),
              Counter(title: 'Everyone', count: stats['done_prompts'] ?? 0),
            ],
          );
        }
        return const SizedBox.shrink();
      },
    );
  }
}
