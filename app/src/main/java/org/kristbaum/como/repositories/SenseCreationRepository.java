package org.kristbaum.como.repositories;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import org.kristbaum.como.database.AppDatabase;
import org.kristbaum.como.database.Daos.SenseCreationDao;
import org.kristbaum.como.database.models.SenseCreation;
import org.kristbaum.como.viewModels.MainViewModel;

import java.util.List;

public class SenseCreationRepository {

    private final SenseCreationDao senseCreationDao;
    private final LiveData<List<SenseCreation>> uneditedSenseCreations;


    public SenseCreationRepository(Application application) {
        AppDatabase database = AppDatabase.getInstance(application);
        senseCreationDao = database.senseCreationDao();

        uneditedSenseCreations = senseCreationDao.getAllUnedited();
    }

    public LiveData<List<SenseCreation>> getUneditedSenseCreations() {
        return uneditedSenseCreations;
    }

    public List<SenseCreation> getAllCreatedNotTransferred() {
        return senseCreationDao.getAllCreatedNotTransferred();
    }

    public LiveData<List<SenseCreation>> getAllUneditedInLanguage() {
        return senseCreationDao.getAllUneditedInLanguage(MainViewModel.getLemma_lang());
    }

    public void updateSenseCreation (SenseCreation senseCreation) {
        new updateSenseCreationAsyncTask(senseCreationDao).execute(senseCreation);
    }

    private static class updateSenseCreationAsyncTask extends AsyncTask<SenseCreation, Void, Void> {
        private final SenseCreationDao senseCreationDao;

        private updateSenseCreationAsyncTask(SenseCreationDao senseCreationDao) {
            this.senseCreationDao = senseCreationDao;
        }

        @Override
        protected Void doInBackground(SenseCreation... senseCreations) {
            senseCreationDao.update(senseCreations[0]);
            return null;
        }
    }

    public void insertSenseCreation(SenseCreation senseCreation) {
        new insertSenseCreationAsyncTask(senseCreationDao).execute(senseCreation);
    }

    private static class insertSenseCreationAsyncTask extends AsyncTask<SenseCreation, Void, Void> {
        private final SenseCreationDao senseCreationDao;

        private insertSenseCreationAsyncTask(SenseCreationDao senseCreationDao) {
            this.senseCreationDao = senseCreationDao;
        }

        @Override
        protected Void doInBackground(SenseCreation... senseCreations) {
            senseCreationDao.insert(senseCreations[0]);
            return null;
        }
    }

    public void deleteSenseCreation(SenseCreation senseCreation) {
        new deleteSenseCreationAsyncTask(senseCreationDao).execute(senseCreation);
    }

    private static class deleteSenseCreationAsyncTask extends AsyncTask<SenseCreation, Void, Void> {
        private final SenseCreationDao senseCreationDao;

        private deleteSenseCreationAsyncTask(SenseCreationDao senseCreationDao) {
            this.senseCreationDao = senseCreationDao;
        }

        @Override
        protected Void doInBackground(SenseCreation... senseCreations) {
            senseCreationDao.delete(senseCreations[0]);
            return null;
        }
    }
}
