import 'package:como/model/game.dart';
import 'package:como/modules/ui/game_details/text_detail_card.dart';
import 'package:flutter/material.dart';
import 'package:como/l10n/app_localizations.dart';

class AdvancedDetails extends StatelessWidget {
  final Game game;
  final bool isEditable;

  const AdvancedDetails({
    super.key,
    required this.game,
    required this.isEditable,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const Divider(),
        TextDetailCard(
            title: "${AppLocalizations.of(context)!.user}: ",
            content: game.userpage.toString(),
            isEditable: false,
            gameSlug: game.slug,
            fieldName: 'userpage'),
        TextDetailCard(
          title:
              "${AppLocalizations.of(context)!.question_with_placeholders}: ",
          content: game.questionWithJokers,
          isEditable: isEditable,
          gameSlug: game.slug,
          fieldName: 'questionWithJokers',
        ),
        TextDetailCard(
            title: "${AppLocalizations.of(context)!.attached_requests}: ",
            content: game.actionRequestsWithPlaceholders.toString(),
            isEditable: false,
            gameSlug: game.slug,
            fieldName: ''),
        TextDetailCard(
          title: "${AppLocalizations.of(context)!.prompt_labels}: ",
          content: game.promptLabelsList.toString(),
          isEditable: isEditable,
          gameSlug: game.slug,
          fieldName: 'prompt_labels_list',
        ),
        TextDetailCard(
            title: "${AppLocalizations.of(context)!.prompt_type}: ",
            content: game.promptType,
            isEditable: false,
            gameSlug: game.slug,
            fieldName: ''),
        TextDetailCard(
            title: "${AppLocalizations.of(context)!.response_type}: ",
            content: game.responseType,
            isEditable: false,
            gameSlug: game.slug,
            fieldName: ''),
        TextDetailCard(
            title: "${AppLocalizations.of(context)!.action_type}: ",
            content: game.actionType ?? AppLocalizations.of(context)!.none,
            isEditable: false,
            gameSlug: game.slug,
            fieldName: ''),
        TextDetailCard(
            title: "${AppLocalizations.of(context)!.confidence_type}: ",
            content: game.confidenceType,
            isEditable: false,
            gameSlug: game.slug,
            fieldName: ''),
      ],
    );
  }
}
