import 'package:como/l10n/app_localizations.dart';
import 'package:como/services/game_create.dart';
import 'package:file_selector/file_selector.dart';
import 'package:flutter/material.dart';

class UploadDialog extends StatefulWidget {
  final String suggestedCSVHeaders;
  final String slug;

  const UploadDialog(
      {super.key, required this.slug, required this.suggestedCSVHeaders});

  @override
  UploadDialogState createState() => UploadDialogState();
}

class UploadDialogState extends State<UploadDialog> {
  bool isUploading = false;
  bool _fileSelected = false;
  String _fileButtonLabel = "Select File";
  String _dataFromFile = "";

  Future<void> _openTextFile() async {
    const typeGroup = XTypeGroup(label: 'text', extensions: ['csv']);
    final file = await openFile(acceptedTypeGroups: [typeGroup]);
    if (file == null) return;

    final fileContent = await file.readAsString();
    setState(() {
      _dataFromFile = fileContent;
      _fileButtonLabel = file.name;
      _fileSelected = true;
    });
  }

  Future<void> _uploadFile() async {
    setState(() {
      isUploading = true;
    });
    try {
      await createPrompts(widget.slug, _dataFromFile);
      if (mounted) Navigator.of(context).pop();
    } catch (error) {
      // Handle error
    } finally {
      setState(() {
        isUploading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(AppLocalizations.of(context)!.upload_csv),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(
            "These are the headers we expect in the CSV file ('?' means they are optional):",
          ),
          Text(widget.suggestedCSVHeaders),
          const SizedBox(height: 20),
          Row(
            children: [
              Expanded(
                child: ElevatedButton.icon(
                  onPressed: _openTextFile,
                  icon: const Icon(Icons.folder_open),
                  label: Text(_fileButtonLabel), // Dynamic label
                ),
              ),
              const SizedBox(width: 10),
              Expanded(
                child: ElevatedButton.icon(
                  onPressed: _fileSelected ? _uploadFile : null,
                  icon: isUploading
                      ? const SizedBox(
                          width: 20,
                          height: 20,
                          child: CircularProgressIndicator(),
                        )
                      : const Icon(Icons.cloud_upload),
                  label: Text(AppLocalizations.of(context)!.start_upload),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
