import 'package:como/config.dart';
import 'package:como/modules/ui/app_bar.dart';
import 'package:como/modules/ui/game_create/option_card.dart';
import 'package:como/modules/ui/game_create/radio_card.dart';
import 'package:como/modules/ui/game_create/text_field_card.dart';
import 'package:como/modules/ui/options/confidence_type_options.dart';
import 'package:como/modules/ui/options/prompt_type_options.dart';
import 'package:como/modules/ui/options/response_type_options.dart';
import 'package:como/modules/utils.dart';
import 'package:como/services/game_create.dart';
import 'package:flutter/material.dart';
import 'package:como/l10n/app_localizations.dart';
import 'package:go_router/go_router.dart';
import 'package:slugify/slugify.dart';

class GameCreatePage extends StatefulWidget {
  const GameCreatePage({super.key});

  @override
  GameCreatePageState createState() => GameCreatePageState();
}

class GameCreatePageState extends State<GameCreatePage> {
  final _formKey = GlobalKey<FormState>();
  final _nameController = TextEditingController();
  final _shortDescriptionController = TextEditingController();
  final _longDescriptionController = TextEditingController();
  final _questionController = TextEditingController();
  final _labelController = TextEditingController();

  final _requestHTTPMethods = <String>[];
  final _requestURLControllers = <TextEditingController>[];
  final _requestBodyControllers = <TextEditingController>[];

  String _promptType = 'TT';
  String _responseType = 'YN';
  String _confidence = 'S';
  String _actionType = 'none';
  bool _listPublicly = true;

  bool _submitPressed = false;

  List<Card> requestCards = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: getComoAppBar(context, AppLocalizations.of(context)!.create_game),
      floatingActionButton: user.isWikimediaUser()
          ? FloatingActionButton.extended(
              onPressed: _handleSubmit,
              label: _submitPressed
                  ? const CircularProgressIndicator()
                  : Text(AppLocalizations.of(context)!.submit),
              icon: const Icon(Icons.send),
              backgroundColor: Colors.green,
            )
          : null,
      body: user.isWikimediaUser()
          ? SingleChildScrollView(
              child: Center(
                  child: ConstrainedBox(
                      constraints: const BoxConstraints(maxWidth: appWidth),
                      child: buildForm(context))))
          : Padding(
              padding: const EdgeInsets.all(8.0),
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(AppLocalizations.of(context)!.login_to_create_game),
                    const LoginButton(),
                  ],
                ),
              ),
            ),
    );
  }

  void _handleSubmit() {
    if (_formKey.currentState!.validate()) {
      if (_submitPressed) return;

      setState(() => _submitPressed = true);

      createGame(
        requestHTTPMethods: _requestHTTPMethods,
        requestURLControllers: _requestURLControllers,
        requestBodyControllers: _requestBodyControllers,
        name: _nameController.text,
        promptType: _promptType,
        promptLabels: _labelController.text,
        responseType: _responseType,
        shortDescription: _shortDescriptionController.text,
        longDescription: _longDescriptionController.text,
        question: _questionController.text,
        actionType: _actionType,
        listPublicly: _listPublicly,
        confidence: _confidence,
      ).then((_) {
        setState(() => _submitPressed = false);
        final slug = slugify(_nameController.text);
        if (mounted) {
          context.pop();
          context.push('/game/$slug');
          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(
                content: Text(
                    AppLocalizations.of(context)!.game_created_successfully)),
          );
        }
      }).catchError((e) {
        if (mounted) {
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Text(
                '${AppLocalizations.of(context)!.failed_to_create_game}: $e'),
            backgroundColor: Colors.red,
          ));
        }
        setState(() => _submitPressed = false);
      });
    }
  }

  Form buildForm(BuildContext context) {
    return Form(
      key: _formKey,
      child: ListView(
        shrinkWrap: true,
        physics: const NeverScrollableScrollPhysics(),
        children: [
          buildDocumentationCard(),
          TextFieldCard(
            title: AppLocalizations.of(context)!.name,
            controller: _nameController,
            hintText: AppLocalizations.of(context)!.name_hint,
            maxLength: 50,
            validatorMessage: AppLocalizations.of(context)!.enter_name,
          ),
          TextFieldCard(
            title: AppLocalizations.of(context)!.short_description,
            controller: _shortDescriptionController,
            hintText: AppLocalizations.of(context)!.short_description_hint,
            maxLength: 50,
            validatorMessage:
                AppLocalizations.of(context)!.enter_short_description,
          ),
          TextFieldCard(
            title: AppLocalizations.of(context)!.long_description,
            controller: _longDescriptionController,
            hintText: AppLocalizations.of(context)!.long_description_hint,
            maxLength: 500,
            validatorMessage:
                AppLocalizations.of(context)!.enter_long_description,
            minLines: 3,
            maxLines: 10,
          ),
          TextFieldCard(
            title: AppLocalizations.of(context)!.question,
            controller: _questionController,
            hintText: AppLocalizations.of(context)!.question_hint,
            maxLength: 50,
            validatorMessage: AppLocalizations.of(context)!.enter_question,
          ),
          RadioCard(
            context: context,
            title: AppLocalizations.of(context)!.list_publicly,
            groupValue: _listPublicly,
            options: [
              AppLocalizations.of(context)!.yes,
              AppLocalizations.of(context)!.no
            ],
            onChanged: (value) => setState(() => _listPublicly = value!),
          ),
          OptionCard(
            title: AppLocalizations.of(context)!.prompt_type,
            groupValue: _promptType,
            optionWidget: PromptTypeOptions(
              onChanged: (value) => setState(() => _promptType = value),
              promptType: _promptType,
            ),
          ),
          TextFieldCard(
            title: AppLocalizations.of(context)!.prompt_labels,
            controller: _labelController,
            hintText: AppLocalizations.of(context)!.prompt_labels_hint,
            maxLength: 100,
            validatorMessage: AppLocalizations.of(context)!.enter_prompt_labels,
          ),
          OptionCard(
            title: AppLocalizations.of(context)!.response_type,
            groupValue: _responseType,
            optionWidget: ResponseTypeOptions(
              onChanged: (value) => setState(() => _responseType = value),
              responseType: _responseType,
            ),
          ),
          OptionCard(
            title: AppLocalizations.of(context)!.confidence_type,
            groupValue: _confidence,
            optionWidget: ConfidenceOptions(
              onChanged: (value) => setState(() => _confidence = value),
              confidence: _confidence,
            ),
          ),
          buildActionTypeCard(),
          if (_actionType == 'WA' || _actionType == 'AA')
            ..._buildRequestCards(),
          if (_actionType == 'WA' || _actionType == 'AA')
            buildAddRequestButton(),
        ],
      ),
    );
  }

  Card buildActionTypeCard() {
    return Card(
      child: ListTile(
        title: Text(AppLocalizations.of(context)!.action_type),
        subtitle: Wrap(
          alignment: WrapAlignment.center,
          children: [
            buildActionTypeRadio(AppLocalizations.of(context)!.none, 'none'),
            buildActionTypeRadio(
                AppLocalizations.of(context)!.wikibase_api, 'WA'),
            buildActionTypeRadio(
                AppLocalizations.of(context)!.action_api, 'AA'),
          ],
        ),
      ),
    );
  }

  ListTile buildActionTypeRadio(String title, [String? value]) {
    return ListTile(
      title: Text(title),
      leading: Radio<String>(
        value: value ?? title,
        groupValue: _actionType,
        onChanged: (value) => setState(() => _actionType = value!),
      ),
    );
  }

  List<Widget> _buildRequestCards() {
    return List.generate(_requestHTTPMethods.length, (i) {
      return buildRequestCard(_requestHTTPMethods[i], _requestURLControllers[i],
          _requestBodyControllers[i]);
    });
  }

  ElevatedButton buildAddRequestButton() {
    return ElevatedButton(
      onPressed: () {
        setState(() {
          requestCards.add(createCard());
        });
      },
      child: Text(AppLocalizations.of(context)!.add_another_request),
    );
  }

  Card buildRequestCard(String httpMethod, TextEditingController urlController,
      TextEditingController bodyController) {
    return Card(
      child: Column(
        children: [
          buildHttpMethodDropdown(httpMethod),
          buildTextField(AppLocalizations.of(context)!.request_url,
              urlController, AppLocalizations.of(context)!.request_url_hint),
          buildTextField(AppLocalizations.of(context)!.request_body,
              bodyController, AppLocalizations.of(context)!.request_body_hint,
              minLines: 1, maxLines: 10),
        ],
      ),
    );
  }

  ListTile buildHttpMethodDropdown(String httpMethod) {
    return ListTile(
      title: Text(AppLocalizations.of(context)!.http_method),
      subtitle: Padding(
        padding: const EdgeInsets.all(8.0),
        child: DropdownButtonFormField<String>(
          value: httpMethod,
          items: ['PUT', 'PATCH', 'POST']
              .map((value) =>
                  DropdownMenuItem<String>(value: value, child: Text(value)))
              .toList(),
          onChanged: (newValue) {
            setState(() {
              _requestHTTPMethods[_requestHTTPMethods.indexOf(httpMethod)] =
                  newValue!;
            });
          },
          validator: (value) => value == null
              ? AppLocalizations.of(context)!.select_http_method
              : null,
        ),
      ),
    );
  }

  ListTile buildTextField(
      String title, TextEditingController controller, String hintText,
      {int minLines = 1, int maxLines = 1}) {
    return ListTile(
      title: Text(title),
      subtitle: TextFormField(
        controller: controller,
        minLines: minLines,
        maxLines: maxLines,
        decoration: InputDecoration(hintText: hintText),
        validator: (value) =>
            value!.isEmpty ? AppLocalizations.of(context)!.enter_value : null,
      ),
    );
  }

  Card createCard() {
    String requestHTTPMethod = 'POST';
    final TextEditingController reqeuestURLController = TextEditingController();
    final TextEditingController requestBodyController = TextEditingController();
    reqeuestURLController.text =
        "https://www.wikidata.org/w/rest.php/wikibase/v0/entities/items/%custom_id%/descriptions/en";
    // TODO: Add warning about www.
    requestBodyController.text = """{
  "description": "%response_text%",
  "bot": false
}""";

    _requestHTTPMethods.add(requestHTTPMethod);
    _requestURLControllers.add(reqeuestURLController);
    _requestBodyControllers.add(requestBodyController);

    return Card(
      child: Column(
        children: [
          ListTile(
            title: Text(AppLocalizations.of(context)!.http_method),
            subtitle: Padding(
              padding: const EdgeInsets.all(8.0),
              child: DropdownButtonFormField<String>(
                value: requestHTTPMethod,
                items: <String>['PUT', 'PATCH', 'POST']
                    .map<DropdownMenuItem<String>>((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(value),
                  );
                }).toList(),
                onChanged: (String? newValue) {
                  setState(() {
                    _requestHTTPMethods[_requestHTTPMethods
                        .indexOf(requestHTTPMethod)] = newValue!;
                    requestHTTPMethod = newValue;
                  });
                },
                validator: (value) {
                  if (value == null) {
                    return AppLocalizations.of(context)!.select_http_method;
                  }
                  return null;
                },
              ),
            ),
          ),
          ListTile(
            title: Text(AppLocalizations.of(context)!.request_url),
            subtitle: TextFormField(
              controller: reqeuestURLController,
              decoration: const InputDecoration(
                  hintText:
                      'https://www.wikidata.org/w/rest.php/wikibase/v0/entities/items/%custom_id%/descriptions/en'),
              validator: (value) {
                if (value!.isEmpty) {
                  return value;
                }
                return null;
              },
            ),
          ),
          ListTile(
            title: Text(AppLocalizations.of(context)!.request_body),
            subtitle: SizedBox(
              height: 200,
              width: 400,
              child: TextField(
                expands: true,
                minLines: null,
                maxLines: null,
                keyboardType: TextInputType.multiline,
                controller: requestBodyController,
                decoration: InputDecoration(
                    hintText: AppLocalizations.of(context)!.json_data_hint),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
