package org.kristbaum.como.database.models;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "sense_creation_table")
public class SenseCreation {

    @ColumnInfo(name = "creation_id")
    @PrimaryKey
    public Integer creation_id;

    @ColumnInfo(name = "suggestion_content")
    public String suggestion_content;

    @ColumnInfo(name = "player_input", defaultValue = "NULL")
    public String player_input;

    @ColumnInfo(name = "sense_lang")
    public String sense_lang;

    @ColumnInfo(name = "skipped", defaultValue = "FALSE")
    public Boolean skipped;

    @ColumnInfo(name = "transferred", defaultValue = "FALSE")
    public boolean transferred;

    @ColumnInfo(name = "lemma_lang")
    public String lemma_lang;

    public String getSense_lang() {
        return sense_lang;
    }

    public void setSense_lang(String sense_lang) {
        this.sense_lang = sense_lang;
    }

    public String getLemma_lang() {
        return lemma_lang;
    }

    public void setLemma_lang(String lemma_lang) {
        this.lemma_lang = lemma_lang;
    }

    public Boolean getTransferred() {
        return transferred;
    }

    public void setTransferred(Boolean transferred) {
        this.transferred = transferred;
    }

    public Boolean getSkipped() {
        return skipped;
    }

    public void setSkipped(Boolean skipped) {
        this.skipped = skipped;
    }

    public Integer getCreation_id() {
        return creation_id;
    }

    public void setCreation_id(Integer creation_id) {
        this.creation_id = creation_id;
    }

    public String getSuggestion_content() {
        return suggestion_content;
    }

    public void setSuggestion_content(String suggestion_content) {
        this.suggestion_content = suggestion_content;
    }

    public String getPlayer_input() {
        return player_input;
    }

    public void setPlayer_input(String player_input) {
        this.player_input = player_input;
    }

    public SenseCreation(int creation_id, String suggestion_content, String lemma_lang){
        this.creation_id = creation_id;
        this.suggestion_content = suggestion_content;
        this.lemma_lang = lemma_lang;
    }
}


