import 'package:como/config.dart';
import 'package:como/model/game.dart';
import 'package:como/modules/responses/m_et_response.dart';
import 'package:como/modules/responses/m_sc_response.dart';
import 'package:como/modules/responses/m_yn_response.dart';
import 'package:flutter/material.dart';

Widget buildResponse(Function(String) nextPrompt, String responseType,
    Game game, prompt, String leadingFigure) {
  Widget responseWidget =
      _getResponseWidget(responseType, nextPrompt, game, prompt, leadingFigure);

  return ConstrainedBox(
    constraints: const BoxConstraints(maxWidth: appWidth),
    child: responseWidget,
  );
}

Widget _getResponseWidget(String responseType, Function(String) nextPrompt,
    Game game, prompt, String leadingFigure) {
  switch (responseType) {
    case "YN":
      return buildYNResponse(nextPrompt, game, prompt.id);
    case "SC":
      return SCResponseWidget(
          nextPrompt: nextPrompt,
          game: game,
          promptId: prompt.id,
          responseOptions: prompt.responseOptions);
    case "ET":
      return ETResponseWidget(
          nextPrompt: nextPrompt,
          game: game,
          promptId: prompt.id,
          responseOption: prompt.responseOptions[0],
          leadingFigure: leadingFigure);
    default:
      return const Text("Unknown Response Type");
  }
}
