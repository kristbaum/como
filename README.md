# COMO

A game that helps to create better descriptions for words

Como lets players guess words by only showing their descriptions.

The more words you guess correctly, the more descriptions you can create to test them on other players.

For every description that helps other players to guess correctly, you receive points.

The data you enter into the game is used to improve descriptions for lexemes in Wikidata.
This project is being developed as a Bachelor thesis at LMU Munich.