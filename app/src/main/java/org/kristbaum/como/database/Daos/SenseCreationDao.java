package org.kristbaum.como.database.Daos;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import org.kristbaum.como.database.models.SenseCreation;

import java.util.List;

@Dao
public interface SenseCreationDao {
    @Query("SELECT * FROM sense_creation_table")
    LiveData<List<SenseCreation>> getAll();

    @Query("SELECT * FROM sense_creation_table WHERE player_input ISNULL")
    LiveData<List<SenseCreation>> getAllUnedited();

    @Query("SELECT * FROM sense_creation_table WHERE player_input ISNULL AND lemma_lang = :lemma_lang")
    LiveData<List<SenseCreation>> getAllUneditedInLanguage(String lemma_lang);

    @Query("SELECT * FROM sense_creation_table WHERE (NOT player_input ISNULL) AND (NOT transferred) AND lemma_lang = lemma_lang")
    List<SenseCreation> getAllCreatedNotTransferred();

    @Insert
    void insertAll(SenseCreation... senseCreations);

    @Insert
    void insert(SenseCreation senseCreation);

    @Delete
    void delete(SenseCreation senseCreation);

    @Update
    void update(SenseCreation senseCreation);
}
