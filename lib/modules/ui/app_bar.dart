import 'package:como/config.dart';
import 'package:flutter/material.dart';

AppBar getComoAppBar(BuildContext context, String title) {
  final double screenWidth = MediaQuery.of(context).size.width;

  return AppBar(
    title: screenWidth < appWidth ? null : Text(title),
  );
}