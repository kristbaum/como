package org.kristbaum.como.viewModels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import org.kristbaum.como.client.Get;
import org.kristbaum.como.client.Post;
import org.kristbaum.como.database.models.Game;
import org.kristbaum.como.database.models.SenseCreation;
import org.kristbaum.como.database.models.User;
import org.kristbaum.como.repositories.GameRepository;
import org.kristbaum.como.repositories.SenseCreationRepository;
import org.kristbaum.como.repositories.UserRepository;

import java.util.List;

public class SyncViewModel extends AndroidViewModel {
    private final GameRepository gameRepository;
    private final UserRepository userRepository;
    private final SenseCreationRepository senseCreationRepository;


    public SyncViewModel(@NonNull Application application) {
        super(application);
        gameRepository = new GameRepository(application);
        userRepository = new UserRepository(application);
        senseCreationRepository = new SenseCreationRepository(application);
    }

    public void syncData() {
        new Get(this);
        new Post(this);
    }


    public void insertGame(Game game) {
        gameRepository.insertGame(game);
    }

    public void insertSenseCreation(SenseCreation senseCreation) {
        senseCreationRepository.insertSenseCreation(senseCreation);
    }

    public User loadUserDataNotLive() {
        return userRepository.loadUserDataNotLive();
    }

    public void updateUser(User user) {
        userRepository.updateUser(user);
    }

    public List<Game> getPlayedNotTransferredGames() {
        return gameRepository.loadPlayedNotTransferredGames();
    }

    public List<SenseCreation> getAllCreatedNotTransferred() {
        return senseCreationRepository.getAllCreatedNotTransferred();
    }

    public void updateGame(Game game) {
        gameRepository.updateGame(game);
    }

    public void updateSenseCreation(SenseCreation senseCreation) {
        senseCreationRepository.updateSenseCreation(senseCreation);
    }

}
