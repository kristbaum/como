package org.kristbaum.como.repositories;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import org.kristbaum.como.database.AppDatabase;
import org.kristbaum.como.database.Daos.GameDao;
import org.kristbaum.como.database.models.Game;
import org.kristbaum.como.viewModels.MainViewModel;

import java.util.List;

public class GameRepository {
    private final GameDao gameDao;
    private final LiveData<List<Game>> unplayedGames;

    public GameRepository(Application application) {
        AppDatabase database = AppDatabase.getInstance(application);
        gameDao = database.gameDao();
        unplayedGames = gameDao.getAllUnplayedGames(MainViewModel.getLemma_lang(), MainViewModel.getSense_lang());
    }

    public LiveData<List<Game>> getUnplayedGames() {
        return unplayedGames;
    }

    public List<Game> loadPlayedNotTransferredGames() {
        return gameDao.getAllPlayedNoTransferred();
    }

    public void deleteGame(Game game) {
        new deleteGameAsyncTask(gameDao).execute(game);
    }

    private static class deleteGameAsyncTask extends AsyncTask<Game, Void, Void> {
        private final GameDao gameDao;

        private deleteGameAsyncTask(GameDao gameDao) {
            this.gameDao = gameDao;
        }

        @Override
        protected Void doInBackground(Game... games) {
            gameDao.delete(games[0]);
            return null;
        }
    }

    public void insertGame(Game game) {
        new insertGameAsyncTask(gameDao).execute(game);
    }

    private static class insertGameAsyncTask extends AsyncTask<Game, Void, Void> {
        private final GameDao gameDao;

        private insertGameAsyncTask(GameDao gameDao) {
            this.gameDao = gameDao;
        }

        @Override
        protected Void doInBackground(Game... games) {
            gameDao.insertGame(games[0]);
            return null;
        }
    }

    public void updateGame(Game game) {
        new updateGameAsyncTask(gameDao).execute(game);
    }

    private static class updateGameAsyncTask extends AsyncTask<Game, Void, Void> {
        private final GameDao gameDao;

        private updateGameAsyncTask(GameDao gameDao) {
            this.gameDao = gameDao;
        }

        @Override
        protected Void doInBackground(Game... games) {
            gameDao.updateGame(games[0]);
            return null;
        }
    }
}
