import 'package:como/model/prompts/response_option.dart';

abstract class ComoPrompt {
  ComoPrompt({
    required this.id,
    required this.question,
    required this.responseOptions,
  });

  final int id;
  final String question;
  final List<ResponseOption> responseOptions;

  factory ComoPrompt.fromJson(Map<String, dynamic> json) =>
      throw UnimplementedError();
}
