package org.kristbaum.como.client;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.kristbaum.como.database.models.Game;
import org.kristbaum.como.database.models.SenseCreation;
import org.kristbaum.como.database.models.User;
import org.kristbaum.como.viewModels.MainViewModel;
import org.kristbaum.como.viewModels.SyncViewModel;

import java.util.ArrayList;
import java.util.List;

public class Post {
    private static final String TAG = "Post";
    private final SyncViewModel syncViewModel;
    List<Game> gameResult;
    List<SenseCreation> senseCreationsResult;
    List<Game> gamesToTransfer = new ArrayList<>();
    List<SenseCreation> senseCreationToTransfer = new ArrayList<>();
    private boolean retry = true;

    public Post(SyncViewModel syncViewModel) {
        this.syncViewModel = syncViewModel;
        gameResult = syncViewModel.getPlayedNotTransferredGames();
        senseCreationsResult = syncViewModel.getAllCreatedNotTransferred();
        postData();
    }

    private void postData() {
        JSONObject postDataObj = generateData();
        /*try {
            Log.v(TAG, "POST this: " + postDataObj.toString(4));
        } catch (JSONException e) {
            e.printStackTrace();
        }*/

        try {
            String postResult = HttpConn.sendPost("https://como.kristbaum.org/como-app/v2/data", postDataObj);
            if (postResult == null) {
                Log.v(TAG, "NULL result from POST");
            } else if (postResult.equals("retry") && retry) {
                retry = false;
                postData();
            } else {
                Log.v(TAG, postResult);
                saveTransferredStatus(gamesToTransfer, senseCreationToTransfer);

                /*JSONObject postResultObj = new JSONObject(postResult);
                if (postResultObj.getString("status").equals("processed")) {
                    Log.v(TAG, "Successful upload");
                }*/
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void saveTransferredStatus(List<Game> gamesToTransfer, List<SenseCreation> senseCreationToTransfer) {
        if (gamesToTransfer != null) {
            for (int i = 0; i < gamesToTransfer.size(); i++) {
                Game game = gamesToTransfer.get(i);
                game.setTransferred(true);
                syncViewModel.updateGame(game);
            }
        }
        if (senseCreationToTransfer != null) {
            for (int i = 0; i < senseCreationToTransfer.size(); i++) {
                SenseCreation senseCreation = senseCreationToTransfer.get(i);
                senseCreation.setTransferred(true);
                syncViewModel.updateSenseCreation(senseCreation);
            }
        }

    }

    private JSONObject generateData() {
        JSONObject obj = new JSONObject();


        try {
            JSONArray gameArray = new JSONArray();
            if (gameResult != null) {
                for (int i = 0; i < gameResult.size(); i++){
                    Game game = gameResult.get(i);
                    JSONObject performanceObj = new JSONObject(game.getPerformance());
                    performanceObj.put("id", game.getGame_id());
                    gameArray.put(performanceObj);
                    gamesToTransfer.add(game);
                }
            }

            JSONArray senseCreationArray = new JSONArray();
            if (senseCreationsResult != null) {
                for (int i = 0; i < senseCreationsResult.size(); i++){
                    SenseCreation senseCreation = senseCreationsResult.get(i);
                    JSONObject inputObj = new JSONObject(senseCreation.getPlayer_input());
                    inputObj.put("id", senseCreation.getCreation_id());
                    senseCreationArray.put(inputObj);
                    senseCreationToTransfer.add(senseCreation);
                }
            }


            obj.put("senseCreationData", senseCreationArray);
            obj.put("gameData", gameArray);
            obj.put("id", MainViewModel.getUniqueID());
            // DO THIS LAST to hopefully prevent race condition
            User user = syncViewModel.loadUserDataNotLive();
            while (user == null) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                user = syncViewModel.loadUserDataNotLive();
            }

            JSONObject userObj = new JSONObject(user.getPrefs());
            userObj.put("version", 11);
            obj.put("user", userObj);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return obj;
    }
}