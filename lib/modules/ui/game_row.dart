import 'package:como/model/game.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

class GameRow extends StatelessWidget {
  final String rowTitle;
  final String rowMode;
  final Future<List<Game>> gameListFuture; // Passed from HomePage

  const GameRow({
    super.key,
    required this.rowTitle,
    required this.rowMode,
    required this.gameListFuture,
  });

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<Game>>(
      future: gameListFuture, // Use the future passed from HomePage
      builder: (context, snapshot) =>
          snapshot.connectionState == ConnectionState.waiting
              ? const Center(child: CircularProgressIndicator())
              : snapshot.hasError
                  ? const Text(
                      'Error connecting to server, please try again later.',
                      style: TextStyle(color: Colors.red),
                    )
                  : _buildGamesList(context, snapshot),
    );
  }

  Widget _buildGamesList(
    BuildContext context,
    AsyncSnapshot<List<Game>> snapshot,
  ) {
    final scrollController = ScrollController();

    if (snapshot.data == null || snapshot.data!.isEmpty) {
      return Center(
        child: Scrollbar(
          controller: scrollController,
          child: ListTile(
            title: Text(rowTitle),
            subtitle: SizedBox(
              height: 250,
              child: ListView.builder(
                controller: scrollController,
                shrinkWrap: true,
                scrollDirection: Axis.horizontal,
                itemCount: 1,
                itemBuilder: (context, index) => _buildEmptyGameCard(context),
              ),
            ),
          ),
        ),
      );
    }

    return Center(
      child: Scrollbar(
        controller: scrollController,
        child: ListTile(
          title: Text(rowTitle),
          subtitle: SizedBox(
            height: 250,
            child: ListView.builder(
              controller: scrollController,
              shrinkWrap: true,
              scrollDirection: Axis.horizontal,
              itemCount: snapshot.data!.length,
              itemBuilder: (context, index) =>
                  _buildGameCard(context, snapshot.data![index]),
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildEmptyGameCard(BuildContext context) {
    return const Card(
      child: SizedBox(
        width: 250,
        child: ListTile(
          titleAlignment: ListTileTitleAlignment.top,
          title: Text(
            'No games found.',
            maxLines: 4,
          ),
        ),
      ),
    );
  }

  Widget _buildGameCard(BuildContext context, Game game) {
    return Card(
      child: SizedBox(
        width: 250,
        child: ListTile(
          titleAlignment: ListTileTitleAlignment.top,
          onTap: () => context.push('/game/${game.slug}'),
          title: Text(
            game.name,
            overflow: TextOverflow.ellipsis,
            maxLines: 4,
          ),
          subtitle: Text(
            game.shortDescription,
            overflow: TextOverflow.ellipsis,
            maxLines: 3,
          ),
          leading: Column(
            children: [
              game.promptIcon,
              game.responseIcon,
            ],
          ),
        ),
      ),
    );
  }
}
