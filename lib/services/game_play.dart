import 'dart:convert';

import 'package:como/config.dart';
import 'package:como/model/game.dart';
import 'package:como/model/prompts/m_prompt.dart';
import 'package:como/model/prompts/mt_prompt.dart';
import 'package:como/model/prompts/tt_prompt.dart';
import 'package:como/model/prompts/ttt_prompt.dart';
import 'package:como/model/prompts/tttt_prompt.dart';

Future<List<T>> getPrompts<T>(
    Game game, T Function(Map<String, dynamic>) fromJson) async {
  try {
    final response = await client.get(
        Uri.parse('$backendUrl/api/games/${game.slug}/prompts/'),
        headers: defaultRequestHeaders);
    if (response.statusCode == 200) {
      List<dynamic> jsonResponse = jsonDecode(utf8.decode(response.bodyBytes));
      return jsonResponse.map<T>((item) => fromJson(item)).toList();
    } else {
      throw Exception('Failed to fetch prompts');
    }
  } catch (e) {
    throw Exception('Failed to fetch prompts: $e');
  }
}

Future<List<TTTTPrompt>> getTTTTPrompts(Game game) async {
  return getPrompts<TTTTPrompt>(game, TTTTPrompt.fromJson);
}

Future<List<TTTPrompt>> getTTTPrompts(Game game) async {
  return getPrompts<TTTPrompt>(game, TTTPrompt.fromJson);
}

Future<List<TTPrompt>> getTTPrompts(Game game) async {
  return getPrompts<TTPrompt>(game, TTPrompt.fromJson);
}

Future<List<MPrompt>> getIPrompts(Game game) async {
  return getPrompts<MPrompt>(game, MPrompt.fromJson);
}

Future<List<MTPrompt>> getITPrompts(Game game) async {
  return getPrompts<MTPrompt>(game, MTPrompt.fromJson);
}
