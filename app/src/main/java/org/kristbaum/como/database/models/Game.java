package org.kristbaum.como.database.models;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "game_table")
public class Game {

    @PrimaryKey
    @ColumnInfo(name = "game_id")
    public Integer game_id;

    @ColumnInfo(name = "content")
    public String content;

    @ColumnInfo(name = "lemma_lang")
    public String lemma_lang;

    @ColumnInfo(name = "sense_lang")
    public String sense_lang;

    @ColumnInfo(name = "performance", defaultValue = "NULL")
    public String performance;

    @ColumnInfo(name = "transferred", defaultValue = "FALSE")
    public boolean transferred;

    public Boolean getTransferred() {
        return transferred;
    }

    public void setTransferred(Boolean transferred) {
        this.transferred = transferred;
    }

    public String getPerformance() {
        return performance;
    }

    public void setPerformance(String performance) {
        this.performance = performance;
    }

    public Integer getGame_id() {
        return game_id;
    }

    public void setGame_id(Integer game_id) {
        this.game_id = game_id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getLemma_lang() {
        return lemma_lang;
    }

    public void setLemma_lang(String lemma_lang) {
        this.lemma_lang = lemma_lang;
    }

    public String getSense_lang() {
        return sense_lang;
    }

    public void setSense_lang(String sense_lang) {
        this.sense_lang = sense_lang;
    }

    public Game (Integer game_id, String content, String lemma_lang, String sense_lang) {
        this.game_id = game_id;
        this.content = content;
        this.lemma_lang = lemma_lang;
        this.sense_lang = sense_lang;
    }

}

