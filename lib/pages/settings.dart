import 'package:como/app.dart';
import 'package:como/config.dart';
import 'package:como/modules/ui/app_bar.dart';
import 'package:como/modules/ui/settings/delete_account.dart';
import 'package:como/modules/ui/settings/language_selection_card.dart';
import 'package:como/modules/ui/settings/theme_selection.dart';
import 'package:flutter/material.dart';
import 'package:como/l10n/app_localizations.dart';

class SettingsPage extends StatefulWidget {
  const SettingsPage({super.key});

  @override
  State<SettingsPage> createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  String? _selectedLanguage;
  String? _selectedThemeMode;

  @override
  void initState() {
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _selectedLanguage ??= Localizations.localeOf(context).languageCode;
  }

  void _changeLanguage(String? newLang) {
    if (newLang != null && newLang != _selectedLanguage) {
      setState(() {
        _selectedLanguage = newLang;
        Locale newLocale = Locale(newLang, '');
        ComoApp.of(context)?.setLocale(newLocale);
      });
    }
  }

  void _changeThemeMode(String? newTheme) {
    if (newTheme != null && newTheme != _selectedThemeMode) {
      setState(() {
        _selectedThemeMode = newTheme;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:
          getComoAppBar(context, AppLocalizations.of(context)!.drawer_settings),
      body: SingleChildScrollView(
        child: Center(
          child: ConstrainedBox(
            constraints: const BoxConstraints(maxWidth: appWidth),
            child: ListView(
              physics: const NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              children: <Widget>[
                const SizedBox(height: 10),
                DeleteAccountCard(),
                const SizedBox(height: 20),
                LanguageSettingsCard(
                  selectedLanguage: _selectedLanguage,
                  onLanguageChanged: _changeLanguage,
                ),
                const SizedBox(height: 10),
                ThemeSettingsCard(
                  selectedTheme: _selectedThemeMode,
                  onThemeChanged: _changeThemeMode,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
