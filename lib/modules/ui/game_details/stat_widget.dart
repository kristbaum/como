import 'package:como/pages/game_details.dart';
import 'package:como/services/game_details.dart';
import 'package:flutter/material.dart';
import 'package:como/l10n/app_localizations.dart';

class GameDetailStatWidget extends StatelessWidget {
  const GameDetailStatWidget({
    super.key,
    required this.widget,
  });

  final GameDetailsPage widget;

  @override
  Widget build(BuildContext context) {
    return Card(
      child: ListTile(
        title: Text(AppLocalizations.of(context)!.statistics),
        subtitle: FutureBuilder(
          future: fetchGameStats(widget.gameSlug),
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return const Center(child: CircularProgressIndicator());
            } else if (snapshot.hasError) {
              return Text(
                  '${AppLocalizations.of(context)!.error} ${snapshot.error}');
            }

            int donePrompts = snapshot.data!.donePrompts;
            int totalPrompts = snapshot.data!.totalPrompts;
            double ratio = totalPrompts > 0 ? donePrompts / totalPrompts : 0.0;

            return Column(
              children: [
                LinearProgressIndicator(value: ratio),
                Wrap(spacing: 10, children: [
                  Text(
                      '${AppLocalizations.of(context)!.done_prompts}: $donePrompts'),
                  Text(
                      '${AppLocalizations.of(context)!.total_prompts}: $totalPrompts')
                ]),
              ],
            );
          },
        ),
      ),
    );
  }
}
