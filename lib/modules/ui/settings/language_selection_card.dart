import 'package:flutter/material.dart';
import 'package:como/l10n/app_localizations.dart';

class LanguageSettingsCard extends StatelessWidget {
  final String? selectedLanguage;
  final ValueChanged<String?> onLanguageChanged;

  const LanguageSettingsCard({
    super.key,
    required this.selectedLanguage,
    required this.onLanguageChanged,
  });

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          ListTile(
            leading: const Icon(Icons.language),
            title: Text(AppLocalizations.of(context)!.change_language),
            subtitle: DropdownButton<String>(
              value: selectedLanguage,
              items: <String>['en', 'de']
                  .map<DropdownMenuItem<String>>((String value) {
                return DropdownMenuItem<String>(
                  value: value,
                  child: Text(value.toUpperCase()),
                );
              }).toList(),
              onChanged: onLanguageChanged,
              underline: Container(),
            ),
          ),
        ],
      ),
    );
  }
}
