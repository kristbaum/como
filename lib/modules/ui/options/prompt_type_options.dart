import 'package:flutter/material.dart';

class PromptTypeOptions extends StatefulWidget {
  final String promptType;
  final ValueChanged<String> onChanged;

  const PromptTypeOptions({
    super.key,
    required this.promptType,
    required this.onChanged,
  });

  @override
  PromptTypeOptionsState createState() => PromptTypeOptionsState();
}

class PromptTypeOptionsState extends State<PromptTypeOptions> {
  late String _promptType;

  @override
  void initState() {
    super.initState();
    _promptType = widget.promptType;
  }

  Widget _buildOption(String title, String value) {
    return ListTile(
      title: Text(title),
      leading: Radio<String>(
        value: value,
        groupValue: _promptType,
        onChanged: (String? newValue) {
          setState(() {
            _promptType = newValue!;
            widget.onChanged(_promptType);
          });
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    const options = {
      'TT': 'Two texts',
      'TTT': 'Three texts',
      'TTTT': 'Four texts',
      'I': 'One image',
      'IT': 'One image, one text',
    };

    return Wrap(
      alignment: WrapAlignment.center,
      children: options.entries
          .map((entry) => _buildOption(entry.value, entry.key))
          .toList(),
    );
  }
}
