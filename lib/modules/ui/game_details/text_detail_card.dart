import 'package:como/services/game_details.dart';
import 'package:flutter/material.dart';
import 'package:como/l10n/app_localizations.dart';

class TextDetailCard extends StatelessWidget {
  final String title;
  final String content;
  final bool isEditable;
  final String gameSlug;
  final String fieldName;

  const TextDetailCard({
    super.key,
    required this.title,
    required this.content,
    required this.isEditable,
    required this.gameSlug,
    required this.fieldName,
  });

  @override
  Widget build(BuildContext context) {
    return Card(
      child: ListTile(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(title),
            if (isEditable)
              IconButton(
                icon: Icon(Icons.edit),
                onPressed: () => _showEditDialog(context),
              ),
          ],
        ),
        subtitle: Text(content),
      ),
    );
  }

  void _showEditDialog(BuildContext context) {
    TextEditingController controller = TextEditingController(text: content);
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(AppLocalizations.of(context)!.edit),
          content: TextField(
            controller: controller,
            maxLines: null,
            decoration: InputDecoration(
              hintText: AppLocalizations.of(context)!.editDescription,
            ),
          ),
          actions: [
            TextButton(
              onPressed: () => Navigator.of(context).pop(),
              child: Text(MaterialLocalizations.of(context).cancelButtonLabel),
            ),
            ElevatedButton(
              onPressed: () {
                if (isEditable) {
                  updateGameField(
                      context, gameSlug, {fieldName: controller.text});
                }
                Navigator.of(context).pop();
              },
              child: Text(MaterialLocalizations.of(context).saveButtonLabel),
            ),
          ],
        );
      },
    );
  }
}
