// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'm_prompt.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MPrompt _$MPromptFromJson(Map<String, dynamic> json) => MPrompt(
      id: (json['id'] as num).toInt(),
      question: json['question'] as String,
      responseOptions: (json['response_options'] as List<dynamic>)
          .map((e) => ResponseOption.fromJson(e as Map<String, dynamic>))
          .toList(),
      media1: Uri.parse(json['image1'] as String),
      media1Filename: json['image1_filename'] as String,
      media1FilenameUrl: Uri.parse(json['image1_filename_url'] as String),
    );

Map<String, dynamic> _$MPromptToJson(MPrompt instance) => <String, dynamic>{
      'id': instance.id,
      'question': instance.question,
      'response_options': instance.responseOptions,
      'image1': instance.media1.toString(),
      'image1_filename': instance.media1Filename,
      'image1_filename_url': instance.media1FilenameUrl.toString(),
    };
