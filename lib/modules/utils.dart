import 'package:como/config.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

Center questionWidget(String question) {
  return Center(
    child: Text(question,
        textAlign: TextAlign.center,
        style: const TextStyle(
          fontSize: 30,
          fontWeight: FontWeight.bold,
        )),
  );
}

class LoginButton extends StatelessWidget {
  const LoginButton({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: () {
        launchUrl(
          Uri.parse(
            '$backendUrl/auth/mediawiki/url/',
          ),
          webOnlyWindowName: '_self',
        );
      },
      child: const Text("Login with Wikimedia"),
    );
  }
}

String addPromptLabel(int position, List<String> promptLabelsList) {
  List<String> defaultLabels = ["A)", "B)", "C)", "D)", "E)", "F)"];

  if (promptLabelsList.isEmpty && position >= 0 && position < 6) {
    return defaultLabels[position];
  }

  if (position >= 0 && position < promptLabelsList.length) {
    return promptLabelsList[position];
  }

  return "";
}
