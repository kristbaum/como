import 'package:como/config.dart';

import 'web_conditional_import.dart';
import 'dart:js_interop';

@JS('window.history.replaceState')
external void replaceState(dynamic state, String title, String? url);

WebConditionalImport getInstance() => WebPlatform();

class WebPlatform implements WebConditionalImport {
  @override
  Future<void> pushState(Uri redirectUri) async {
    log.info('Web: Redirected to $redirectUri');
    replaceState(null, 'Redirected URL', redirectUri.toString());
  }
}
