import 'package:como/config.dart';

import 'web_conditional_import.dart';

WebConditionalImport getInstance() => NativePlatform();

class NativePlatform implements WebConditionalImport {
  @override
  Future<void> pushState(Uri redirectUri) async {
    log.info('Native: No redirection required.');
  }
}
