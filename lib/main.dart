import 'package:como/app.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:como/config.dart';
import 'package:flutter/services.dart';
import 'package:logging/logging.dart';

void main() async {
  configureLogging();
  //Disabled, because webserver configuration is missing
  //setPathUrlStrategy();
  WidgetsFlutterBinding.ensureInitialized();

  if (kDebugMode) {
    if (kIsWeb) {
      log.info("Running in Web Debug Mode");
      backendUrl = 'http://localhost:8000';
    } else {
      log.info("Running in Mobile Debug Mode");
      // for Android Emulator use 'http://some-ip:8000'
      // for iOS Simulator use 'http://localhost:8000'
      backendUrl = 'http://localhost:8000';
    }
  } else {
    backendUrl = 'https://como-backend.toolforge.org';
  }
  log.info("Selected Backend URL: $backendUrl");

  await SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
  ]);
  runApp(const ComoApp());
}

void configureLogging() {
  if (kDebugMode) {
    Logger.root.level = Level.ALL;
  } else {
    Logger.root.level = Level.SEVERE;
  }
  Logger.root.onRecord.listen((record) {
    // ignore: avoid_print
    print('${record.level.name}: ${record.time}: ${record.message}');
  });
}
