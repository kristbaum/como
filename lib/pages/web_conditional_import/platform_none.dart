import 'package:como/config.dart';

import 'web_conditional_import.dart';

WebConditionalImport getInstance() => UnsupportedPlatform();

class UnsupportedPlatform implements WebConditionalImport {
  @override
  Future<void> pushState(Uri redirectUri) async {
    log.info('Unsupported platform: No redirection.');
  }
}
