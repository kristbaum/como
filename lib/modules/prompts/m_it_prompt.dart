import 'package:como/config.dart';
import 'package:como/model/prompts/mt_prompt.dart';
import 'package:como/modules/prompts/base_prompt.dart';
import 'package:como/modules/responses/build_response.dart';
import 'package:como/modules/ui/expandable_text_widget.dart';
import 'package:como/modules/ui/image_card_widget.dart';
import 'package:como/modules/utils.dart';
import 'package:como/services/game_play.dart';
import 'package:flutter/material.dart';

class ITPromptModule extends BasePromptModule<MTPrompt> {
  const ITPromptModule({super.key, required super.game});

  @override
  ITPromptModuleState createState() => ITPromptModuleState();
}

class ITPromptModuleState extends BasePromptModuleState<MTPrompt> {
  @override
  Future<List<MTPrompt>> addNewPrompts() async {
    return await getITPrompts(widget.game);
  }

  @override
  Widget buildPromptContent(MTPrompt prompt) {
    if (prompts.length > 1) {
      log.info("Precaching image: ${prompts[1].media1}");
      precacheImage(NetworkImage(prompts[1].media1.toString()), context);
    }
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Flexible(
          child: SingleChildScrollView(
            child: Center(
              child: ConstrainedBox(
                constraints: const BoxConstraints(maxWidth: appWidth),
                child: ListView(
                  physics: const NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  children: [
                    questionWidget(prompt.question),
                    const SizedBox(height: 10),
                    ExpandableTextWidget(
                      title: addPromptLabel(0, widget.game.promptLabelsList),
                      text: prompt.text1,
                      link: prompt.link1,
                    ),
                    const SizedBox(height: 10),
                    ImageCardWidget(
                      title: addPromptLabel(0, widget.game.promptLabelsList),
                      imageUri: prompt.media1,
                      imageFilename: prompt.media1Filename,
                      imageFilenameUrl: prompt.media1FilenameUrl,
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
        buildResponse(nextPrompt, widget.game.responseType, widget.game, prompt,
            addPromptLabel(1, widget.game.promptLabelsList))
      ],
    );
  }
}
