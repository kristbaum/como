import 'package:flutter/material.dart';

class Counter extends StatelessWidget {
  const Counter({
    super.key,
    required this.title,
    required this.count,
  });

  final String title;
  final int count;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(
          title,
          style: const TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
        ),
        const SizedBox(height: 8),
        TweenAnimationBuilder<int>(
          // Tween for animating the count
          tween: IntTween(begin: 0, end: count),
          duration: const Duration(milliseconds: 2000),
          builder: (context, animatedValue, child) {
            return Text(
              animatedValue.toString(),
              style: const TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
            );
          },
        ),
      ],
    );
  }
}
