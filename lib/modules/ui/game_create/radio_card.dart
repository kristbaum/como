import 'package:flutter/material.dart';
import 'package:como/l10n/app_localizations.dart';

class RadioCard extends StatelessWidget {
  const RadioCard({
    super.key,
    required this.context,
    required this.title,
    required this.groupValue,
    required this.options,
    required this.onChanged,
  });

  final BuildContext context;
  final String title;
  final bool groupValue;
  final List<String> options;
  final ValueChanged<bool?> onChanged;

  @override
  Widget build(BuildContext context) {
    return Card(
      child: ListTile(
        title: Text(title),
        subtitle: Wrap(
          children: options.map((option) {
            final bool value = option == AppLocalizations.of(context)!.yes;
            return ListTile(
              title: Text(option),
              leading: Radio<bool>(
                value: value,
                groupValue: groupValue,
                onChanged: onChanged,
              ),
            );
          }).toList(),
        ),
      ),
    );
  }
}
