import 'package:como/model/leaderboard_entry.dart';

class Stat {
  final int totalPrompts;
  final int donePrompts;
  final List<LeaderboardEntry> leaderboard;

  Stat({
    required this.totalPrompts,
    required this.donePrompts,
    required this.leaderboard,
  });

  factory Stat.fromJson(Map<String, dynamic> json) {
    var list = json['leaderboard'] as List;
    List<LeaderboardEntry> leaderboardList = list.map((i) => LeaderboardEntry.fromJson(i)).toList();
    
    return Stat(
      totalPrompts: json['total_prompts'],
      donePrompts: json['done_prompts'],
      leaderboard: leaderboardList,
    );
  }
}