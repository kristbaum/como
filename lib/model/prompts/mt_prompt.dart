import 'package:como/model/prompts/como_prompt.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:como/model/prompts/response_option.dart';
part 'mt_prompt.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class MTPrompt extends ComoPrompt {
  MTPrompt({
    required super.id,
    required super.question,
    required super.responseOptions,
    required this.text1,
    this.link1,
    required this.media1,
    required this.media1Filename,
    required this.media1FilenameUrl,
  });
  final String text1;
  final Uri? link1;
  final Uri media1;
  final String media1Filename;
  final Uri media1FilenameUrl;

  factory MTPrompt.fromJson(Map<String, dynamic> json) =>
      _$MTPromptFromJson(json);
}
