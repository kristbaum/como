// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'response_option.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ResponseOption _$ResponseOptionFromJson(Map<String, dynamic> json) =>
    ResponseOption(
      text: json['text'] as String,
      link: json['link'] == null ? null : Uri.parse(json['link'] as String),
      scId: (json['sc_id'] as num?)?.toInt(),
    );

Map<String, dynamic> _$ResponseOptionToJson(ResponseOption instance) =>
    <String, dynamic>{
      'text': instance.text,
      'link': instance.link?.toString(),
      'sc_id': instance.scId,
    };
