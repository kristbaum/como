import 'package:como/config.dart';
import 'package:como/model/game.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

abstract class BasePromptModule<T> extends StatefulWidget {
  final Game game;

  const BasePromptModule({super.key, required this.game});

  @override
  @protected
  BasePromptModuleState<T> createState();
}

abstract class BasePromptModuleState<T> extends State<BasePromptModule<T>> {
  List<T> prompts = [];
  bool isLoading = false;

  @protected
  Future<List<T>> addNewPrompts();

  @override
  Widget build(BuildContext context) {
    log.info("${prompts.length} Prompts cached at this time");
    if (isLoading) {
      return const Center(child: CircularProgressIndicator());
    } else if (prompts.isNotEmpty) {
      return buildPromptContent(prompts.first);
    } else {
      return _showEmpty(context);
    }
  }

  @protected
  Widget buildPromptContent(T prompt);

  @override
  void initState() {
    super.initState();
    _loadPrompts();
  }

  void nextPrompt(String responseResult) {
    log.info("Next Prompt");
    if (prompts.isNotEmpty) {
      prompts.removeAt(0);
      if (prompts.length < 3) {
        _loadPrompts();
      } else {
        setState(() {});
      }
    }
  }

  Center _showEmpty(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const Text("All done!"),
          ElevatedButton(
            onPressed: () {
              context.pop();
            },
            child: const Text("Back"),
          ),
        ],
      ),
    );
  }

  void _loadPrompts() async {
    setState(() {
      isLoading = true;
    });
    List<T> newPrompts = await addNewPrompts();
    setState(() {
      prompts.addAll(newPrompts);
      isLoading = false;
    });
  }
}
