import 'package:como/config.dart';
import 'package:como/model/game.dart';
import 'package:como/modules/prompts/m_a_prompt.dart';
import 'package:como/modules/prompts/m_at_prompt.dart';
import 'package:como/modules/prompts/m_i_prompt.dart';
import 'package:como/modules/ui/app_bar.dart';
import 'package:como/modules/prompts/m_it_prompt.dart';
import 'package:como/modules/prompts/m_tt_prompt.dart';
import 'package:como/modules/prompts/m_ttt_prompt.dart';
import 'package:como/modules/prompts/m_tttt_prompt.dart';
import 'package:flutter/material.dart';

class GamePlayPage extends StatefulWidget {
  final Game game;

  const GamePlayPage({super.key, required this.game});

  @override
  GamePlayPageState createState() => GamePlayPageState();
}

class GamePlayPageState extends State<GamePlayPage> {
  double _calculateStreakProgress() {
    return (streakTarget - user.missingForStreakToday) / streakTarget;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: getComoAppBar(context, widget.game.name),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: LinearProgressIndicator(
              value: _calculateStreakProgress(),
              minHeight: 10,
              backgroundColor: Colors.grey[300],
              valueColor: AlwaysStoppedAnimation<Color>(Colors.green),
            ),
          ),
          Expanded(
            child: Center(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: _buildPrompt(widget.game.promptType),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildPrompt(String promptType) {
    switch (promptType) {
      case "TT":
        return TTPromptModule(game: widget.game);
      case "TTT":
        return TTTPromptModule(game: widget.game);
      case "TTTT":
        return TTTTPromptModule(game: widget.game);
      case "I":
        return IPromptModule(game: widget.game);
      case "IT":
        return ITPromptModule(game: widget.game);
      case "A":
        return APromptModule(game: widget.game);
      case "AT":
        return ATPromptModule(game: widget.game);
      default:
        log.warning("Unknown Prompt Type: $promptType");
        return const Text("Unknown Prompt Type");
    }
  }
}
