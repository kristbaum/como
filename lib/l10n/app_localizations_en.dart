// ignore: unused_import
import 'package:intl/intl.dart' as intl;
import 'app_localizations.dart';

// ignore_for_file: type=lint

/// The translations for English (`en`).
class AppLocalizationsEn extends AppLocalizations {
  AppLocalizationsEn([String locale = 'en']) : super(locale);

  @override
  String get pageHomeWikimediaLogin => 'Login with Wikimedia';

  @override
  String get drawer_continue => 'Continue';

  @override
  String get drawer_continue_desc => 'Continue the game you were playing last';

  @override
  String get drawer_random_game => 'Random';

  @override
  String get drawer_random_game_desc => 'Start a random game';

  @override
  String get drawer_create => 'Create';

  @override
  String get drawer_create_desc => 'Create a new game';

  @override
  String get drawer_gameslist => 'List';

  @override
  String get drawer_gameslist_desc => 'List all public games';

  @override
  String get drawer_mygames => 'My games';

  @override
  String get drawer_stats => 'Stats';

  @override
  String get drawer_privacy_policy => 'Privacy policy';

  @override
  String get drawer_settings => 'Settings';

  @override
  String get drawer_about => 'Help/About';

  @override
  String get error => 'Error';

  @override
  String get set_inactive_as_admin => 'Set Inactive (as Admin)';

  @override
  String get set_active_as_admin => 'Set Active (as Admin)';

  @override
  String get share_this_game => 'Share this game';

  @override
  String get start => 'Start';

  @override
  String get this_game_is_not_yet_active_or_finished => 'This game is not yet active or finished';

  @override
  String get you_need_to_be_logged_in_to_play_this_game => 'You need to be logged in to play this game';

  @override
  String get description => 'Description';

  @override
  String get statistics => 'Statistics';

  @override
  String get done_prompts => 'Done Prompts';

  @override
  String get total_prompts => 'Total Prompts';

  @override
  String get data_from_this_game_as_a_csv_table => 'Data from this game as a CSV table';

  @override
  String get download_csv => 'Download CSV';

  @override
  String get user => 'User';

  @override
  String get question_with_placeholders => 'Question (with placeholders)';

  @override
  String get attached_requests => 'Attached Requests';

  @override
  String get prompt_labels => 'Prompt Labels';

  @override
  String get prompt_type => 'Prompt Type';

  @override
  String get response_type => 'Response Type';

  @override
  String get action_type => 'Action Type';

  @override
  String get none => 'None';

  @override
  String get confidence_type => 'Confidence Type';

  @override
  String get show_advanced => 'Show Advanced';

  @override
  String get welcome_to_como => 'Welcome to Como!';

  @override
  String get play_to_increase_knowledge => 'Contribute Easily, Crowdsource Quickly.';

  @override
  String get continue_recent_games => 'Recently Played';

  @override
  String get popular_games => 'Popular';

  @override
  String get newest_games => 'Newest';

  @override
  String get continue_button => 'Continue Last';

  @override
  String get delete_account => 'Delete Account';

  @override
  String get delete_account_description => 'Delete your account and all your data. A new account will be generated without any usage data on reentry. This action cannot be undone.';

  @override
  String get account_deleted_reload => 'Account deleted, reload the app now!';

  @override
  String get add_csv_file => 'Add a csv file';

  @override
  String get create_game => 'Create Game';

  @override
  String get submit => 'Submit';

  @override
  String get login_to_create_game => 'Please log in first to create a game.';

  @override
  String get game_created_successfully => 'Game created successfully';

  @override
  String get failed_to_create_game => 'Failed to create game';

  @override
  String get name => 'Name';

  @override
  String get name_hint => 'e.g \"Picture Quiz\"';

  @override
  String get enter_name => 'Enter a name';

  @override
  String get short_description => 'Short Description';

  @override
  String get short_description_hint => 'e.g \"Match the image with the text.\"';

  @override
  String get enter_short_description => 'Enter a short description';

  @override
  String get long_description => 'Long Description';

  @override
  String get long_description_hint => 'e.g Where the data came from, what edits will be performed. Included links will be clickable!';

  @override
  String get enter_long_description => 'Enter a long description';

  @override
  String get question => 'Question';

  @override
  String get question_hint => 'e.g. \"Does a) depict b)?\"';

  @override
  String get enter_question => 'Enter a question';

  @override
  String get list_publicly => 'Public';

  @override
  String get yes => 'yes';

  @override
  String get no => 'no';

  @override
  String get prompt_labels_hint => 'Enter your prompt labels as a JSON Array here, maximum 20 characters each. Separate with commas.';

  @override
  String get enter_prompt_labels => 'Enter prompt labels';

  @override
  String get wikibase_api => 'Wikibase REST API';

  @override
  String get action_api => 'Action API';

  @override
  String get add_another_request => 'Add another request';

  @override
  String get request_url => 'Request URL';

  @override
  String get request_url_hint => 'https://www.wikidata.org/w/rest.php/wikibase/v0/entities/items/%id1%/descriptions/en';

  @override
  String get request_body => 'Request Body:';

  @override
  String get request_body_hint => 'Enter your data as JSON formatted text here';

  @override
  String get http_method => 'HTTP Method';

  @override
  String get select_http_method => 'Please select an HTTP method';

  @override
  String get enter_value => 'Enter a value';

  @override
  String get paste_data_or_use_file => 'Paste your data here or use a file: ';

  @override
  String get csv_data_hint => 'Enter your data as CSV formatted text here';

  @override
  String get use_file => 'Use file: ';

  @override
  String get json_data_hint => 'Enter your data as JSON formatted text here';

  @override
  String get change_language => 'Change Language';

  @override
  String get upload_csv => 'Upload CSV';

  @override
  String get required_columns => 'Required columns';

  @override
  String get start_upload => 'Start Upload';

  @override
  String get select_file => 'Select File';

  @override
  String get edit => 'Edit';

  @override
  String get editDescription => 'Edit Description';

  @override
  String get enterDescription => 'Enter a description';

  @override
  String get description_updated => 'Description updated';

  @override
  String get update_failed => 'Update failed';

  @override
  String get fields_updated => 'Fields updated';
}
