// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'mt_prompt.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MTPrompt _$MTPromptFromJson(Map<String, dynamic> json) => MTPrompt(
      id: (json['id'] as num).toInt(),
      question: json['question'] as String,
      responseOptions: (json['response_options'] as List<dynamic>)
          .map((e) => ResponseOption.fromJson(e as Map<String, dynamic>))
          .toList(),
      text1: json['text1'] as String,
      link1: json['link1'] == null ? null : Uri.parse(json['link1'] as String),
      media1: Uri.parse(json['image1'] as String),
      media1Filename: json['image1_filename'] as String,
      media1FilenameUrl: Uri.parse(json['image1_filename_url'] as String),
    );

Map<String, dynamic> _$MTPromptToJson(MTPrompt instance) => <String, dynamic>{
      'id': instance.id,
      'question': instance.question,
      'response_options': instance.responseOptions,
      'text1': instance.text1,
      'link1': instance.link1?.toString(),
      'image1': instance.media1.toString(),
      'image1_filename': instance.media1Filename,
      'image1_filename_url': instance.media1FilenameUrl.toString(),
    };
