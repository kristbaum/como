import 'package:json_annotation/json_annotation.dart';
part 'response_option.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class ResponseOption {
  final String text;
  final Uri? link;
  final int? scId;

  ResponseOption({
    required this.text,
    this.link,
    this.scId,
  });

  factory ResponseOption.fromJson(Map<String, dynamic> json) =>
      _$ResponseOptionFromJson(json);
  Map<String, dynamic> toJson() => _$ResponseOptionToJson(this);
}
