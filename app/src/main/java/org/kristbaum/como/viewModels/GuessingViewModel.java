package org.kristbaum.como.viewModels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.kristbaum.como.database.models.Game;
import org.kristbaum.como.database.models.User;
import org.kristbaum.como.repositories.GameRepository;
import org.kristbaum.como.repositories.UserRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class GuessingViewModel extends AndroidViewModel {
    private final GameRepository gameRepository;
    private final UserRepository userRepository;
    private final LiveData<List<Game>> unplayedGames;
    private final LiveData<User> userData;
    private char[] activeTipString;



    public GuessingViewModel(@NonNull Application application) {
        super(application);
        gameRepository = new GameRepository(application);
        userRepository = new UserRepository(application);
        unplayedGames = gameRepository.getUnplayedGames();
        userData = userRepository.getUserData();
    }

    public void updateGame(Game game) {
        gameRepository.updateGame(game);
    }

    public void updateUser(User user) {
        userRepository.updateUser(user);
    }

    public LiveData<List<Game>> getUnplayedGames() {return unplayedGames;}

    public LiveData<User> getUserData() {
        return userData;
    }

    public void finishGame(int tries, JSONArray playerInputs, Game activeGame, boolean solved, boolean skipped) {
        JSONObject obj = new JSONObject();
        try {
            obj.put("tries", tries);
            obj.put("skipped", skipped);
            obj.put("solved", solved);
            obj.put("input_lang", MainViewModel.getLemma_lang());
            obj.put("player_inputs", playerInputs);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        activeGame.setPerformance(obj.toString());
        this.updateGame(activeGame);
    }

    public JSONObject getStats(User user) {
        try {
            return new JSONObject(user.getStats());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getCategoryString(Game activeGame) {
        String cat = "";
        try {
            JSONObject activeObj = new JSONObject(activeGame.getContent());
            cat = activeObj.getJSONObject("cat").getString("value");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return cat;
    }

    public String getTip(String lemma, int tries) {
        if (tries == 1) {
            activeTipString = new char[lemma.length()];
            activeTipString[0] = lemma.charAt(0);
            for (int i = 1; i < activeTipString.length; i++) {
                activeTipString[i] = '_';
            }
        } else if (tries > 1) {
            List<Integer> hiddenChars = new ArrayList<>();
            for (int i = 0; i < activeTipString.length; i++) {
                if (activeTipString[i] == '_') {
                    hiddenChars.add(i);
                }
            }
            if ((hiddenChars.size() > lemma.length() / 2) &&
                    (lemma.length() - hiddenChars.size() <= tries)) {
                int random = new Random().nextInt(hiddenChars.size());
                int index = hiddenChars.get(random);
                activeTipString[index] = lemma.charAt(index);
            }
        }

        StringBuilder returnString = new StringBuilder();

        for (char c : activeTipString) {
            returnString.append(c);
            returnString.append(" ");
        }

        return returnString.toString();
    }
}

