import 'package:flutter/material.dart';

class ComoIcon extends StatelessWidget {
  const ComoIcon({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        padding: const EdgeInsets.all(4),
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          border: Border.all(color: Colors.black, width: 4),
        ),
        child: const Image(
          image: AssetImage('assets/images/icon.png'),
          height: 80,
          fit: BoxFit.cover,
        ),
      ),
    );
  }
}
