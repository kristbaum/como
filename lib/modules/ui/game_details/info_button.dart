import 'package:flutter/material.dart';

class InfoButton extends StatelessWidget {
  final String dialogTitle;
  final String dialogContent;

  const InfoButton({
    super.key,
    required this.dialogTitle,
    required this.dialogContent,
  });

  @override
  Widget build(BuildContext context) {
    return IconButton(
      icon: const Icon(Icons.info_outline),
      tooltip: dialogContent,
      onPressed: () {
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text(dialogTitle),
              content: Text(dialogContent),
              actions: [
                TextButton(
                  onPressed: () => Navigator.of(context).pop(),
                  child: Text(MaterialLocalizations.of(context).okButtonLabel),
                ),
              ],
            );
          },
        );
      },
    );
  }
}
