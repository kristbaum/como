import 'package:como/model/game.dart';
import 'package:como/model/prompts/response_option.dart';
import 'package:como/model/responses/sc_response.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class SCResponseWidget extends StatefulWidget {
  final Function(String) nextPrompt;
  final Game game;
  final int promptId;
  final List<ResponseOption> responseOptions;

  const SCResponseWidget({
    super.key,
    required this.nextPrompt,
    required this.game,
    required this.promptId,
    required this.responseOptions,
  });

  @override
  SCResponseWidgetState createState() => SCResponseWidgetState();
}

class SCResponseWidgetState extends State<SCResponseWidget> {
  int selectedOption = -1;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        ...widget.responseOptions.map(buildResponseOption),
        buildBottomButtons(),
      ],
    );
  }

  Widget buildResponseOption(ResponseOption option) {
    return Card(
      child: RadioListTile<int>(
        value: option.scId!,
        groupValue: selectedOption,
        onChanged: (int? value) {
          setState(() {
            selectedOption = value!;
          });
        },
        title: InkWell(
          onTap: () => launchUrl(option.link!),
          child: Text(
            option.text,
            style: TextStyle(
              color: option.link != null ? Colors.blue : Colors.black,
              decoration: option.link != null
                  ? TextDecoration.underline
                  : TextDecoration.none,
              decorationColor: Colors.blue,
            ),
            softWrap: true,
            overflow: TextOverflow.ellipsis,
          ),
        ),
        secondary: Text(option.scId.toString()),
      ),
    );
  }

  Widget buildBottomButtons() {
    return Align(
      alignment: Alignment.bottomCenter,
      child: SizedBox(
        height: 100,
        child: FittedBox(
          fit: BoxFit.contain,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              buildActionButton(
                  "skip", Icons.redo, const Color.fromARGB(255, 100, 100, 100)),
              buildActionButton(
                  "yes", Icons.send, const Color.fromARGB(255, 36, 99, 66)),
            ],
          ),
        ),
      ),
    );
  }

  Widget buildActionButton(
      String promptAction, IconData icon, Color backgroundColor) {
    return Padding(
      padding: const EdgeInsets.all(4.0),
      child: ElevatedButton(
        onPressed: () {
          if (promptAction == "yes") {
            sendSCResponse(widget.game, widget.promptId, false, selectedOption);
          }
          widget.nextPrompt(promptAction);
        },
        style: ElevatedButton.styleFrom(
          backgroundColor: backgroundColor,
        ),
        child: Icon(icon, color: Colors.white),
      ),
    );
  }
}
