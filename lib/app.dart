import 'package:como/config.dart';
import 'package:como/l10n/app_localizations.dart';
import 'package:como/login.dart';
import 'package:como/pages/games_list.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:http/http.dart';
import 'pages/home.dart';
import 'pages/game_create.dart';
import 'pages/settings.dart';
import 'pages/game_details.dart';

class ComoApp extends StatefulWidget {
  const ComoApp({super.key});

  static ComoAppState? of(BuildContext context) =>
      context.findAncestorStateOfType<ComoAppState>();

  @override
  ComoAppState createState() => ComoAppState();
}

class ComoAppState extends State<ComoApp> {
  Locale? _locale;
  ThemeMode _themeMode = ThemeMode.system;
  bool _isLoading = true;

  late final GoRouter _router;

  @override
  void initState() {
    super.initState();
    _initializeApp();
    GoRouter.optionURLReflectsImperativeAPIs = true;

    _router = GoRouter(
      initialLocation: '/',
      routes: [
        GoRoute(
          path: '/',
          redirect: (context, state) async {
            final token = state.uri.queryParameters['token'];
            final username = state.uri.queryParameters['username'];

            if (token != null && username != null) {
              log.info("Wikimedia Login URL detected. Logging in...");
              await handleWikimediaLogin(username, token);

              // Remove them from the query to clean up the URL
              final newQueryParams = Map.of(state.uri.queryParameters)
                ..remove('token')
                ..remove('username');

              final newUri = state.uri.replace(queryParameters: newQueryParams);
              return newUri.toString();
            }
            return null;
          },
          builder: (context, state) => const HomePage(),
        ),
        GoRoute(
          path: '/creategame',
          builder: (context, state) => const GameCreatePage(),
        ),
        GoRoute(
          path: '/games',
          builder: (context, state) => const GamesListPage(),
        ),
        GoRoute(
          path: '/settings',
          builder: (context, state) => const SettingsPage(),
        ),
        GoRoute(
          path: '/game/:slug',
          builder: (context, state) {
            final slug = state.pathParameters['slug'] ?? '';
            return GameDetailsPage(gameSlug: slug);
          },
        ),
      ],
    );
  }

  Future<void> _initializeApp() async {
    try {
      log.info("Handling standard login flow.");
      await handleStandardLogin();
    } catch (e) {
      log.info("Error during initialization: $e");
    } finally {
      setState(() {
        _isLoading = false;
      });
    }
  }

  Future<void> handleWikimediaLogin(String username, String token) async {
    try {
      await newWMLogin(username, token);
      log.info("Wikimedia login successful.");
    } on ClientException catch (e) {
      log.warning("MW Login Failed: $e");
    } catch (e) {
      log.warning("Wikimedia login failed with error: $e");
    }
  }

  Future<void> handleStandardLogin() async {
    try {
      await readCredentials();
      log.info("Standard login successful.");
    } on ClientException catch (e) {
      log.warning("Connection failed: $e");
    } catch (e) {
      log.warning("Standard login failed with error: $e");
    }
  }

  void setLocale(Locale newLocale) {
    setState(() {
      _locale = newLocale;
    });
  }

  void setThemeMode(String? themeSelection) {
    ThemeMode newThemeMode = ThemeMode.system;
    if (themeSelection == 'light') {
      newThemeMode = ThemeMode.light;
    } else if (themeSelection == 'dark') {
      newThemeMode = ThemeMode.dark;
    }
    setState(() {
      _themeMode = newThemeMode;
    });
  }

  @override
  Widget build(BuildContext context) {
    if (_isLoading) {
      return MaterialApp(
        home: Scaffold(
          body: Center(
            child: CircularProgressIndicator(),
          ),
        ),
      );
    }

    return MaterialApp.router(
      title: 'Como',
      routerConfig: _router,
      // Themes
      theme: ThemeData(
        primarySwatch: Colors.blue,
        brightness: Brightness.light,
        fontFamily: 'Roboto',
        textTheme: Theme.of(context).textTheme.apply(fontSizeFactor: 1.5),
      ),
      darkTheme: ThemeData(
        primarySwatch: Colors.cyan,
        brightness: Brightness.dark,
        fontFamily: 'Roboto',
        textTheme: Theme.of(context).textTheme.apply(
              fontSizeFactor: 1.5,
              bodyColor: Colors.white,
              displayColor: Colors.white,
            ),
      ),
      themeMode: _themeMode,
      // Localization
      locale: _locale ?? Locale(user.language),
      localizationsDelegates: AppLocalizations.localizationsDelegates,
      supportedLocales: AppLocalizations.supportedLocales,
      debugShowCheckedModeBanner: false,
    );
  }
}
