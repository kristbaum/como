package org.kristbaum.como.database.Daos;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import org.kristbaum.como.database.models.User;

@Dao
public interface UserDao {
    @Query("SELECT * FROM user_table WHERE uid IS 1")
    LiveData<User> loadUserData();

    @Query("SELECT * FROM user_table WHERE uid IS 1")
    User loadUserDataNotLive();

    @Update
    void updateUser(User user);

    @Insert
    void insertUser(User user);
}