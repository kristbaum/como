import 'package:como/model/prompts/response_option.dart';
import 'package:como/model/responses/et_response.dart';
import 'package:flutter/material.dart';
import 'package:como/config.dart';
import 'package:como/model/game.dart';

class ETResponseWidget extends StatefulWidget {
  final Function(String) nextPrompt;
  final Game game;
  final int promptId;
  final ResponseOption responseOption;
  final String leadingFigure;

  const ETResponseWidget({
    super.key,
    required this.nextPrompt,
    required this.game,
    required this.promptId,
    required this.responseOption,
    required this.leadingFigure,
  });

  @override
  ETResponseWidgetState createState() => ETResponseWidgetState();
}

class ETResponseWidgetState extends State<ETResponseWidget> {
  TextEditingController controller = TextEditingController();
  bool isEditing = false;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        buildTopBox(),
        Align(
          alignment: Alignment.bottomCenter,
          child: SizedBox(
            height: 100,
            child: FittedBox(
              fit: BoxFit.contain,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  buildEditButton(),
                  buildSkipButton(),
                  buildSendButton(),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }

  Padding buildSendButton() {
    return Padding(
      padding: const EdgeInsets.all(4.0),
      child: ElevatedButton(
        onPressed: () {
          try {
            sendETResponse(
                false,
                isEditing ? controller.text : widget.responseOption.text,
                widget.game,
                widget.promptId);
            isEditing = false; // Reset the editing state
            widget.nextPrompt("yes");
            user.missingForStreakToday = 0;
          } catch (e) {
            log.warning("Failed to send response");
          }
        },
        style: ElevatedButton.styleFrom(
          backgroundColor: const Color.fromARGB(255, 36, 99, 66),
        ),
        child: Icon(isEditing ? Icons.send : Icons.check, color: Colors.white),
      ),
    );
  }

  Padding buildSkipButton() {
    return Padding(
      padding: const EdgeInsets.all(4.0),
      child: ElevatedButton(
        onPressed: () {
          sendETResponse(true, controller.text, widget.game, widget.promptId);
          isEditing = false; // Reset the editing state
          widget.nextPrompt("skip");
        },
        style: ElevatedButton.styleFrom(
          backgroundColor: const Color.fromARGB(255, 100, 100, 100),
        ),
        child: const Icon(
          Icons.redo,
          color: Colors.white,
        ),
      ),
    );
  }

  Padding buildEditButton() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: ElevatedButton(
        onPressed: () {
          // Reset the text to the original value
          controller.text = widget.responseOption.text;
          setState(() {
            isEditing = !isEditing;
          });
        },
        style: ElevatedButton.styleFrom(
          backgroundColor: const Color.fromARGB(255, 12, 87, 168),
        ),
        child: isEditing
            ? const Icon(Icons.cancel, color: Colors.white)
            : const Icon(Icons.edit, color: Colors.white),
      ),
    );
  }

  Card buildTopBox() {
    return Card(
      child: ListTile(
        title: Text(
          widget.leadingFigure,
          style: const TextStyle(
            fontSize: 12.0,
            color: Colors.grey,
          ),
        ),
        subtitle: Padding(
          padding: const EdgeInsets.all(8.0),
          child: isEditing
              ? TextField(
                  minLines: 1,
                  maxLines: 5,
                  autocorrect: true,
                  controller: controller,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                  ),
                  textAlign: TextAlign.center,
                )
              : Text(
                  widget.responseOption.text,
                  textAlign: TextAlign.center,
                ),
        ),
      ),
    );
  }
}
