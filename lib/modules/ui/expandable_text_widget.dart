import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class ExpandableTextWidget extends StatefulWidget {
  final String title;
  final String text;
  final Uri? link;

  const ExpandableTextWidget({
    super.key,
    required this.title,
    required this.text,
    this.link,
  });

  @override
  ExpandableTextWidgetState createState() => ExpandableTextWidgetState();
}

class ExpandableTextWidgetState extends State<ExpandableTextWidget> {
  bool isExpanded = false;

  void _toggleExpanded() {
    setState(() {
      isExpanded = !isExpanded;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      child: ListTile(
        title: _buildTitle(),
        subtitle: _buildSubtitle(context),
      ),
    );
  }

  Widget _buildTitle() {
    return Text(
      widget.title,
      style: const TextStyle(
        fontSize: 12.0,
        color: Colors.grey,
      ),
    );
  }

  Widget _buildSubtitle(BuildContext context) {
    return Center(
      child: InkWell(
        onTap: _handleTap,
        child: Text(
          widget.text,
          style: TextStyle(
            color: widget.link != null ? Colors.blue : null,
            decoration: widget.link != null
                ? TextDecoration.underline
                : TextDecoration.none,
            decorationColor: widget.link != null ? Colors.blue : null,
          ),
          softWrap: true,
          overflow: isExpanded ? TextOverflow.visible : TextOverflow.ellipsis,
          maxLines:
              isExpanded ? null : 3, // Expand or contract based on `isExpanded`
        ),
      ),
    );
  }

  void _handleTap() {
    if (widget.link != null) {
      launchUrl(widget.link!);
    } else {
      _toggleExpanded();
    }
  }
}
