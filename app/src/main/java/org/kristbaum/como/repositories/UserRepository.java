package org.kristbaum.como.repositories;

import android.app.Application;
import android.os.AsyncTask;
import android.util.Log;

import androidx.lifecycle.LiveData;

import org.kristbaum.como.database.AppDatabase;
import org.kristbaum.como.database.Daos.UserDao;
import org.kristbaum.como.database.models.User;

public class UserRepository {
    private final UserDao userDao;
    private final LiveData<User> userData;

    public UserRepository(Application application) {
        AppDatabase database = AppDatabase.getInstance(application);
        userDao = database.userDao();
        userData = userDao.loadUserData();
    }

    public LiveData<User> getUserData() {
        return  userData;
    }

    public void updateUser(User user) {
        new updateUserAsyncTask(userDao).execute(user);
    }

    private static class updateUserAsyncTask extends AsyncTask<User, Void, Void> {
        private final UserDao userDao;
        private updateUserAsyncTask(UserDao userDao) {
            this.userDao = userDao;
        }

        @Override
        protected Void doInBackground(User... users) {
            userDao.updateUser(users[0]);
            return null;
        }
    }

    public void insertUser(User user) {
        new insertUserAsyncTask(userDao).execute(user);
    }

    private static class insertUserAsyncTask extends AsyncTask<User, Void, Void> {
        private final UserDao userDao;

        private insertUserAsyncTask(UserDao userDao) {
            this.userDao = userDao;
        }

        @Override
        protected Void doInBackground(User... users) {
            userDao.insertUser(users[0]);
            Log.v("Userrepo", "User inserted");
            return null;
        }
    }

    public User loadUserDataNotLive() {
        return userDao.loadUserDataNotLive();
    }
}
