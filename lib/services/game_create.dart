import 'dart:convert';
import 'package:archive/archive.dart';
import 'package:como/config.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

Future<bool> createGame({
  required List<String> requestHTTPMethods,
  required List<TextEditingController> requestURLControllers,
  required List<TextEditingController> requestBodyControllers,
  required String name,
  required String promptType,
  required String promptLabels,
  required String responseType,
  required String shortDescription,
  required String longDescription,
  required String question,
  required String actionType,
  required bool listPublicly,
  required String confidence,
}) async {
  final List<Map<String, String>> actionRequests = List.generate(
    requestHTTPMethods.length,
    (i) => {
      'request_http_method': requestHTTPMethods[i],
      'request_url': requestURLControllers[i].text,
      'request_body': requestBodyControllers[i].text,
    },
  );

  final Map<String, dynamic> gameData = {
    'name': name,
    'prompt_type': promptType,
    'prompt_labels': promptLabels,
    'response_type': responseType,
    'short_description': shortDescription,
    'long_description': longDescription,
    'question': question,
    'action_type': actionType,
    'action_requests': jsonEncode(actionRequests),
    'is_listed': listPublicly.toString(),
    'confidence_type': confidence,
  };

  final List<int> bytes = utf8.encode(jsonEncode(gameData));
  final List<int> compressedData = GZipEncoder().encode(bytes);

  log.info("Sending create game request with compressed data");
  final response = await client
      .post(
        Uri.parse('$backendUrl/api/games/'),
        headers: {...defaultRequestHeaders, 'Content-Encoding': 'gzip'},
        body: compressedData,
      )
      .timeout(const Duration(seconds: 120));

  log.info("Response status: ${response.statusCode}");
  if (response.statusCode == 201) {
    log.info("Game created");
    return true;
  } else {
    log.info("Error creating game");
    throw Exception('Failed to create game with status ${response.body}');
  }
}

Future<bool> createPrompts(String gameSlug, String data) async {
  final Map<String, dynamic> gameData = {
    'data': data,
  };

  final List<int> bytes = utf8.encode(jsonEncode(gameData));
  final List<int> compressedData = GZipEncoder().encode(bytes);

  log.info("Sending create game request with compressed data");
  final response = await client
      .post(
        Uri.parse('$backendUrl/api/games/$gameSlug/create-prompts/'),
        headers: {...defaultRequestHeaders, 'Content-Encoding': 'gzip'},
        body: compressedData,
      )
      .timeout(const Duration(seconds: 120));

  log.info("Response status: ${response.statusCode}");
  if (response.statusCode == 201) {
    log.info("Game created");
    return true;
  } else {
    log.info("Error creating game");
    throw Exception('Failed to create game with status ${response.body}');
  }
}

Card buildDocumentationCard() {
  return Card(
    child: ListTile(
      leading: const Icon(Icons.help),
      title: InkWell(
        child: const Text('Read the Documentation first!',
            style: TextStyle(color: Colors.blue)),
        onTap: () =>
            launchUrl(Uri.parse('https://meta.wikimedia.org/wiki/Como')),
      ),
    ),
  );
}
