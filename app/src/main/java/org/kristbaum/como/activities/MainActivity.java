package org.kristbaum.como.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import org.kristbaum.como.R;
import org.kristbaum.como.client.Invoker;
import org.kristbaum.como.database.models.User;
import org.kristbaum.como.viewModels.MainViewModel;
import org.kristbaum.como.viewModels.SyncViewModel;

import java.util.concurrent.Executor;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";
    private MainViewModel mainViewModel;
    private SyncViewModel syncViewModel;
    private boolean initial = true;
    private Button senseCreationButton;
    private TextView textViewPoints;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.v(TAG, "Main activity started");
        Log.v(TAG,"Check connection to DB");

        this.mainViewModel = new ViewModelProvider(this).get(MainViewModel.class);
        this.syncViewModel = new ViewModelProvider(this).get(SyncViewModel.class);
        mainViewModel.createDB(getApplicationContext());
        preloadData();

        textViewPoints = findViewById(R.id.textViewPoints);

        final Button startButton = findViewById(R.id.startButton);
        startButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                launchActivity();
            }
        });

        senseCreationButton = findViewById(R.id.senseCreationButton);
        senseCreationButton.setEnabled(false);
        senseCreationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchCreatorActivity();
            }
        });

        /*final Button creatorButton = findViewById(R.id.creatorButton);
        creatorButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                launchCreatorActivity();
            }
        });*/

        /*final Button httpButton = findViewById(R.id.buttonHttp);
        httpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                syncData();
            }
        });*/

        final Button statsButton = findViewById(R.id.buttonStats);
        statsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchStatsActivity();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finishAffinity();
    }

    private void syncData() {
        Executor executor = new Invoker();
        executor.execute(new Runnable() {
            @Override
            public void run() {
                syncViewModel.syncData();
            }
        });
    }

    private void preloadData() {
        mainViewModel.getUserData().observe(this, new Observer<User>() {
            @Override
            public void onChanged(User user) {
                Log.v(TAG, "Userdata changed");
                if (user == null) {
                    Log.v(TAG, "New user created");
                    mainViewModel.userSetup();
                    Log.v(TAG, "Initial data sync");
                    syncData();
                    launchStatsActivity();
                } else {
                    mainViewModel.setValues(user);
                    if (initial) {
                        initial = false;
                    }

                    int credits = mainViewModel.getSenseCreationCredits(user);
                    senseCreationButton.setText(String.valueOf(credits));
                    if (credits > 0) {
                        senseCreationButton.setEnabled(true);
                    } else {
                        senseCreationButton.setEnabled(false);
                    }

                    int points = mainViewModel.getPoints(user);
                    textViewPoints.setText(String.valueOf(points));
                }

            }
        });
    }

    private void launchCreatorActivity() {
        Intent intent = new Intent(this, CreationActivity.class);
        startActivity(intent);
    }

    private void launchStatsActivity() {
        Intent intent = new Intent(this, SettingsActivity.class);
        startActivity(intent);
    }

    private void launchActivity() {
        Intent intent = new Intent(this, GuessingActivity.class);
        startActivity(intent);
    }
}
