import 'package:como/config.dart';
import 'package:como/model/game.dart';
import 'package:como/model/responses/yn_response.dart';
import 'package:flutter/material.dart';

Widget buildYNResponse(Function(String) nextPrompt, Game game, int promptId) {
  Widget createResponseButton(Color color, IconData icon, String result,
      bool skipped, bool answerIsYes) {
    return Padding(
      padding: const EdgeInsets.all(4.0),
      child: ElevatedButton(
        onPressed: () {
          try {
            sendYNResponse(skipped, answerIsYes, game, promptId);
            nextPrompt(result);
          } catch (e) {
            log.warning("Failed to send response");
          }
        },
        style: ElevatedButton.styleFrom(backgroundColor: color),
        child: Icon(icon, color: Colors.white),
      ),
    );
  }

  return Align(
    alignment: Alignment.bottomCenter,
    child: Padding(
      padding: const EdgeInsets.all(8.0),
      child: SizedBox(
        height: 100,
        child: FittedBox(
          fit: BoxFit.contain,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              createResponseButton(const Color.fromARGB(255, 151, 3, 2),
                  Icons.close, "no", false, false),
              createResponseButton(const Color.fromARGB(255, 100, 100, 100),
                  Icons.redo, "skip", true, false),
              createResponseButton(const Color.fromARGB(255, 36, 99, 66),
                  Icons.check, "yes", false, true),
            ],
          ),
        ),
      ),
    ),
  );
}
