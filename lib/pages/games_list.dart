import 'package:como/config.dart';
import 'package:como/model/game.dart';
import 'package:como/services/home.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:share_plus/share_plus.dart';

enum FilterOption { showAll, showActive }

class GamesListPage extends StatefulWidget {
  const GamesListPage({super.key});

  @override
  GamesListPageState createState() => GamesListPageState();
}

class GamesListPageState extends State<GamesListPage> {
  List<Game>? gamesList;
  List<Game>? filteredGamesList;
  bool showInactive = false;

  final TextEditingController searchController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _refreshGames();
  }

  Future<void> _refreshGames() async {
    var games = await fetchGamesList("new", showInactive);
    setState(() {
      gamesList = games;
      filteredGamesList =
          showInactive ? games : games.where((g) => g.isActive).toList();
    });
  }

  void _toggleFilter(FilterOption? option) {
    setState(() {
      showInactive = option == FilterOption.showAll;
      _refreshGames();
    });
  }

  void _filterGames(String query) {
    setState(() {
      filteredGamesList = query.isEmpty
          ? gamesList
          : gamesList
              ?.where((game) =>
                  game.name.toLowerCase().contains(query.toLowerCase()))
              .toList();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildAppBar(),
      floatingActionButton: _buildFloatingActionButton(),
      body: RefreshIndicator(
        onRefresh: _refreshGames,
        child: SingleChildScrollView(
          child: Center(
            child: ConstrainedBox(
              constraints: const BoxConstraints(maxWidth: appWidth),
              child: _buildGamesList(),
            ),
          ),
        ),
      ),
    );
  }

  AppBar _buildAppBar() {
    return AppBar(
      title: TextField(
        controller: searchController,
        decoration: const InputDecoration(
          hintText: "Search games...",
          border: InputBorder.none,
          suffixIcon: Icon(Icons.search),
        ),
        onChanged: _filterGames,
      ),
      actions: <Widget>[
        _buildPopupMenu(),
      ],
    );
  }

  FloatingActionButton _buildFloatingActionButton() {
    return FloatingActionButton(
      onPressed: () {
        context.push('/creategame');
      },
      child: const Icon(Icons.add),
    );
  }

  Widget _buildPopupMenu() {
    return PopupMenuButton<FilterOption>(
      onSelected: _toggleFilter,
      itemBuilder: (BuildContext context) => <PopupMenuEntry<FilterOption>>[
        CheckedPopupMenuItem<FilterOption>(
          value: FilterOption.showAll,
          checked: showInactive,
          child: const Text('Show All Games'),
        ),
        CheckedPopupMenuItem<FilterOption>(
          value: FilterOption.showActive,
          checked: !showInactive,
          child: const Text('Show Active Games'),
        ),
      ],
    );
  }

  Widget _buildGamesList() {
    if (filteredGamesList == null || filteredGamesList!.isEmpty) {
      return const Center(
          child: Text("No games created yet or no matches found"));
    }
    return ListView.builder(
      physics: const NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      itemCount: filteredGamesList!.length,
      itemBuilder: (context, index) {
        return _buildGameCard(filteredGamesList![index]);
      },
    );
  }

  Widget _buildGameCard(Game game) {
    return Card(
      child: ListTile(
        title: Text(
          game.name,
          overflow: TextOverflow.ellipsis,
          maxLines: 2,
        ),
        subtitle: Text(
          game.shortDescription,
          overflow: TextOverflow.ellipsis,
          maxLines: 2,
        ),
        trailing: _buildTrailingIcons(game),
        onTap: () {
          context.push('/game/${game.slug}');
        },
      ),
    );
  }

  Widget _buildTrailingIcons(Game game) {
    final double screenWidth = MediaQuery.of(context).size.width;
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        screenWidth > appWidth
            ? Text(game.createdAtShort)
            : const SizedBox.shrink(),
        IconButton(
          icon: const Icon(Icons.share),
          tooltip: "Share this game",
          onPressed: () {
            Share.share('https://como.toolforge.org/#/game/${game.slug}');
          },
        ),
      ],
    );
  }
}
