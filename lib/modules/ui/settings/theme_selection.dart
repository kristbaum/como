import 'package:flutter/material.dart';

class ThemeSettingsCard extends StatelessWidget {
  final String? selectedTheme;
  final ValueChanged<String?> onThemeChanged;

  const ThemeSettingsCard({
    super.key,
    required this.selectedTheme,
    required this.onThemeChanged,
  });

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          ListTile(
            leading: const Icon(Icons.dark_mode),
            title: Text("Change Theme"),
            subtitle: DropdownButton<String>(
              value: selectedTheme,
              items: <String>['dark', 'light', 'default']
                  .map<DropdownMenuItem<String>>((String value) {
                return DropdownMenuItem<String>(
                  value: value,
                  child: Text(value),
                );
              }).toList(),
              onChanged: onThemeChanged,
              underline: Container(),
            ),
          ),
        ],
      ),
    );
  }
}
