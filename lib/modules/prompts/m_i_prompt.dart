import 'package:como/config.dart';
import 'package:como/model/prompts/m_prompt.dart';
import 'package:como/modules/prompts/base_prompt.dart';
import 'package:como/modules/responses/build_response.dart';
import 'package:como/modules/ui/image_card_widget.dart';
import 'package:como/modules/utils.dart';
import 'package:como/services/game_play.dart';
import 'package:flutter/material.dart';

class IPromptModule extends BasePromptModule<MPrompt> {
  const IPromptModule({super.key, required super.game});

  @override
  IPromptModuleState createState() => IPromptModuleState();
}

class IPromptModuleState extends BasePromptModuleState<MPrompt> {
  @override
  Future<List<MPrompt>> addNewPrompts() async {
    return await getIPrompts(widget.game);
  }

  @override
  Widget buildPromptContent(MPrompt prompt) {
    if (prompts.length > 1) {
      log.info("Precaching image: ${prompts[1].media1}");
      precacheImage(NetworkImage(prompts[1].media1.toString()), context);
    }
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Flexible(
          child: SingleChildScrollView(
            child: Center(
              child: ConstrainedBox(
                constraints: const BoxConstraints(maxWidth: appWidth),
                child: ListView(
                  physics: const NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  children: [
                    questionWidget(prompt.question),
                    const SizedBox(height: 10),
                    ImageCardWidget(
                      title: addPromptLabel(0, widget.game.promptLabelsList),
                      imageUri: prompt.media1,
                      imageFilename: prompt.media1Filename,
                      imageFilenameUrl: prompt.media1FilenameUrl,
                    ),
                    const SizedBox(height: 10),
                  ],
                ),
              ),
            ),
          ),
        ),
        buildResponse(nextPrompt, widget.game.responseType, widget.game, prompt,
            addPromptLabel(1, widget.game.promptLabelsList)),
      ],
    );
  }
}
