import 'package:flutter/material.dart';

class OptionCard extends StatelessWidget {
  const OptionCard({
    super.key,
    required this.title,
    required this.groupValue,
    required this.optionWidget,
  });

  final String title;
  final String groupValue;
  final Widget optionWidget;

  @override
  Widget build(BuildContext context) {
    return Card(
      child: ListTile(
        title: Text(title),
        subtitle: optionWidget,
      ),
    );
  }
}
