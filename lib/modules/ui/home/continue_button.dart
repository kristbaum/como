import 'package:como/model/game.dart';
import 'package:flutter/material.dart';
import 'package:como/l10n/app_localizations.dart';
import 'package:go_router/go_router.dart';

class ContinueButton extends StatelessWidget {
  const ContinueButton({
    super.key,
    this.lastPlayedGameFuture,
  });

  final Future<Game?>? lastPlayedGameFuture;

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<Game?>(
      future: lastPlayedGameFuture,
      builder: (context, snapshot) {
        VoidCallback? buttonAction;
        if (snapshot.connectionState != ConnectionState.waiting &&
            snapshot.hasData) {
          buttonAction = () => context.push('/game/${snapshot.data!.slug}');
        }
        return Center(
          child: TextButton(
            onPressed: buttonAction,
            child: Text(AppLocalizations.of(context)!.continue_button),
          ),
        );
      },
    );
  }
}
