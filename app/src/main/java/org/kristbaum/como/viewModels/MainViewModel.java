package org.kristbaum.como.viewModels;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.room.Room;

import org.json.JSONException;
import org.json.JSONObject;
import org.kristbaum.como.database.AppDatabase;
import org.kristbaum.como.database.models.User;
import org.kristbaum.como.repositories.UserRepository;

import java.util.Locale;
import java.util.UUID;

public class MainViewModel extends AndroidViewModel {
    private static final String TAG = "MainViewModel";

    private final UserRepository userRepository;
    private final LiveData<User> userData;
    private static String uuid;
    private static String lemma_lang;
    private static String sense_lang;
    private final static int pointsToSenseCreation = 4;


    public MainViewModel(@NonNull Application application) {
        super(application);
        userRepository = new UserRepository(application);
        userData = userRepository.getUserData();
    }


    public LiveData<User> getUserData() {
        return userData;
    }

    public void insertUser(User user) {
        userRepository.insertUser(user);
    }

    public static void setUUID(String uuid) {
        MainViewModel.uuid = uuid;
    }

    public static String getUniqueID() {
        return uuid;
    }

    public void createDB(Context applicationContext) {
        Room.databaseBuilder(applicationContext,
                AppDatabase.class, "como_database").build();
    }


    public void userSetup() {
        //Initialise User if it doesn't exist
        String lang = Locale.getDefault().getLanguage();
        String uuid = UUID.randomUUID().toString();
        Log.v(TAG, "New User created: " + uuid);
        MainViewModel.setUUID(uuid);
        MainViewModel.setLemma_lang(lang);
        MainViewModel.setSense_lang(lang);
        JSONObject prefs = new JSONObject();
        JSONObject emptyStats = new JSONObject();
        try {
            emptyStats.put("times_guessed_correct", 0);
            emptyStats.put("times_guessed_wrong", 0);
            emptyStats.put("times_skipped_games", 0);
            emptyStats.put("times_looked_at_tips", 0);
            emptyStats.put("times_sense_created", 0);
            emptyStats.put("times_skipped_sense_creation", 0);
            emptyStats.put("times_others_guessed_correctly", 0);
            emptyStats.put("times_guessed_correct_since_sense_creation", 0);
            emptyStats.put("games_downloaded", 0);
            emptyStats.put("sense_creations_downloaded", 0);
            emptyStats.put("useful_interactions_since_last_sync", 0);
            emptyStats.put("points", 0);

            prefs.put("lemma_lang", lang);
            prefs.put("sense_lang", lang);
            prefs.put("client", "android");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        User newUser = new User(uuid);
        newUser.setStats(emptyStats.toString());
        newUser.setPrefs(prefs.toString());
        insertUser(newUser);
    }

    public static String getLemma_lang() {
        return lemma_lang;
    }

    public static void setLemma_lang(String lemma_lang) {
        MainViewModel.lemma_lang = lemma_lang;
    }

    public static String getSense_lang() {
        return sense_lang;
    }

    public static void setSense_lang(String sense_lang) {
        MainViewModel.sense_lang = sense_lang;
    }

    public static int getPointsToSenseCreation() {
        return pointsToSenseCreation;
    }

    public void setValues(User user) {
        if (MainViewModel.getUniqueID() == null) {
            MainViewModel.setUUID(user.getUuid());
        }
        try {
            JSONObject prefs = new JSONObject(user.getPrefs());
            MainViewModel.setLemma_lang(prefs.getString("lemma_lang"));
            MainViewModel.setSense_lang(prefs.getString("sense_lang"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public int getSenseCreationCredits(User user) {
        int result = 0;
        try {
            JSONObject stats = new JSONObject(user.getStats());
            int solved = stats.getInt("times_guessed_correct_since_sense_creation");
            result = solved / 4;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public int getPoints(User user) {
        int result = 0;
        try {
            JSONObject stats = new JSONObject(user.getStats());
            result = stats.getInt("points");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }
}