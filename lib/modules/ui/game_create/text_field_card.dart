import 'package:flutter/material.dart';

class TextFieldCard extends StatelessWidget {
  final String title;
  final TextEditingController controller;
  final String hintText;
  final int maxLength;
  final String validatorMessage;
  final int minLines;
  final int maxLines;

  const TextFieldCard({
    super.key,
    required this.title,
    required this.controller,
    required this.hintText,
    required this.maxLength,
    required this.validatorMessage,
    this.minLines = 1,
    this.maxLines = 1,
  });

  @override
  Widget build(BuildContext context) {
    return Card(
      child: ListTile(
        title: Text(title),
        subtitle: TextFormField(
          maxLength: maxLength,
          controller: controller,
          minLines: minLines,
          maxLines: maxLines,
          decoration: InputDecoration(hintText: hintText),
          validator: (value) => value!.isEmpty ? validatorMessage : null,
        ),
      ),
    );
  }
}
