package org.kristbaum.como.activities;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import org.json.JSONException;
import org.json.JSONObject;
import org.kristbaum.como.R;
import org.kristbaum.como.database.models.User;
import org.kristbaum.como.viewModels.MainViewModel;
import org.kristbaum.como.viewModels.SettingsViewModel;

public class SettingsActivity extends AppCompatActivity {
    private static final String TAG = "UserActivity";
    private TextView textViewPlayed;
    private TextView textViewCorrect;
    private TextView textViewPoints;
    private User activeUser;
    private SettingsViewModel settingsViewModel;
    private final String defaultLang = "de";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        settingsViewModel = new ViewModelProvider(this).get(SettingsViewModel.class);
        settingsViewModel.getUserData().observe(this, new Observer<User>() {
            @Override
            public void onChanged(User user) {
                activeUser = user;
                setInfo(user.getStats());
            }
        });
        textViewPlayed = findViewById(R.id.textViewPlayed);
        textViewCorrect = findViewById(R.id.textViewCorrect);
        textViewPoints = findViewById(R.id.textViewPoints);
        setSpinner();
        final Button syncButton = findViewById(R.id.buttonSync);
        syncButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                settingsViewModel.sync();
            }
        });


        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    private void setSpinner() {
        final Spinner spinner = findViewById(R.id.spinnerLanguage);
        int lang = getLangInt(activeUser);
        Log.v(TAG, "Language from user: " + lang);
        spinner.setSelection(lang);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String string = String.valueOf(parent.getItemAtPosition(position));
                Log.v(TAG, "Language selected: " + string + position);
                setLangInt(position, activeUser);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });
    }


    private void setInfo(String userStats) {
        try {
            JSONObject stats = new JSONObject(userStats);
            int correct = stats.getInt("times_guessed_correct");
            int skipped = stats.getInt("times_skipped_games");
            int points = stats.getInt("points");
            textViewPlayed.setText(String.valueOf(correct + skipped));
            textViewCorrect.setText(String.valueOf(correct));
            textViewPoints.setText(String.valueOf(points));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public int getLangInt(User activeUser) {
        String lang;
        lang = MainViewModel.getLemma_lang();
        if (lang.equals("en")) {
            return 0;
        } else if (lang.equals("de")) {
            return 1;
        }
        return 1;
    }

    public void setLangInt(int position, User activeUser) {
        String lang = defaultLang;
        if (position == 0) {
            lang = "en";
        } else if (position == 1) {
            lang = "de";
        }

        if (!lang.equals(MainViewModel.getLemma_lang())) {
            try {
                JSONObject prefs = new JSONObject(activeUser.getPrefs());
                prefs.put("lemma_lang", lang);
                prefs.put("sense_lang", lang);
                activeUser.setPrefs(prefs.toString());
                MainViewModel.setLemma_lang(lang);
                MainViewModel.setSense_lang(lang);
                settingsViewModel.updateUser(activeUser);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}