# Como

Template for toolforge.org services using flutter, including OAUTH

## Setup

Run `flutter build web` locally and commit the build folder to your project (watch out for included secrets!)

ssh login.toolforge.org
become como

Clone the project via https in toolforge in your $HOME folder.

Symlink to the build/web folder:

```bash
ln -s $HOME/como/build/web $HOME/public_html
```

Create a service.template file with the following contents in $HOME (we are using the default backend, because we only need the webserver):

```bash
# Toolforge webservice template
# Provide default arguments for `webservice start` commands for this tool.
#
# Uncomment lines below and adjust as needed

# Set backend cluster to run this webservice (--backend={gridengine,kubernetes})
backend: kubernetes

# Set Kubernetes cpu limit (--cpu=...)
#cpu: 500m

# Set Kubernetes memory limit (--mem=...)
#mem: 512Mi

# Set ReplicaSet size for a Kubernetes deployment (--replicas=...)
#replicas: 2

# Runtime type
# See "Supported webservice types" in `webservice --help` output for valid values.
# type: 

# Extra arguments to be parsed by the chosen TYPE
#extra_args:
#  - arg0
#  - arg1
#  - arg2
```

## Development setup for Ubuntu

```bash
sudo snap install --classic flutter
sudo snap install --classic android-studio
sudo snap install chromium
echo -e "export CHROME_EXECUTABLE=/snap/bin/chromium" >> ~/.bashrc
echo -e "alias google-chrome='chromium'" >> ~/.bashrc
flutter gen-l10n
```

## New icons

```bash
dart run flutter_launcher_icons -f flutter_launcher_icons.yaml
```

## Deeplink debugging

```bash
adb shell 'am start -a android.intent.action.VIEW \
    -c android.intent.category.BROWSABLE \
    -d "http://como.toolforge.org"' \
    org.toolforge.como

adb shell pm get-app-links org.toolforge.como
```

## Toolforge setup + Release

Create file key.properties

To use these buildoptions:

```bash
flutter build web --no-web-resources-cdn
```

Edit the file:

```bash
nano ~/.lighttpd.conf
mimetype.assign += (
    ".wasm" => "application/wasm"
)
```

```bash
flutter build web
# Commit changes
ssh login.toolforge.org
become como
git -C como pull
toolforge webservice stop
toolforge webservice start
```

or  run

```bash
cat update_toolforge.sh | ssh login.toolforge.org
```

```bash
dart run build_runner watch --delete-conflicting-outputs
dart run build_runner build --delete-conflicting-outputs
```
