import 'package:como/config.dart';
import 'package:como/modules/utils.dart';
import 'package:flutter/material.dart';
import 'package:como/l10n/app_localizations.dart';
import 'package:go_router/go_router.dart';
import 'package:url_launcher/url_launcher.dart';

Widget drawer(BuildContext context) {
  return Drawer(
    child: ListView(
      padding: EdgeInsets.zero,
      children: [
        _buildDrawerHeader(),
        _buildUserProfile(context),
        const Divider(thickness: 10),
        _buildMenuTile(context,
            title: AppLocalizations.of(context)!.drawer_create,
            subtitle: AppLocalizations.of(context)!.drawer_create_desc,
            path: "creategame",
            icon: Icons.add),
        _buildMenuTile(context,
            title: AppLocalizations.of(context)!.drawer_gameslist,
            subtitle: AppLocalizations.of(context)!.drawer_gameslist_desc,
            path: "games",
            icon: Icons.list),
        const Divider(thickness: 10),
        _buildMenuTile(context,
            title: AppLocalizations.of(context)!.drawer_settings,
            subtitle: "",
            path: "settings",
            icon: Icons.settings),
        _buildMenuTile(context,
            title: AppLocalizations.of(context)!.drawer_privacy_policy,
            subtitle: "",
            path: "https://foundation.wikimedia.org/wiki/Policy:Privacy_policy",
            icon: Icons.privacy_tip,
            isExternal: true),
        _buildMenuTile(context,
            title: AppLocalizations.of(context)!.drawer_about,
            subtitle: "",
            path: "https://meta.wikimedia.org/wiki/Como",
            icon: Icons.info,
            isExternal: true),
      ],
    ),
  );
}

Widget _buildDrawerHeader() {
  return DrawerHeader(
    decoration: const BoxDecoration(),
    child: Column(
      children: [
        Container(
            padding: const EdgeInsets.all(4),
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              border: Border.all(color: Colors.black, width: 4),
            ),
            child: const Image(
                image: AssetImage('assets/images/icon.png'),
                height: 80,
                fit: BoxFit.cover)),
        const FittedBox(
          child: Text("Como",
              style: TextStyle(fontWeight: FontWeight.w500, fontSize: 22)),
        ),
      ],
    ),
  );
}

Widget _buildUserProfile(BuildContext context) {
  return ListTile(
      title: const Text("Profile"),
      subtitle: user.isWikimediaUser()
          ? Text(user.wikimediaUsername!)
          : Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text("Anonymous User ID:\n${user.username}",
                    overflow: TextOverflow.ellipsis),
                const LoginButton()
              ],
            ),
      isThreeLine: !user.isWikimediaUser(),
      leading: const Icon(Icons.account_circle));
}

Widget _buildMenuTile(BuildContext context,
    {required String title,
    required String subtitle,
    required String path,
    required IconData icon,
    bool isExternal = false}) {
  return ListTile(
    title: Text(
      title,
      style: const TextStyle(fontWeight: FontWeight.w500, fontSize: 22),
    ),
    subtitle: subtitle.isNotEmpty
        ? Text(subtitle, style: const TextStyle(fontSize: 20))
        : null,
    leading: Icon(icon),
    onTap: () {
      //context.pop();
      if (!isExternal) {
        context.push('/$path');
      } else {
        launchUrl(Uri.parse(path));
      }
    },
    trailing: isExternal ? const Icon(Icons.arrow_outward) : null,
  );
}
