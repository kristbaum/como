import 'package:como/config.dart';
import 'package:como/model/prompts/ttt_prompt.dart';
import 'package:como/modules/prompts/base_prompt.dart';
import 'package:como/modules/responses/build_response.dart';
import 'package:como/modules/ui/expandable_text_widget.dart';
import 'package:como/modules/utils.dart';
import 'package:como/services/game_play.dart';
import 'package:flutter/material.dart';

class TTTPromptModule extends BasePromptModule<TTTPrompt> {
  const TTTPromptModule({super.key, required super.game});

  @override
  TTTPromptModuleState createState() => TTTPromptModuleState();
}

class TTTPromptModuleState extends BasePromptModuleState<TTTPrompt> {
  @override
  Future<List<TTTPrompt>> addNewPrompts() async {
    return await getTTTPrompts(widget.game);
  }

  @override
  Widget buildPromptContent(TTTPrompt prompt) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Flexible(
          child: SingleChildScrollView(
            child: Center(
              child: ConstrainedBox(
                constraints: const BoxConstraints(maxWidth: appWidth),
                child: ListView(
                  physics: const NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  children: [
                    questionWidget(prompt.question),
                    const SizedBox(height: 10),
                    ExpandableTextWidget(
                      title:
                          addPromptLabel(0, widget.game.promptLabelsList),
                      text: prompt.text1,
                      link: prompt.link1,
                    ),
                    const SizedBox(height: 5),
                    ExpandableTextWidget(
                      title:
                          addPromptLabel(1, widget.game.promptLabelsList),
                      text: prompt.text2,
                      link: prompt.link2,
                    ),
                    const SizedBox(height: 5),
                    ExpandableTextWidget(
                      title:
                          addPromptLabel(2, widget.game.promptLabelsList),
                      text: prompt.text3,
                      link: prompt.link3,
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
        buildResponse(nextPrompt, widget.game.responseType, widget.game, prompt,
            addPromptLabel(3, widget.game.promptLabelsList)),
      ],
    );
  }
}
