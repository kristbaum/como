import 'package:como/pages/upload_dialog.dart';
import 'package:como/services/game_details.dart';
import 'package:flutter/material.dart';
import 'package:como/l10n/app_localizations.dart';
import 'info_button.dart';

class DataManagementCard extends StatefulWidget {
  final String gameSlug;
  final String suggestedCSVHeaders;

  const DataManagementCard({
    super.key,
    required this.gameSlug,
    required this.suggestedCSVHeaders,
  });

  @override
  DataManagementCardState createState() => DataManagementCardState();
}

class DataManagementCardState extends State<DataManagementCard> {
  bool _isDownloading = false;

  @override
  Widget build(BuildContext context) {
    return Card(
      child: ListTile(
        title: Text("Data"),
        subtitle: LayoutBuilder(
          builder: (context, constraints) {
            // TODO: Find better values for wide screen
            bool isWideScreen = constraints.maxWidth > 500;
            return isWideScreen
                ? Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: _buildActionButtons(),
                  )
                : Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children:
                        _buildActionButtons().map((child) => child).toList(),
                  );
          },
        ),
      ),
    );
  }

  List<Widget> _buildActionButtons() {
    return [
      Row(
        children: [
          ElevatedButton.icon(
            onPressed: _isDownloading ? null : () => _downloadCsv(),
            label: Text(AppLocalizations.of(context)!.download_csv),
            icon: _isDownloading
                ? const SizedBox(
                    width: 20,
                    height: 20,
                    child: CircularProgressIndicator(),
                  )
                : const Icon(Icons.download_sharp),
          ),
          InfoButton(
            dialogTitle: AppLocalizations.of(context)!.download_csv,
            dialogContent: "Download a CSV file with all game data.",
          ),
        ],
      ),
      Row(
        children: [
          ElevatedButton.icon(
            onPressed: () => _showUploadDialog(
              context,
              widget.gameSlug,
              widget.suggestedCSVHeaders,
            ),
            label: Text(AppLocalizations.of(context)!.upload_csv),
            icon: const Icon(Icons.upload_file),
          ),
          InfoButton(
            dialogTitle: AppLocalizations.of(context)!.upload_csv,
            dialogContent: "Upload a CSV file to create new prompts",
          ),
        ],
      ),
    ];
  }

  Future<void> _downloadCsv() async {
    setState(() => _isDownloading = true);
    try {
      await downloadZip(widget.gameSlug);
    } finally {
      setState(() => _isDownloading = false);
    }
  }

  void _showUploadDialog(BuildContext context, String slug, String headers) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return UploadDialog(slug: slug, suggestedCSVHeaders: headers);
      },
    );
  }
}
